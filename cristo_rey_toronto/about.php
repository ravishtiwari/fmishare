<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("about");
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<!--div id="main-content">
			<div id="carousel" style="padding-top: 20px; padding-left: 35px;" ></div>
		</div-->
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_about.png" alt=""/>
			
			<?php include("ssi/about_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Cristo Rey Students </h1>
			<br/>
			As you walk down the corridor of a Cristo Rey School, you�re greeted by a firm handshake and an engaging smile.   Whether it�s through experiences gained in the workplace, lessons learned in school or interactions among peers, teachers and mentors, Cristo Rey students are continuously building strength of character and intelligence that will guide them as they journey into their future... a future of success.
			<br/><br/>
			&#34;Research has shown low income students fall consistently behind in Ontario�s public school system,&#34; says Roland Sintos Coloma, an equity studies and sociology professor at the University of Toronto�s Ontario Institute for Studies in Education.
			<br/><br/>
			Cristo Rey teaches students to stand up to the challenges and obstacles presented by each day.  These students choose to succeed in a school where they will work an entry level job to earn their tuition, have a longer school day and receive a university-preparatory education.
			<br/><br/>
			<br/><br/>
			<span style="font-weight: bold;">Cristo Rey Graduates Move Forward</span>:
			<br/><br/>
			The Cristo Rey Network has achieved impressive high school graduation and post secondary enrolment rates relative to other high schools.  While many factors such as financial aid and family expectations likely affect the post secondary success of our students, rigorous academic preparation has been shown to be the most important driver of college completion.
			<br/><br/>
			98% of Cristo Rey graduates head off to post secondary education clearly demonstrating that Cristo Rey provides students with the skills to succeed.    
			<br/><br/>
			The Cristo Rey system works.  It pulls its student body not from the top of the pile, but from the middle of the pack who four years later graduate and go on to university or college.  Many students arrive in the ninth grade with reading and math skills at an early elementary school level, others lack the basic life skills to look an adult in the eye, shake hands, dress neatly and show up for school and work on time and many come from gang-dominated neighbourhoods.  A post secondary education is what Cristo Rey shoots for, and in 98% of cases, delivers on.
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>