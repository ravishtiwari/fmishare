<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("community");
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<!--div id="main-content">
			<div id="carousel" style="padding-top: 20px; padding-left: 35px;" ></div>
		</div-->
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_community.png" alt=""/>
			
			<?php include("ssi/community_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Meeting the Challenges of Toronto&#39;s Students:</h1>
			<br/>

			Cristo Rey Toronto will open its doors September 2012 to help bring about social justice. The central challenge we will address is providing equal access to a quality university-preparatory education for students from low-income families.
			<br/><br/>
			Cristo Rey Toronto is a collaborative project of lay men and women, and the Basilian Fathers.  Cristo Rey Toronto High School will be funded through the generosity of individual and institutional benefactors. 
			<br/><br/>
			
			<h1 class="header1">Surveys</h1>
			<br/>
			Parent Survey - English [ <a href="docs/parent_survey_english.pdf" target="_blank">Download</a> ]<br/><br/>
			Student Survey - English [ <a href="docs/student_survey_english.pdf" target="_blank">Download</a> ]<br/><br/>
			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>