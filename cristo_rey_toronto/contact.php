<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("contact");
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<!--div id="main-content">
			<div id="carousel" style="padding-top: 20px; padding-left: 35px;" ></div>
		</div-->
		
		<div id="sub-sidebar" style="height: 985px;">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_contact.png" alt=""/>
			
			<!--?php include("ssi/about_sidebar.php"); ?-->
		</div>
		
		<div id="sub-page-content" style="height: 900px;">
			
			Feasibility Study � Chair<br/>
			Father Joseph Redican csb<br/>
			<a href="mailto:redican@torontocristorey.com">redican@torontocristorey.com</a><br/><br/>
			
			Feasibility Study Director<br/>
			Kimberley Bailey<br/>
			416-653-3180 x118<br/>
			<a href="mailto:baileyk@torontocristorey.com">baileyk@torontocristorey.com</a><br/><br/>

			Feasibility Study � Workplace Programme<br/>
			Colleen Cole<br/>
			416-653-3180 x114<br/>
			<a href="mailto:colleenc@torontocristorey.com">colleenc@torontocristorey.com</a><br/><br/>

			Feasibility Study � Community Leadership<br/>
			Faruk Hossan<br/>
			416-653-3180 x115<br/>
			<a href="mailto:farukh@torontocristorey.com">farukh@torontocristorey.com</a><br/><br/>


			Mailing Address:<br/>
			Toronto Cristo Rey High School<br/>
			c/o St. Michael&#39;s College School<br/>
			1515 Bathurst Street<br/>
			Toronto, ON<br/>
			M5P 3H4<br/><br/>

			Fax # 416-653-7704<br/><br/>

			Information:<br/>
			<a href="mailto:info@torontocristorey.com">info@torontocristorey.com</a><br/><br/>
			
			<a href="forms/Cristo_Rey_Information_Request_Form.pdf">Click to download the information request contact form</a>
			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>