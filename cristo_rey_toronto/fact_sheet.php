<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		
		setMenuSection("about");
	}
	
	
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_about.png" alt=""/>
			
			<?php include("ssi/about_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Fact Sheet</h1>
			<br/>
			<ul>
				<li class="fact-item">Toronto Cristo Rey High School will be a coed private Catholic high school that will provide high-quality university preparatory education to academically qualified youth who would not otherwise be able to afford private school education.</li>
				<li class="fact-item">The School is being sponsored by the Basilian Fathers.</li>
				<li class="fact-item">The school will open its doors to Grade 9 students in September 2012. It will add one grade a year and will be Grade 9 -12 by the 2015-2016 school year.</li>
				<li class="fact-item">Catholic and non-catholic students will be welcome as long as they can prove financial need and are able to succeed in a university preparatory program</li>
				<li class="fact-item">Financial need will be defined as being from a family that is below a certain income level. For example a student from a family of two parents and three children with a family income below $45,000 would be eligible to attend the school.</li>
				<li class="fact-item">Each student participates in a Corporate Work Study Program and works one day per week at a pre-selected work site.</li>
				<li class="fact-item">Students must be employable through the Corporate Work Study Program.</li>
				<li class="fact-item">Each student will also pay a modest level of tuition, likely about $2,000 per annum though financial assistance will be available for students in greater need.</li>
				<li class="fact-item">The school will be located on the subway line reasonably close to the downtown business core. This will enable students from anywhere in Toronto to access the school with relative ease and also enable most of the students to go to their work placements by public transportation.</li>
			</ul>
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>