<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		
		setMenuSection("about");
	}
	
	
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_about.png" alt=""/>
			
			<?php include("ssi/about_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Frequently Asked Questions</h1>
			<br/>
			
			<h2 class="header2">What is a Cristo Rey High School?</h2><br/>
			A Cristo Rey High School is a coed Catholic high school that provides high-quality college preparatory education to youth who would not otherwise be able to afford private school education. The outstanding feature of a Cristo Rey High School is its Corporate Work Study Program. All students at Cristo Rey Network schools participate in the program working one day a week at an entry level commercial position. In this way they finance the majority of the cost of their education, gain real world job experience, grow in self-confidence, and realize the relevance of their education.
			<br/><br/>
			
			<h2 class="header2">Why a Cristo Rey High School in Toronto?</h2><br/>
			Since the first Cristo Rey High School was founded in Chicago by Fr. John Foley SJ in 1996, over 22 high schools have been established across the U.S. . They have a proven capacity to help disadvantaged young people graduate from high school and go on to university. Toronto has thousands of young people who are falling through the cracks of the publicly funded public and Catholic schools.  A Cristo Rey High School will provide an opportunity for some of them to achieve their full potential and for our community to benefit from the gifts these young people will develop.
			<br/><br/>
			
			<h2 class="header2">Who will be able to attend Toronto Cristo Rey?</h2><br/>
			Any young man or woman who meets the entrance requirements and who is from a family that is measurably financially disadvantaged will be able to attend Toronto Cristo Rey. The school will be a Catholic school and under the sponsorship of Catholic religious orders, however students of all faiths from within the boundaries of city of Toronto will be able to attend.  In the United States some schools such as Detroit Cristo Rey are 60% Catholic because they serve a Mexican- Catholic community. In other schools such as in Chicago or Cleveland the schools are serving non-Catholic communities and are only 10 -20% Catholic.
			<br/><br/>
			
			<h2 class="header2">When will the school start and where will it be located?</h2><br/>
			It is hoped that the school will open its doors to Grade 9 students in September 2012. It will add one grade a year and will be Grade 9 -12 by the 2015-2016 school year.  While an exact location has not been identified we are looking for a site on the subway line reasonably close to the core area. This will enable students from anywhere in Toronto to access the school with relative ease and also enable most of the students to go to their work placements by public transportation. 
			<br/><br/>
			
			<h2 class="header2">What is the Cristo Rey Network?</h2><br/>
			The Cristo Rey Network� is an association of Cristo Rey high schools in the United States. The Network ensures that member schools follow the effectiveness standards that will enable them to function as good schools following the Cristo Rey model. Fr. John Foley  SJ is the Chair of the Board. The Toronto Cristo Rey  project is planning on being part of the network, the first Cristo Rey school outside of the United States.  The network mandates that all prospective new schools undertake a feasibility study before it is recognized as a member school. We are presently beginning that feasibility study process.
			<div style="font-style: italic;">For more information, please visit the <a href="http://www.cristoreynetwork.org/" style="font-weight: bold;">Cristo Rey Network website.</a></div>
			<br/>
 
			<h2 class="header2">Who are the sponsors of a Cristo Rey High School?</h2><br/>
			Typically Cristo Rey High Schools are sponsored by one or more religious communities. The religious communities are usually ones who have some connection with both the city in which the school is to be located and with the charism of Catholic education. For example the Jesuits sponsor schools in Chicago and Houston. The Cristo Rey School in Detroit is jointly sponsored by the Basilian Fathers and the Sisters of the Immaculate Heart of Mary.
			<br/><br/>
 
			Through sponsorship, the religious orders impart a clarity of mission of Catholic education. Usually one or more members of each order are active on the Board of the school and on the Board of the Corporate Work Study Program. Financial contributions are welcome and often made by the sponsoring Congregations but not expected or necessary.
			<br/><br/>
			
			The Basilian Fathers are committed to sponsoring Toronto Cristo Rey and are open to co-sponsorship with a community of women religious.
			<br/><br/>
 
			<h2 class="header2">Where Does the School&#39;s Funding Come From?</h2><br/>
			Much of the school&#39;s funding comes from the Corporate Work Study Program. The school integrates classroom instruction with real-life work experience for each student by partnering with many of the finest businesses and corporations in the community. These partnerships allow the school to make a faith-based, student centered education available at a very affordable cost to its students.
			Each student&#39;s family is expected to pay a modest amount of tuition that is set based on the resources of that family. In addition each school has an active development program to raise additional operation and capital funds needed.
			
			
			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>