<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		setMenuSection("support");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_support.png" alt=""/>
			<?php include("ssi/support_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			<h1 class="header1">Founder&#39;s Fund</h1>
			<br/>
			In donating $1 million towards the establishment of the Cristo Rey Toronto School,  $400,000 will be directed towards the seed capital to ensure Cristo Rey Toronto is launched successfully.  The remaining $600,000 will be invested in an endowed fund which will ensure the meeting of the immediate needs and providing University Awards to our graduating students.  A substantial endowment fund serves not only as a safety net but benefits students directly, since a portion of the investment income each year will provide �extras� for the school.
			<br/><br/>
			<h1 class="header1">A Fund for Academic Excellence</h1>
			<br/>
			This fund is a vote of confidence for our students as they pursue University studies by annually awarding a Scholarship in your name to a deserving graduating student.
			<br/><br/>

			<h1 class="header1">A Fund for Student Financial Aid</h1>
			<br/>
			The ability for Cristo Rey families to meet some of the challenges such as transportation and uniform will be supported through this fund.
			<br/><br/>
			<h1 class="header1">A Fund for Technology</h1>
			<br/>
			Information technology is virtually integrated in all aspects of the curriculum.  Our students must never fall behind in this crucial area.
			<br/><br/>
			<h1 class="header1">A Fund for the Library</h1>
			<br/>
			Excellence in education depends on access to information through all the media available from books to online resources.  An endowment for the library will ensure that Cristo Rey has the breadth and depth to its collections, across subjects, grades and ages.
			<br/><br/>
			<h1 class="header1">A Fund for the Arts</h1>
			<br/>
			Research shows that exploring the visual arts promotes the ability to think spatially.  Creative writing helps students think, speak and write better.  The study of music improves mathematical ability, while performing arts programs build confidence.  A fund for the arts will keep a strong, creative and consistent program for years to come.
			<br/><br/>
			<h1 class="header1">A Fund for Athletics</h1>
			<br/>
			The pursuit of sport teaches both leadership and teamwork.  It builds discipline and mental strength and such play is an important part of learning.  An endowment in athletics will maintain and build upon initiatives like specialized coaching and facility upgrades.
			<br/><br/>
			We are asking that you consider making a transformational gift to Cristo Rey Toronto by becoming a member of the Cristo Rey Founder�s Fund.
			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>