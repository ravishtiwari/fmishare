<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
	
		setMenuSection("about");
	}
	
	
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_about.png" alt=""/>
			
			<?php include("ssi/about_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Cristo Rey Network</h1>
			<br/>
			Cristo Rey has emerged as one of North America�s exceptional academic institutions by responding to the challenge of providing students below the poverty level the opportunity to complete high school and attend college or university.
			<br/><br/>
			What began in 1996 with the goal of creating college readiness for students, now has 24 high schools teaching 6,000 students in the U.S. � in big cities like L.A., New York, Chicago and Detroit- and is preparing to open in Toronto.
			<br/><br/>
			Founded in 2003, the Cristo Rey Network Office offers professional development programs for faculty and staff, facilitates the sharing of best practices among schools, and monitors each school�s progress on the Network�s Mission Effectiveness Standards.
			<br/><br/>
			In the 2008-2009 school year: 
			<ul>
				<li>5,003 Cristo Rey Network students contributed, cumulatively, $26.5 million to fund their education. </li>
				<li>The Network had more than 1,000 work study jobs at 1,252 sponsoring companies. (This school year, more than 250 additional companies will provide jobs for Cristo Rey students.) </li>
				<li>99% of the schools� senior class were accepted into college.</li>
			</ul>
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>