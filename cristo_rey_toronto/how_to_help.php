<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		
		setMenuSection("support");
	}
	
	
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_support.png" alt=""/>
			<?php include("ssi/support_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			<h1 class="header1">How To Help</h1>
			<br/>
			Our school has been blessed by gifts and support from the many people and organizations who have been inspired by the Cristo Rey mission. 
			<br/><br/>
			We are grateful for the support of so many and hope that this generosity will inspire others so that we can provide a quality, university-prep education for our students. 
			<br/><br/>
			Learn how you can support our mission:
			<ul>
				<li>Make a Donation </li>
				<!--li>Join the 2-Year Cristo Rey Volunteer Program </li-->
				<li>Find Other Volunteer Opportunities with Cristo Rey </li>
				<li>Attend a Fundraising Event</li>
			</ul>
			<br/><br/>
			<a href="forms/Donation_Form.pdf">Click to download the donation form</a>
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>