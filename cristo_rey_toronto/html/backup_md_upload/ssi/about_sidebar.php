<div id="sidebar-section-item-container">
	<div class="sidebar-section-item">
		<a href="about.php">About</a>
		<br/>
		<span class="sidebar-item-desc">About Cristo Rey</span>
		<br/>
		<!-- ------------------------------------------- -->
		<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="mission.php">Mission</a>
		<br/>
		<span class="sidebar-item-desc">Our mission</span>
		<br/>
		<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="history.php">History</a>
		<br/>
		<span class="sidebar-item-desc">Our history</span>
		<br/>
		<img src="images/divider.png" class="divider"/>
	</div>
	
	<!--div class="sidebar-section-item">
		<a href="news.php">News</a>
		<br/>
		<span class="sidebar-item-desc">News</span>
		<br/>
		<img src="images/divider.png" class="divider"/>
	</div-->
	
	<div class="sidebar-section-item">
		<a href="fact_sheet.php">Fact Sheet</a>
		<br/>
		<span class="sidebar-item-desc">Cristo Rey High School Fact Sheet</span>
		<br/>
	<img src="images/divider.png" class="divider"/>

	</div>
	
	<div class="sidebar-section-item">
		<a href="faq.php">FAQ</a>
		<br/>
		<span class="sidebar-item-desc">Frequently asked questions</span>
		<br/>
		<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="links.php">Links</a>
		<br/>
		<span class="sidebar-item-desc">Links and Resources</span>
		<br/>
		<img src="images/divider.png" class="divider"/>
	</div>
	
</div>