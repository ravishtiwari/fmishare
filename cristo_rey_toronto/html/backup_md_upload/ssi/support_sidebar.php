<div id="sidebar-section-item-container">
	<div class="sidebar-section-item">
		<a href="support.php">Support</a>
		<br/>
		<span class="sidebar-item-desc">Support Cristo Rey</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="how_to_help.php">How To Help</a>
		<br/>
		<span class="sidebar-item-desc">See how you can support Cristo Rey</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="donor_wall.php">Donor Wall</a>
		<br/>
		<span class="sidebar-item-desc">List of donors</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="founders_fund.php">Founder&#39;s Fund</a>
		<br/>
		<span class="sidebar-item-desc">Founder&#39;s Fund</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
	<div class="sidebar-section-item">
		<a href="events.php">Events</a>
		<br/>
		<span class="sidebar-item-desc">Cristo Rey Events</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
</div>
