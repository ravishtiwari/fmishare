<div id="sidebar-section-item-container">
	<div class="sidebar-section-item">
		<a href="workstudy.php">
		Workstudy
		</a>
		<br/>
		<span class="sidebar-item-desc">Our innovative corporate work study program</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
	<!--div class="sidebar-section-item">
		<a href="case_study.php">Case Study</a>
		<br/>
		Success stories
		<br/>
		-------------------------------------------
	</div-->
	
	<!--div class="sidebar-section-item">
		<a href="industry_network.php">Participating Companies</a>
		<br/>
		Meet our support network
		<br/>
		-------------------------------------------
	</div-->
	
	<div class="sidebar-section-item">
		<a href="job_descriptions.php">Job Descriptions</a>
		<br/>
		<span class="sidebar-item-desc">Outline the work that our students perform in various departments</span>
		<br/>
	<img src="images/divider.png" class="divider"/>
	</div>
	
</div>