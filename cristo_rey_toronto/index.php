<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<!--carousel-->
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		createCarousel();
		setMenuSection("home");
	}
	
	function createCarousel(){
	
		var ilist = [
			{"path":"images/carousel/carousel13.jpg", "text":"MISSION: A Cristo Rey School prepares young people with limited options, for post secondary education"},
			{"path":"images/carousel/carousel11.jpg", "text":"...Cristo Rey students are continuously building strength of character and intelligence that will guide them as they journey into their future... a future of success"},
			{"path":"images/carousel/carousel10.jpg", "text":"Cristo Rey is an independent Catholic high school which combines personal responsibility, academic rigor and a corporate work-study program to empower students of all faiths from economically challenged families to reach their full potential."},
			{"path":"images/carousel/carousel12.jpg", "text":"The Cristo Rey model pays students to work and is one of the most innovative ideas in education; every student at a Cristo Rey high school works full time one day a week with a local private company or not-for-profit organization."},
			{"path":"images/carousel/carousel15.jpg", "text":"'Toronto Cristo Rey will enable deserving students to benefit from academic, life and employment experiences in a caring and motivating environment.' - Patricia D. Parisi, Former Principal, St. Clement�s School"},
			{"path":"images/carousel/carousel09.jpg", "text":"'I believe that a Cristo Rey school in Toronto will address the growing issues of access to and success in education within the GTA. It will provide an alternative educational opportunity for both parents and students.' - Emile John, VP St. Michaels College School"},
			{"path":"images/carousel/carousel16.jpg", "text":"The work-study program contributes approximately 60% of a school's budget and keeps average tuitions low."},
			{"path":"images/carousel/carousel17.jpg", "text":"Toronto Cristo Rey will provide a unique opportunity for academically qualified students who would not otherwise be able to afford an independent school education. Through this special programming, these students will pursue scholarship, develop strong leadership and experience entrepreneurship."},
			{"path":"images/carousel/carousel18.jpg", "text":"When Toronto Cristo Rey opens its doors September 2012 it will be helping to bring about social justice in our community. The central challenge we will address is providing equal access to a quality university-preparatory education for students from low-income families."},
			{"path":"images/carousel/carousel19.jpg", "text":"Toronto Cristo Rey is a collaborative project of lay men and women, and the Basilian Fathers. Toronto Cristo Rey High School will be funded through the generosity of individual and institutional benefactors."},
			{"path":"images/carousel/carousel20.jpg", "text":"98% of Cristo Rey students graduate to post secondary programmes clearly demonstrating that Cristo Rey provides students with the skills to succeed."},
			{"path":"images/carousel/carousel21.jpg", "text":"Toronto Cristo Rey Corporate Work Study Program seeks full-time, entry-level clerical positions for its students."}

		];
	
	//#8b0b04
		//carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "black", "white", 14, ilist, "carousal_transparency_left_black.png");
	carouselBox = new carousel("carouselBox", 641, 325, 104, 133, "black", "white", 14, ilist, "carousal_transparency_left_black.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
		
	<?php include("ssi/header.php"); ?>
	
	<div id="front-body-middle-container">
		
		<div id="main-content">
			<!--div id="carousel" style="margin-top:20px; padding-top: 20px; padding-left: 35px;" ></div-->

			<div id="carousel" style="margin-top:0px; padding-top: 20px; padding-left: 19px;" ></div>
		</div>
		
		<div id="sidebar">
			<img id="side-bar-logo" style="top: 150px;" src="images/logo.png" alt=""/>
		</div>
		
		<div id="front-page-news-container">
			
			<div id="news-column1" class="front-column" >
				<!--img src="images/boxheading1.png" alt=""/-->
				<p class="front-column">
				<div class="content-constraint">
				<span class="home-heading">NEWS</span>
				<p class="home-col-content">April 25, 2011 - The Toronto Cristo Rey website is currently under construction.</p>
				</div>
				</p>
			</div>
			
			<div id="news-column2" class="front-column" >
				<!--img src="images/boxheading2.png" alt=""/-->
				<p class="front-column">
								<div class="content-constraint">
								</div>

				</p>
			</div>
			
			<div id="news-column3" class="front-column" >
				<!--img src="images/boxheading3.png" alt=""/-->
				<p class="front-column">
								<div class="content-constraint">

					<span class="home-heading">MISSION</span>
					
				<blockquote>
					
					<p class="home-col-content">
					For all students to have equal access to the educational opportunities they need to be successful in college and contribute with their gifts.</p>
					</blockquote>
					</div>
				</p>
			</div>
			
		</div>
		
	</div>
	
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>