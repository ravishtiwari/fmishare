<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		setMenuSection("workstudy");
	}
	
	function setMenuSection(msection){
		document.getElementById("menu_" + msection).src = "images/menu_" + msection + "4.png";
		document.getElementById("menu_" + msection).onmouseover = "";
		document.getElementById("menu_" + msection).onmouseout = "";
		document.getElementById("menu_" + msection).onmousedown = "";
		document.getElementById("menu_" + msection).onmouseup = "";
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_workstudy.png" alt=""/>
			
			<?php include("ssi/workstudy_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Job Descriptions</h1>
			<br/>
			Cristo Rey Students are trained in office and clerical duties such as filing, photocopying and mail delivery. These skills can apply to different roles at different companies. The following sample job descriptions outline the work that our students perform in various departments for our sponsor-clients.
			<br/>
			
			<ul>
				<li>Accounting</li>
				<li>Human Resources</li>
				<li>Finance, Middle Office, and Compliance Functions</li>
				<li>Marketing and Business Development</li>
				<li>IT</li>
				<li>Office Services</li>
			</ul>
			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>