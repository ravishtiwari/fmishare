function carousel(n, pw, ph, dh, db, tc, fc, fs, il, tl){

	var name = n;
	var picWidth = pw;
	var picHeight = ph;
	var descHeight = dh;
	var descBottom = db;
	var transColor = tc;
	var fontColor = fc;
	var fontSize = fs;
	var transparentLeftImage = tl
	
	var self = this;
	
	self.imageList = il;
	
	
	self.timer = 0;
	
	// current index of the image
	self.curImage = 0;
	
	// time between image changes
	self.time_length = 5000;
	

	// create the carousel elements
	self.createElements = function(){
	
		// display area
		var displayArea = document.createElement("div");
		displayArea.id = "display";
		displayArea.onmouseover = new Function(name + '.stopTimer();');
		displayArea.onmouseout = new Function(name + '.delayedTimedRotation();');
		document.getElementById("carousel").appendChild(displayArea);
		
		
		// rotating image
		var picture = document.createElement("img");
		picture.id = "picture";
		picture.src = self.imageList[0].path;
		picture.alt = "No Image Loaded";
		document.getElementById("display").appendChild(picture);
		
		// description text contaner
		var description = document.createElement("div");
		description.id = "description";
		document.getElementById("display").appendChild(description);
		
		// description text transparency
		var descTransparency = document.createElement("div");
		descTransparency.id = "descTransparency";
		document.getElementById("description").appendChild(descTransparency);
		
		var transLeft = document.createElement("img");
		transLeft.id = "transLeft";
		transLeft.src = "images/carousel/" + transparentLeftImage;
		transLeft.alt = "";
		document.getElementById("carousel").appendChild(transLeft);
		
		// description text 
		var descText = document.createElement("div");
		descText.id = "descText";
		descText.innerHTML = "This is where the image description will be.  Information will be provided shortly.  This is where the image description will be.  Information will be provided shortly.";
		document.getElementById("description").appendChild(descText);
		
		
		// position dot markers
		var position = document.createElement("div");
		position.id = "position";
		document.getElementById("description").appendChild(position);
		
		// donate button
		var donate = document.createElement("img");
		donate.id = "donate";
		donate.src = "images/carousel/button_donate1.png";
		donate.alt = "";
		document.getElementById("display").appendChild(donate);
		
		// donate button top
		var donateTop = document.createElement("img");
		donateTop.id = "donateTop";
		donateTop.src = "images/carousel/button_donate2.png";
		donateTop.alt = "";
		document.getElementById("carousel").appendChild(donateTop);
		
		// donate button left
		var donateLeft = document.createElement("img");
		donateLeft.id = "donateLeft";
		donateLeft.src = "images/carousel/button_donate3.png";
		donateLeft.alt = "";
		document.getElementById("carousel").appendChild(donateLeft);
	};
	
	
	
	self.setParameters = function(){
		document.getElementById("carousel").style.width = picWidth + "px";
		document.getElementById("carousel").style.height = picHeight + "px";
		document.getElementById("display").style.height = picHeight + "px";
		document.getElementById("display").style.width = picWidth + "px";
		document.getElementById("description").style.bottom = descBottom + "px";
		document.getElementById("description").style.height = descHeight + "px";
		document.getElementById("descTransparency").style.backgroundColor = tc;
		document.getElementById("descText").style.color = fc;
		document.getElementById("descText").style.fontSize = fs + "px";
	}
	
	
	
	// set an image at location n
	self.setImage = function(n){
		document.getElementById("picture").src = self.imageList[n].path;
		document.getElementById("descText").innerHTML = self.imageList[n].text;
		self.curImage = n;
	};
	
	
	
	// switch dot n from popped in or popped out
	// s is "over" on mouseover
	// s is "out" on mouseout
	self.dotSwitch = function(n, s){
		
		if (self.curImage !== n){
			if (s === "over"){
				document.getElementById("dot" + n).src = "images/carousel/dot2.png";
			}else if (s === "out"){
				document.getElementById("dot" + n).src = "images/carousel/dot.png";
			}
		}
		
	};
	
	// set reset the previous clicked dot
	// set the new image in carousel display
	// set clicked dot with 'clicked dot' image
	self.dotClick = function(n){
		document.getElementById("dot" + self.curImage).src = "images/carousel/dot.png";
		self.setImage(n);
		document.getElementById("dot" + n).src = "images/carousel/dot3.png";
	};
	
	// create the navigation dots
	self.createDots = function(){
		var totalDots = self.imageList.length;
		var dot;
		
		var i=0;
		for (i=0; i<totalDots; i++)
		{
			dot = document.createElement("img");
			dot.id = "dot" + i;
			dot.setAttribute("class", "dot");
			dot.onclick = new Function(name + '.dotClick(' + i + ');');
			dot.onmouseover = new Function(name + '.dotSwitch(' + i + ',"over");');
			dot.onmouseout = new Function(name + '.dotSwitch(' + i + ',"out");');
			dot.src = "images/carousel/dot.png";
			document.getElementById("position").appendChild(dot);
		}
		
		// realign the set of dots
		//document.getElementById("position").style.left = ((picWidth - 6*i) - 80) + "px";
		document.getElementById("position").style.left = ((picWidth - 6*i) - 125) + "px";

		document.getElementById("position").style.top = (descHeight + 5) + "px";
		
	};
	
	// delay resuming the carousel rotation
	self.delayedTimedRotation = function(){
		self.timer = setTimeout(function() {self.setTimedRotation();},self.time_length);
	};
	
	// timer is recursive
	// image switch immediately, then wait before calling again
	self.setTimedRotation = function(){
		if(self.curImage < self.imageList.length-1){
			self.dotClick(self.curImage+1);
		}else{
			self.dotClick(0);
		}
		self.timer = setTimeout(function() {self.setTimedRotation();},self.time_length);
	};
	
	// stop the timer
	self.stopTimer = function(){
		clearTimeout(self.timer);
	};
	
	
	self.createElements();	
	self.createDots();
	self.setParameters();
	self.dotClick(0);
	self.timer = setTimeout(function() {self.setTimedRotation();}, self.time_length);
	
}