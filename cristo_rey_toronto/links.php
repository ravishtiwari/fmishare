<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		
		setMenuSection("about");
	}
	
	
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_about.png" alt=""/>
			
			<?php include("ssi/about_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">Links</h1>
			<br/>
			<p>Cristo Rey Jesuit High School, the first school in Network, was featured on CBS's 60 Minutes.<br/>[ <a href="http://www.cristoreynetwork.org/flash/cristo_rey_60_min_interview.swf" target="_blank">view video</a> ]
			</p>
			<br/>
			<p>Cristo Rey - Case for Support [ <a href="docs/case_for_support_cristo_rey.pdf" target="_blank">download pdf document</a> ]</p>
			<br/>

			<p>Cristo Rey - Brochure [ <a href="docs/brochure_cristo_rey.pdf" target="_blank">download pdf document</a> ]</p>
			<br/>

			<p>Cristo Rey - Work Study Flyer [ <a href="docs/work_study_flyer_cristo_rey.pdf" target="_blank">download pdf document</a> ]</p>
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>