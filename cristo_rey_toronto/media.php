<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("media");
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<!--div id="main-content">
			<div id="carousel" style="padding-top: 20px; padding-left: 35px;" ></div>
		</div-->
		
		<div id="sub-sidebar" style="height: 1285px;">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_media.png" alt=""/>
			
			<?php include("ssi/media_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content" style="height: 1200px;">
			
			60 Minutes Interview Segment - General Overview of a Cristo Rey School
			<iframe width="425" height="349" src="http://www.youtube.com/embed/6Ev8kmPnzaY" frameborder="0" allowfullscreen></iframe>
		
			<br/><br/>
			About Cristo Rey
			<iframe width="560" height="349" src="http://www.youtube.com/embed/eQ6Ma7-G_88" frameborder="0" allowfullscreen></iframe>
			
			<br/><br/>
			<a href="http://youtu.be/Mwf28lqx4zA">Students work for tuition</a>
			
			
			<br/><br/>
			2010 Cristo Rey Network Graduation
			<iframe width="425" height="349" src="http://www.youtube.com/embed/iMtkACKqenQ" frameborder="0" allowfullscreen></iframe>
			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>