<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		setMenuSection("about");
	}
	
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_about.png" alt=""/>
			
			<?php include("ssi/about_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content" >
			
			<h1 class="header1">Mission: Open doors to success</h1>
			
			Cristo Rey Network prepares young people with limited options for post secondary education.
			<br/><br/>
			Cristo Rey depends on philanthropists and corporate work partners to advance its mission.  The operation budget has been structured to provide students with the opportunity to work their way through their education.  That is why Cristo Rey is embarking on a Capital Campaign under the banner <b>Opening Doors to Success</b>.  
			<br/><br/>
			This campaign will raise $10 million to open the doors at Cristo Rey Toronto providing both operational funding and enhancements to the facilities.  Cristo Rey graduates can be almost anything they want to be; whether entrepreneurs, artists, scientists, medical researchers, lawyers or educators, Cristo Rey graduates are helping shape the world for themselves and for the benefit of others.
			<br/><br/>
			
			<h1 class="header1">Our vision:</h1>
			
			All students have equal access to the educational opportunities they need to be successful in university and contribute with their gifts to the arrival of the Kingdom of God. Why Cristo Rey Toronto Is Needed 
			<br/><br/>
			
			<h1 class="header1">Educational enrichment initiative:</h1>
			
			A university degree is an essential ticket to opportunity and a path out of poverty for young people with limited options. We have enhanced our educational focus to ensure student readiness for success in university through careful attention to students� academic preparedness. 
			<br/><br/>
			Research demonstrates that the primary driver of university success is the rigor of a student�s academic preparation in high school. To deliver a university-preparatory program more closely aligned with criteria for success in university, the Cristo Rey Network is fundamentally transforming our impact by focusing resources to substantially strengthen the Network�s educational program. 
			<br/><br/>
			The Educational Enrichment Initiative has been formulated to define what it means to be &#34;university ready&#34; and to ensure students&#39; success in university and beyond. Key priorities include: 
			<ol>
				<li>Strengthening our educational model through development of a &#34;standards-based&#34;, university- ready curriculum </li>
				<li>Supporting the strengthened model through focused professional development of school leaders </li>
				<li>Utilizing data-driven decision- making to maximize student learning </li>
				<li>Studying and then incorporating effective teaching practices which optimize student achievement</li>
			</ol>
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>