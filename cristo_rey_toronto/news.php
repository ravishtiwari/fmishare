<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	
	function loadNews(){
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.open("GET","news/news.xml",false);
		xmlhttp.send();
		xmlDoc=xmlhttp.responseXML;
		
		newsContent = "";
		
		var x=xmlDoc.getElementsByTagName("item");
		for (i=0;i<x.length;i++)
		{ 
			
			try{
				newsContent = newsContent + "<div class='news-item'><span style='font-style: italic;'>" + x[i].getElementsByTagName("date")[0].childNodes[0].nodeValue 
					+ "</span><br/><span style='font-weight: bold;'>"
					+ x[i].getElementsByTagName("heading")[0].childNodes[0].nodeValue + "</span><br/>";
				
					try{
						newsContent = newsContent + "<div class='news-item'>" + x[i].getElementsByTagName("content")[0].childNodes[0].nodeValue + "<br/>"
					}catch(err){
						// no content
					}
				
				newsContent = newsContent + "</div>";
			}catch(err2){
				// missing heading or date
			}
		}
		document.getElementById("news-listings").innerHTML = newsContent;
	}
	
	
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("media");
		loadNews();
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_media.png" alt=""/>
			
			<?php include("ssi/media_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			
			<h1 class="header1">News </h1>
			<br/>
			<div id="news-listings">
			</div>
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>