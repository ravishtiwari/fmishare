<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("support");
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

	<div id="wrapper">
	
		<?php include("ssi/header.php"); ?>
		
		<div id="sub-body-middle-container">
			
			<!--div id="main-content">
				<div id="carousel" style="padding-top: 20px; padding-left: 35px;" ></div>
			</div-->
			
			<div id="sub-sidebar">
				<img id="side-bar-logo" src="images/logo.png" alt=""/>
				<img id="side-bar-section-header" src="images/transparency_fold_right_support.png" alt=""/>
			
				<?php include("ssi/support_sidebar.php"); ?>
				
			</div>
			
			<div id="sub-page-content">
				<h1 class="header1">A Solid Financial Base Provides A Unique And Exceptional Education � Founder&#39;s Fund</h1>
				<br/>
				The Cristo Rey Founder&#39;s Fund will meet the immediate needs in establishing the Cristo Rey Toronto School&#39;s Opening Doors To Success Campaign. 
				<br/><br/>
				The $10 million in seed capital will ensure the cost associated with the start up for the first two years is addressed.  The $10 million will be secured in pledges over a five year period of time.  The operational costs in year three and beyond will be offset by the Cristo Rey work program which will generate the necessary funding to support the program.
				<br/><br/>
				Founder&#39;s Fund members of the Open Doors To Success Campaign will be recognized for their generosity with a permanent wall honouring their leadership and vision.
				<br/><br/>
				<a href="forms/Donation_Form.pdf">Click to download the donation form</a>
			</div>
			
		</div>
		<?php include("ssi/footer.php"); ?>
	</div>

</body>
</html>