<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/website.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	var carouselBox;
	
	function init(){
		//createCarousel();
		setMenuSection("workstudy");
	}
	
	function setMenuSection(msection){
		document.getElementById("menu_" + msection).src = "images/menu_" + msection + "4.png";
		document.getElementById("menu_" + msection).onmouseover = "";
		document.getElementById("menu_" + msection).onmouseout = "";
		document.getElementById("menu_" + msection).onmousedown = "";
		document.getElementById("menu_" + msection).onmouseup = "";
	}
	
	function createCarousel(){
		var ilist = [
			{"path":"images/carousel/carousel05.jpg", "text":"text here"},
			{"path":"images/carousel/carousel06.jpg", "text":"text here"},
			{"path":"images/carousel/carousel07.jpg", "text":"text here"},
			{"path":"images/carousel/carousel08.jpg", "text":"text here"}
		];
	
		carouselBox = new carousel("carouselBox", 600, 325, 104, 133, "#8b0b04", "white", 14, ilist, "carousal_transparency_left.png");
	}
	
	function highlightMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	function resetMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "1.png";
	}
	
	function mouseDownMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "3.png";
	}
	
	function mouseUpMenu(t){
		var menu_item = "menu_" + t;
		
		document.getElementById(menu_item).src = "images/menu_" + t + "2.png";
	}
	
	
</script>
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<!--end carousel-->

<title>Cristo Rey Toronto</title>
</head>
<body onload="init()">

<div id="wrapper">
	
	<?php include("ssi/header.php"); ?>
	
	<div id="sub-body-middle-container">
		
		<div id="sub-sidebar">
			<img id="side-bar-logo" src="images/logo.png" alt=""/>
			<img id="side-bar-section-header" src="images/transparency_fold_right_workstudy.png" alt=""/>
			
			<?php include("ssi/workstudy_sidebar.php"); ?>
		</div>
		
		<div id="sub-page-content">
			<!--div id="carousel" style="padding-top: 20px; padding-left: 35px;" ></div-->
		
			Our Innovative Corporate Work Study Program (CWSP) is Designed to Break the Cycle of Poverty.
			<br/><br/>
			<h1 class="header1">How It Works</h1>
			<br/>
			Our students work one day a week during the school year in entry level jobs at some of Toronto's most prestigious companies to cover the majority of their cost of education. The remainder of the cost is covered by tuition (all families pay something!) and traditional fundraising. Each company generally hires five students (a full team - one student worker for each day of the week), but partial teams are also available.
			<br/><br/>
			<h1 class="header1">The Benefits</h1>
			<br/>
			CWSP Sponsors: Simultaneously gain reliable, productive, cost-effective entry-level workers and satisfy their philanthropic, community and diversity outreach objectives. 
			<br/><br/>
			Students: Acquire technical and practical workplace skills, develop the strong work ethic, confidence and self-respect needed to succeed in the classroom and beyond, and open their minds to life-changing career possibilities.
						<br/><br/>

			<h1 class="header1">Sample Letters of Intent</h1>
			<br/>
			Sample Letter of Intent #1 [ <a href="docs/sample_letter_of_intent_1.pdf" target="_blank">Download</a> ]<br/><br/>
			Sample Letter of Intent #2 [ <a href="docs/sample_letter_of_intent_2.pdf" target="_blank">Download</a> ]<br/><br/>

			
		</div>
		
	</div>
	<?php include("ssi/footer.php"); ?>
</div>

</body>
</html>