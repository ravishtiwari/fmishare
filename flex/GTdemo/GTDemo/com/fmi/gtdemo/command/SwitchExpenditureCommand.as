package com.fmi.gtdemo.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.gtdemo.event.ChangeViewEvent;
    import com.fmi.gtdemo.model.ViewModelLocator;
    
    public class SwitchExpenditureCommand implements ICommand, IResponder
    {
	public function SwitchExpenditureCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		//play audio clip
		model.soundStop();
		model.soundPlay('assets/audio/expenditure.mp3');
		
		model.income = (event as ChangeViewEvent).income;
		model.numIncome = (event as ChangeViewEvent).numIncome;
		model.currentView = ViewModelLocator.EXPENDITURE_VIEW;

        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();

            
            /* I think it is best not to use these static variables to indicate current state, 
            but since this example is supposed to follow Cairngorm's methodology, I am using
            them here */
            model.currentView = ViewModelLocator.EXPENDITURE_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}