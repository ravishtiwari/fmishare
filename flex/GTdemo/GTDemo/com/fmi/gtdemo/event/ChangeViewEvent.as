package com.fmi.gtdemo.event
{
	import flash.events.Event;
	import com.adobe.cairngorm.control.CairngormEvent;


	public class ChangeViewEvent extends CairngormEvent
	{
		//syncronized with controller commands
		public static var SWITCH_ABOUT:String = "switchAbout";
		public static var SWITCH_FRONT:String = "switchFront";
		public static var SWITCH_INTRO:String = "switchIntro";
		public static var SWITCH_LOCATION:String = "switchLocation";
		public static var SWITCH_ASSETS:String = "switchAssets";
		public static var SWITCH_RETURN_RATE:String = "switchReturnRate";
		public static var SWITCH_INCOME:String = "switchIncome";
		public static var SWITCH_EXPENDITURE:String = "switchExpenditure";
		public static var SWITCH_ASSET_GROWTH_CHART:String = "switchAssetGrowthChart";
		public static var SWITCH_DONATION_RATE:String = "switchDonationRate";
		public static var SWITCH_RESULTS:String = "switchResults";
		public static var SWITCH_CONTACT:String = "switchContact";
		public static var SWITCH_GIFTABULATOR:String = "switchGiftabulator";

		public var country:String;
		public var location:String;
		public var totalAssets:String;
		public var numTotalAssets:Number;
		public var returnRate:Number;
		public var income:String;
		public var numIncome:Number;
		public var expenses:String;
		public var numExpenses:Number;
		public var donationRate:Number;
		

		/**
		 * Constructor.
		 */
		public function ChangeViewEvent(type:String)
		{
			super( type );
		}
		 
		 /**
		  * Override the inherited clone() method, but don't return any state.
		  */
		override public function clone() : Event
		{
			return new ChangeViewEvent(this.type);
		}    
	}
    
}