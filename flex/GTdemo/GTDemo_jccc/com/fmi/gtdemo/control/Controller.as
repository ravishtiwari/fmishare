package com.fmi.gtdemo.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.gtdemo.event.*;
    import com.fmi.gtdemo.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_ABOUT, SwitchAboutCommand );
		addCommand( ChangeViewEvent.SWITCH_INTRO, SwitchIntroCommand );
		addCommand( ChangeViewEvent.SWITCH_LOCATION, SwitchLocationCommand );
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_ASSETS, SwitchAssetsCommand );
		addCommand( ChangeViewEvent.SWITCH_RETURN_RATE, SwitchReturnRateCommand );
		addCommand( ChangeViewEvent.SWITCH_INCOME, SwitchIncomeCommand);
		addCommand( ChangeViewEvent.SWITCH_EXPENDITURE, SwitchExpenditureCommand);
		addCommand( ChangeViewEvent.SWITCH_ASSET_GROWTH_CHART, SwitchAssetGrowthChartCommand);
		addCommand( ChangeViewEvent.SWITCH_DONATION_RATE, SwitchDonationRateCommand);
		addCommand( ChangeViewEvent.SWITCH_RESULTS, SwitchResultsCommand);
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		addCommand( ChangeViewEvent.SWITCH_GIFTABULATOR, SwitchGiftabulatorCommand);
		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}