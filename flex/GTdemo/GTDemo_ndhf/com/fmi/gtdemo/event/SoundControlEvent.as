package com.fmi.gtdemo.event
{
	import flash.events.Event;
	import com.adobe.cairngorm.control.CairngormEvent;


	public class SoundControlEvent extends CairngormEvent
	{
		//sound control events
		public static var SOUND_PLAY:String = "soundPlay";
		public static var SOUND_STOP:String = "soundStop";

		public var soundFile:String="";
		

		/**
		 * Constructor.
		 */
		public function SoundControlEvent(type:String)
		{
			super( type );
		}
		 
		 /**
		  * Override the inherited clone() method, but don't return any state.
		  */
		override public function clone() : Event
		{
			return new ChangeViewEvent(this.type);
		}    
	}
    
}