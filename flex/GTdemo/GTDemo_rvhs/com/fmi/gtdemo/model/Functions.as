package com.fmi.gt.model{
	public class Functions
	{
		private static var pattern:RegExp = /,/gi;
		
        public static function strToNumber(str:String):Number
        {
			return new Number( parseFloat(str.replace(pattern, "")) );
        }

        public static function calculateTaxCredit(gift:Number):Number
        {
	        if ( gift > 200 ) {
		      return (200 * 0.155) + (gift - 200) * 0.29;
	        }
			return gift * 0.155;
        }


		private static function invInc(p:Number, i:Number):Number
		{
			return p * i;
		}
		
        public static function calculateYears(donor:Object):Array
        {   
	        var _years:Array = new Array();
			
			var asset:Number = strToNumber( donor.Asset );
			var income:Number = strToNumber( donor.Income );
			var expenditure:Number = strToNumber( donor.Expenditure );
			var maxYears:Number = strToNumber(  donor.Years );
			var rate:Number = new Number( parseFloat(donor.Yield) - parseFloat(donor.Inflation) );
			
			//calculate inv inc and savings
			var begBal:Number;
			var begBalw:Number;
			
			var totalInc:Number;
			var capGain:Number;
			var saving:Number;
			var gift:Number;
			var cumulativeGift:Number;
			var taxCredit:Number;
			
			var endBal:Number;
			var endBalw:Number; //with giving
			
			//init
			endBal = asset;
			endBalw = asset;
			cumulativeGift = 0;
			
			for (var i:uint = 0; i < maxYears; i++) {
				
				begBal 	= endBal;
				begBalw = endBalw;
				
				capGain = invInc( begBal, (rate/100));
				
				//no giving
				endBal			= begBal + capGain + income - expenditure;
				
                //giving 
				totalInc 		= income + capGain;
				gift 			=  strToNumber( donor.AnnualGiving );	
				if (i == 0) gift += strToNumber( donor.OnetimeGiving );//onetime giving in Year 1
				taxCredit 		= calculateTaxCredit(gift);
				saving 			= totalInc - expenditure - gift + taxCredit ;
				endBalw 		= begBalw + saving ;
				cumulativeGift += gift;
				
				var obj:Object = new Object();
				
	            obj.year 					= i+1;
	            obj.begBalance 				= begBal;
	            obj.begBalanceWithGiving 	= begBalw;
	            obj.capGain	                = capGain;
	            obj.totalIncome 			= totalInc;
				obj.totalIncome 			= totalInc;
				obj.giving 					= gift;
				obj.credit					= taxCredit;
				obj.cumulativeGiving 		= cumulativeGift;
				obj.savings 				= saving;
				obj.endBalance 				= endBal;
				obj.endBalanceWithGiving 	= endBalw;
	
	            _years.push( obj );
            }
			return _years;
        }
	}
}

