package com.fmi.gtdemo.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.gtdemo.event.ChangeViewEvent;
    import com.fmi.gtdemo.model.ViewModelLocator;
    
    public class SwitchAssetGrowthChartCommand implements ICommand, IResponder
    {
	public function SwitchAssetGrowthChartCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		//play audio clip
		model.soundStop();
		model.soundPlay('assets/audio/assetGrowth.mp3');
		
		model.numExpenses = (event as ChangeViewEvent).numExpenses;
		model.expenses = (event as ChangeViewEvent).expenses;
		model.currentView = ViewModelLocator.ASSET_GROWTH_CHART_VIEW;
		
        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();

            /* I think it is best not to use these static variables to indicate current state, 
            but since this example is supposed to follow Cairngorm's methodology, I am using
            them here */
            model.currentView = ViewModelLocator.ASSET_GROWTH_CHART_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}