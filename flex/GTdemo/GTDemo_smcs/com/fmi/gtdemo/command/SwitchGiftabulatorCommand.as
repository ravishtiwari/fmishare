package com.fmi.gtdemo.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.gtdemo.event.ChangeViewEvent;
    import com.fmi.gtdemo.model.ViewModelLocator;
    
    public class SwitchGiftabulatorCommand implements ICommand, IResponder
    {
	public function SwitchGiftabulatorCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		//play audio clip
		model.soundStop();
		model.soundPlay('assets/audio/giftabulator.mp3');
		
		model.currentView = ViewModelLocator.GIFTABULATOR_VIEW;	
		
        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();
            model.currentView = ViewModelLocator.GIFTABULATOR_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}