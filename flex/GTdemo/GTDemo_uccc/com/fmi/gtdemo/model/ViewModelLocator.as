package com.fmi.gtdemo.model
{
	import com.fmi.gtdemo.view.*;
	import com.adobe.cairngorm.model.ModelLocator;
   
	[Bindable]
	public class ViewModelLocator implements ModelLocator{
		private static var modelLocator : ViewModelLocator;
	     
		public static var FRONT_VIEW:uint = 0;
		public static var INTRO_VIEW:uint = 1;
		public static var LOCATION_VIEW:uint = 2;
		public static var ASSETS_VIEW:uint = 3;
		public static var RETURN_RATE_VIEW:uint = 4;
		public static var INCOME_VIEW:uint = 5;
		public static var EXPENDITURE_VIEW:uint = 6;
		public static var ASSET_GROWTH_CHART_VIEW:uint = 7;
		public static var DONATION_RATE_VIEW:uint = 8;
		public static var RESULTS_VIEW:uint = 9;
		public static var GIFTABULATOR_VIEW:uint = 10;
		public static var ABOUT_VIEW:uint = 11;
		public static var CONTACT_VIEW:uint = 12;
		
		//locaton
		public var location:String = "";
		public var country:String = "";
		
		//assets
		public var totalAssets:String = "";
		public var numTotalAssets:Number = 0;
		
		//returnRate
		public var returnRate:Number = 0;
		
		//income
		public var income:String = "";
		public var numIncome:Number = 0;
		
		//expenses
		public var expenses:String = "";
		public var numExpenses:Number = 0;
		
		//donationRate
		public var donationRate:Number = 0;
				
		//assign default view
		public var currentView:uint = FRONT_VIEW;
		
		
		
		/*-------------Sound-------------------------------*/
		
		import flash.media.Sound;
		import flash.media.SoundChannel;
		import flash.net.URLRequest;
			
		private static var soundClip:SoundChannel;
		private static var soundInitialized: Boolean = false;
		private static var soundOn: Boolean = true;

		
		
		public function soundPlay(soundPath:String):void {
			if (soundOn) {
				var request:URLRequest = new URLRequest(soundPath);
				var soundBite:Sound = new Sound();
				soundBite.load(request);
				soundClip = soundBite.play();
			}
		}
		
		public function soundFunctionOff():void {
			soundOn = false;
		}
		
		public function soundFunctionOn():void {
			soundOn=true;
		}
		
		public function soundStop():void {
			if (soundInitialized) {
				soundClip.stop();
			}
		}

		// soundStop is disabled until after a sound clip is first played
		public function initializeSound():void {
			soundInitialized = true;
		}
	
		/*-------------------------------------------------*/
	      
		public static function getInstance() : ViewModelLocator {
			if ( modelLocator == null ){
				modelLocator = new ViewModelLocator();
			}
			return modelLocator;
		}
	      
		public function ViewModelLocator() {    
			if ( modelLocator != null ) {
				throw new Error( "Only one Model instance should be instantiated" );    
			}
		}
		
		
   
	}

}
