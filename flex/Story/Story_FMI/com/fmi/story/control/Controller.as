package com.fmi.story.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.story.event.*;
    import com.fmi.story.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_ABOUT, SwitchAboutCommand );
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		addCommand( ChangeViewEvent.SWITCH_INTRO, SwitchIntroCommand );

		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}