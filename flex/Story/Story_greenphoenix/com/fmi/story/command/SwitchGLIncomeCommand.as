package com.fmi.story.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.story.event.ChangeViewEvent;
    import com.fmi.story.model.ViewModelLocator;
    
    public class SwitchGLIncomeCommand implements ICommand, IResponder
    {
	public function SwitchGLIncomeCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		//play audio clip
		model.soundStop();
		model.soundPlay('assets/audio/income.mp3');
		
		model.returnRate = (event as ChangeViewEvent).returnRate;
		model.currentView = ViewModelLocator.GL_INCOME_VIEW;
		
        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();

            
            model.currentView = ViewModelLocator.GL_INCOME_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}