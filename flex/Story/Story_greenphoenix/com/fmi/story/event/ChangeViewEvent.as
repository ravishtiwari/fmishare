package com.fmi.story.event
{
	import flash.events.Event;
	import com.adobe.cairngorm.control.CairngormEvent;


	public class ChangeViewEvent extends CairngormEvent
	{
		//syncronized with controller commands
		public static var SWITCH_FRONT:String = "switchFront";
		public static var SWITCH_INTRO:String = "switchIntro";
		public static var SWITCH_TENANTS:String = "switchTenants";
		public static var SWITCH_EXPANSION:String = "switchExpansion";
		public static var SWITCH_ENHANCEMENTS:String = "switchEnhancements";
		public static var SWITCH_COMMUNITY:String = "switchCommunity";
		public static var SWITCH_GALLERY:String = "switchGallery";
		public static var SWITCH_DONATE:String = "switchDonate";
		public static var SWITCH_HISTORY:String = "switchHistory";
		public static var SWITCH_CONTACT:String = "switchContact";
		
		public static var SWITCH_GL_ASSET_GROWTH_CHART:String = "switchGLAssetGrowthChart";
		public static var SWITCH_GL_ASSETS:String = "switchGLAssets";
		public static var SWITCH_GL_DONATION_RATE:String = "switchGLDonationRate";
		public static var SWITCH_GL_EXPENDITURE:String = "switchGLExpenditure";
		public static var SWITCH_GL_INCOME:String = "switchGLIncome";
		public static var SWITCH_GL_INTRO:String = "switchGLIntro";
		public static var SWITCH_GL_LOCATION:String = "switchGLLocation";
		public static var SWITCH_GL_RESULTS:String = "switchGLResults";
		public static var SWITCH_GL_RETURN_RATE:String = "switchGLReturnRate";
		
		public var country:String;
		public var location:String;
		public var totalAssets:String;
		public var numTotalAssets:Number;
		public var returnRate:Number;
		public var income:String;
		public var numIncome:Number;
		public var expenses:String;
		public var numExpenses:Number;
		public var donationRate:Number;

		/**
		 * Constructor.
		 */
		public function ChangeViewEvent(type:String)
		{
			super( type );
		}
		 
		 /**
		  * Override the inherited clone() method, but don't return any state.
		  */
		override public function clone() : Event
		{
			return new ChangeViewEvent(this.type);
		}    
	}
    
}