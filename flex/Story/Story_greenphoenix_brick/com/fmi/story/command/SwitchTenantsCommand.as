package com.fmi.story.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.story.event.ChangeViewEvent;
    import com.fmi.story.model.ViewModelLocator;
    
    public class SwitchTenantsCommand implements ICommand, IResponder
    {
	public function SwitchTenantsCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		model.soundStop();
		
		model.currentView = ViewModelLocator.TENANTS_VIEW;	
		
        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();
            model.currentView = ViewModelLocator.TENANTS_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "Could not switch to ContactView","Error" );
        }
    }

}
