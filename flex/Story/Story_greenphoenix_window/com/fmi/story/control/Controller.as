package com.fmi.story.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.story.event.*;
    import com.fmi.story.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_INTRO, SwitchIntroCommand );
		addCommand( ChangeViewEvent.SWITCH_TENANTS, SwitchTenantsCommand );
		addCommand( ChangeViewEvent.SWITCH_EXPANSION, SwitchExpansionCommand );
		addCommand( ChangeViewEvent.SWITCH_ENHANCEMENTS, SwitchEnhancementsCommand );
		addCommand( ChangeViewEvent.SWITCH_COMMUNITY, SwitchCommunityCommand );
		addCommand( ChangeViewEvent.SWITCH_GALLERY, SwitchGalleryCommand );
		addCommand( ChangeViewEvent.SWITCH_DONATE, SwitchDonateCommand );
		addCommand( ChangeViewEvent.SWITCH_HISTORY, SwitchHistoryCommand );
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		
		addCommand( ChangeViewEvent.SWITCH_GL_INTRO, SwitchGLIntroCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_ASSETS, SwitchGLAssetsCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_ASSET_GROWTH_CHART, SwitchGLAssetGrowthChartCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_DONATION_RATE, SwitchGLDonationRateCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_EXPENDITURE, SwitchGLExpenditureCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_INCOME, SwitchGLIncomeCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_LOCATION, SwitchGLLocationCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_RESULTS, SwitchGLResultsCommand);
		addCommand( ChangeViewEvent.SWITCH_GL_RETURN_RATE, SwitchGLReturnRateCommand);
		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}