package com.fmi.story.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.story.event.ChangeViewEvent;
    import com.fmi.story.model.ViewModelLocator;
    
    public class SwitchServicesCommand implements ICommand, IResponder
    {
	public function SwitchServicesCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
                       
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		// stop audio clip
		model.soundStop();
		model.soundFunctionOn();
		model.currentView = ViewModelLocator.SERVICES_VIEW;
        }
	
	public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();

            
            /* I think it is best not to use these static variables to indicate current state, 
            but since this example is supposed to follow Cairngorm's methodology, I am using
            them here */
            model.currentView = ViewModelLocator.SERVICES_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}
