package com.fmi.story.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.story.event.*;
    import com.fmi.story.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_ABOUT, SwitchAboutCommand );
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		addCommand( ChangeViewEvent.SWITCH_INTRO, SwitchIntroCommand );
		addCommand( ChangeViewEvent.SWITCH_WHO_WE_ARE, SwitchWhoWeAreCommand );
		addCommand( ChangeViewEvent.SWITCH_HISTORY, SwitchHistoryCommand );
		addCommand( ChangeViewEvent.SWITCH_SERVICES, SwitchServicesCommand );
		addCommand( ChangeViewEvent.SWITCH_INNOVATIONS, SwitchInnovationsCommand );
		addCommand( ChangeViewEvent.SWITCH_FOUNDATIONS, SwitchFoundationsCommand );
		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}