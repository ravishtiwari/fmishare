package com.fmi.story.event
{
	import flash.events.Event;
	import com.adobe.cairngorm.control.CairngormEvent;


	public class ChangeViewEvent extends CairngormEvent
	{
		//syncronized with controller commands
		public static var SWITCH_ABOUT:String = "switchAbout";
		public static var SWITCH_FRONT:String = "switchFront";
		public static var SWITCH_CONTACT:String = "switchContact";
		public static var SWITCH_WHO_WE_ARE:String = "switchWhoWeAre";
		public static var SWITCH_INTRO:String = "switchIntro";
		public static var SWITCH_HISTORY:String = "switchHistory";
		public static var SWITCH_SERVICES:String = "switchServices";
		public static var SWITCH_INNOVATIONS:String = "switchInnovations";
		public static var SWITCH_FOUNDATIONS:String = "switchFoundations";
		

		/**
		 * Constructor.
		 */
		public function ChangeViewEvent(type:String)
		{
			super( type );
		}
		 
		 /**
		  * Override the inherited clone() method, but don't return any state.
		  */
		override public function clone() : Event
		{
			return new ChangeViewEvent(this.type);
		}    
	}
    
}