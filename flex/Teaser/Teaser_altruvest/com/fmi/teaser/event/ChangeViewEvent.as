package com.fmi.teaser.event
{
	import flash.events.Event;
	import com.adobe.cairngorm.control.CairngormEvent;


	public class ChangeViewEvent extends CairngormEvent
	{
		//syncronized with controller commands
		public static var SWITCH_ABOUT:String = "switchAbout";
		public static var SWITCH_FRONT:String = "switchFront";
		public static var SWITCH_DETAILS1:String = "switchDetails1";
		public static var SWITCH_CONTACT:String = "switchContact";
		public static var SWITCH_DETAILS2:String = "switchDetails2";
		public static var SWITCH_DETAILS3:String = "switchDetails3";
		public static var SWITCH_DETAILS4:String = "switchDetails4";
		public static var SWITCH_DETAILS5:String = "switchDetails5";
		

		/**
		 * Constructor.
		 */
		public function ChangeViewEvent(type:String)
		{
			super( type );
		}
		 
		 /**
		  * Override the inherited clone() method, but don't return any state.
		  */
		override public function clone() : Event
		{
			return new ChangeViewEvent(this.type);
		}    
	}
    
}