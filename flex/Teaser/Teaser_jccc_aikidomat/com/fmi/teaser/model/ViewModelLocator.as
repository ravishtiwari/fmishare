package com.fmi.teaser.model
{
	import com.fmi.teaser.view.*;
	import com.adobe.cairngorm.model.ModelLocator;
   
	[Bindable]
	public class ViewModelLocator implements ModelLocator{
		private static var modelLocator : ViewModelLocator;
	     
		public static var FRONT_VIEW:uint = 0;
		public static var INTRO_VIEW:uint = 1;
		public static var FLY_THROUGH_VIEW:uint = 2;
		public static var ABOUT_VIEW:uint = 3;	
		public static var NAMING_OP_VIEW:uint = 4;
		public static var CONTACT_VIEW:uint = 5;
		
		//assign default view
		public var currentView:uint = FRONT_VIEW;
		
		/*-------------Sound-------------------------------*/
		
		import flash.media.Sound;
		import flash.media.SoundChannel;
		import flash.net.URLRequest;
			
		private static var soundClip:SoundChannel;		//  view specific sound
		private static var soundClipBG:SoundChannel;		// background music
		
		private static var soundInitialized: Boolean = false;
		private static var soundInitializedBG: Boolean = false;
		private static var soundOn: Boolean = true;
		


		
		/*--------------------View Specific Sound Controls-------------------------*/
		public function soundPlay(soundPath:String, soundType:String):void {
			
			var request:URLRequest = new URLRequest(soundPath);
			var soundBite:Sound = new Sound();
			soundBite.load(request);
			
			switch (soundType) {
				case "bgmusic":
					soundClipBG = soundBite.play();
					initializeSound("bgmusic");
					break;
				
				case "viewspecific":
					soundClip = soundBite.play();
					initializeSound("viewspecific");
					break;
			}
			
		}
		
		// disables sound use
		public function soundFunctionOff():void {
			soundOn = false;
		}
		
		// enables sound use
		public function soundFunctionOn():void {
			soundOn=true;
		}
		
		// called only after the sound has been played at least once
		public function soundStop(soundType:String):void {
			
				switch (soundType) {
					case "bgmusic" : 
						if (soundInitializedBG) {
							soundClipBG.stop();
							break;
						}
					
					case "viewspecific":
						if (soundInitialized) {
							soundClip.stop();
							break;
						}
				}
				
			
		}

		// soundStop is disabled until after a sound clip is first played
		public function initializeSound(value:String):void {
			switch (value) {
				case "bgmusic":
					soundInitializedBG=true;
					break;
				
				case "viewspecific":
					soundInitialized = true;
					break;
			}
			
		}	
		
		/*-------------------------------------------------------------------------------------*/
	      
		public static function getInstance() : ViewModelLocator {
			if ( modelLocator == null ){
				modelLocator = new ViewModelLocator();
			}
			return modelLocator;
		}
	      
		public function ViewModelLocator() {    
			if ( modelLocator != null ) {
				throw new Error( "Only one Model instance should be instantiated" );    
			}
		}
		
		
   
	}

}
