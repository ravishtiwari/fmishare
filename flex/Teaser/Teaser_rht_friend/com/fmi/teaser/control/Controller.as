package com.fmi.teaser.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.teaser.event.*;
    import com.fmi.teaser.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_ABOUT, SwitchAboutCommand );
		addCommand( ChangeViewEvent.SWITCH_INTRO, SwitchIntroCommand );
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_OPTION1, SwitchOption1Command);
		addCommand( ChangeViewEvent.SWITCH_OPTION2, SwitchOption2Command);
		addCommand( ChangeViewEvent.SWITCH_OPTION3, SwitchOption3Command);
		addCommand( ChangeViewEvent.SWITCH_SEAT_SALE, SwitchSeatSaleCommand);
		addCommand( ChangeViewEvent.SWITCH_FLY_THROUGH, SwitchFlyThroughCommand);
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}