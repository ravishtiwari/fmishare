package com.fmi.teaser.event
{
	import flash.events.Event;
	import com.adobe.cairngorm.control.CairngormEvent;


	public class ChangeViewEvent extends CairngormEvent
	{
		//syncronized with controller commands
		public static var SWITCH_ABOUT:String = "switchAbout";
		public static var SWITCH_FRONT:String = "switchFront";
		public static var SWITCH_INTRO:String = "switchIntro";
		public static var SWITCH_FLY_THROUGH:String = "switchFlyThrough";
		public static var SWITCH_OPTION1:String = "switchOption1";
		public static var SWITCH_OPTION2:String = "switchOption2";
		public static var SWITCH_OPTION3:String = "switchOption3";
		public static var SWITCH_SEAT_SALE:String = "switchSeatSale";
		public static var SWITCH_CONTACT:String = "switchContact";
		

		/**
		 * Constructor.
		 */
		public function ChangeViewEvent(type:String)
		{
			super( type );
		}
		 
		 /**
		  * Override the inherited clone() method, but don't return any state.
		  */
		override public function clone() : Event
		{
			return new ChangeViewEvent(this.type);
		}    
	}
    
}