package com.fmi.teaser.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.teaser.event.*;
    import com.fmi.teaser.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_ABOUT, SwitchAboutCommand );
		addCommand( ChangeViewEvent.SWITCH_INTRO, SwitchIntroCommand );
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_NAMING_OP, SwitchNamingOpCommand);
		addCommand( ChangeViewEvent.SWITCH_NAMING_OP2, SwitchNamingOp2Command);
		addCommand( ChangeViewEvent.SWITCH_FLY_THROUGH, SwitchFlyThroughCommand);
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}