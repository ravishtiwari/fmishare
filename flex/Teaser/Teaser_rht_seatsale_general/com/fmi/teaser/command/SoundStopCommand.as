package com.fmi.teaser.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.teaser.event.ChangeViewEvent;
    import com.fmi.teaser.model.ViewModelLocator;
    
    public class SoundStopCommand implements ICommand, IResponder
    {
	public function SoundStopCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
                       
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		model.soundFunctionOff();
		// stop audio clip
		model.soundStop();
		
        }
	
	public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();

        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}