package com.fmi.teaser.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.teaser.event.ChangeViewEvent;
    import com.fmi.teaser.model.ViewModelLocator;
    
    public class SwitchGeneralDonationsCommand implements ICommand, IResponder
    {
	public function SwitchGeneralDonationsCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		model.soundStop();
		
		model.currentView = ViewModelLocator.GENERAL_DONATIONS_VIEW;	
		
        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();
            model.currentView = ViewModelLocator.GENERAL_DONATIONS_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "Could not switch to NamingOp2View","Error" );
        }
    }

}
