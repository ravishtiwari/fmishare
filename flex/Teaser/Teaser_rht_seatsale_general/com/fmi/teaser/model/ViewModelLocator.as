package com.fmi.teaser.model
{
	import com.fmi.teaser.view.*;
	import com.adobe.cairngorm.model.ModelLocator;
   
	[Bindable]
	public class ViewModelLocator implements ModelLocator{
		private static var modelLocator : ViewModelLocator;
	     
		public static var FRONT_VIEW:uint = 0;
		public static var INTRO_VIEW:uint = 1;
		public static var FLY_THROUGH_VIEW:uint = 2;
		public static var ABOUT_VIEW:uint = 3;	
		public static var SEAT_SALE_VIEW:uint = 4;
		public static var SEAT_SALE2_VIEW:uint = 5;
		public static var CONTACT_VIEW:uint = 6;
		public static var GENERAL_DONATIONS_VIEW:uint = 7;
		
		//assign default view
		public var currentView:uint = FRONT_VIEW;
		
		/*-------------Sound-------------------------------*/
		
		import flash.media.Sound;
		import flash.media.SoundChannel;
		import flash.net.URLRequest;
			
		private static var soundClip:SoundChannel;
		private static var soundInitialized: Boolean = false;
		private static var soundOn: Boolean = true;

		
		
		public function soundPlay(soundPath:String):void {
			if (soundOn) {
				var request:URLRequest = new URLRequest(soundPath);
				var soundBite:Sound = new Sound();
				soundBite.load(request);
				soundClip = soundBite.play();
			}
		}
		
		public function soundFunctionOff():void {
			soundOn = false;
		}
		
		public function soundFunctionOn():void {
			soundOn=true;
		}
		
		public function soundStop():void {
			if (soundInitialized) {
				soundClip.stop();
			}
		}

		// soundStop is disabled until after a sound clip is first played
		public function initializeSound():void {
			soundInitialized = true;
		}
	
		/*-------------------------------------------------*/
	      
		public static function getInstance() : ViewModelLocator {
			if ( modelLocator == null ){
				modelLocator = new ViewModelLocator();
			}
			return modelLocator;
		}
	      
		public function ViewModelLocator() {    
			if ( modelLocator != null ) {
				throw new Error( "Only one Model instance should be instantiated" );    
			}
		}
		
		
   
	}

}
