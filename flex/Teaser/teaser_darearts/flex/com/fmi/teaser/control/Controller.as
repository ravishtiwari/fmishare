package com.fmi.teaser.control
{
    import com.adobe.cairngorm.control.FrontController;    
    import com.fmi.teaser.event.*;
    import com.fmi.teaser.command.*;

    public class Controller extends FrontController
    {
        public function Controller()
        {
		initialiseCommands();
        }
        
        public function initialiseCommands() : void
        {
		// Change view commands
		addCommand( ChangeViewEvent.SWITCH_ABOUT, SwitchAboutCommand );
		addCommand( ChangeViewEvent.SWITCH_DETAILS1, SwitchDetails1Command );
		addCommand( ChangeViewEvent.SWITCH_FRONT, SwitchFrontCommand );
		addCommand( ChangeViewEvent.SWITCH_CONTACT, SwitchContactCommand);
		addCommand( ChangeViewEvent.SWITCH_DETAILS2, SwitchDetails2Command);
		addCommand( ChangeViewEvent.SWITCH_DETAILS3, SwitchDetails3Command);
		addCommand( ChangeViewEvent.SWITCH_DETAILS4, SwitchDetails4Command);
		addCommand( ChangeViewEvent.SWITCH_DETAILS5, SwitchDetails5Command);
        addCommand( ChangeViewEvent.SWITCH_DETAILS6, SwitchDetails6Command);
        addCommand( ChangeViewEvent.SWITCH_DETAILS7, SwitchDetails7Command);
        addCommand( ChangeViewEvent.SWITCH_DETAILS8, SwitchDetails8Command);
        addCommand( ChangeViewEvent.SWITCH_DETAILS9, SwitchDetails9Command);
        addCommand( ChangeViewEvent.SWITCH_DETAILS10, SwitchDetails10Command);
        addCommand( ChangeViewEvent.SWITCH_DONATE, SwitchDonateCommand);
		
		// Change sound file commands
		addCommand( SoundControlEvent.SOUND_PLAY, SoundPlayCommand);
		addCommand( SoundControlEvent.SOUND_STOP, SoundStopCommand);
		
        }
    }
    
}