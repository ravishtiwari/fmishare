<?php
#variables from form
$email = $_POST["email"];
$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$phone = $_POST["phone"];
$numstudents = $_POST["numstudents"];

#set contact email 
$to = "jko@fundingmatters.com";
$subject = "DAREarts request for student sponsorship";
$body = "First Name: " . $firstname . "\n\nLast Name: " . $lastname . "\n\nPhone Number: " . $phone . "\n\nEmail Address: " . $email . "\n\nNumber of students to sponsor requested: " . $numstudents;


if (mail($to, $subject, $body)) {
    print "Request for student sponsorship has been successfully sent!  We will contact you as soon as possible.";
} else {
    print "Oops, there was a problem with the request.  Please try again.";
}
?>
