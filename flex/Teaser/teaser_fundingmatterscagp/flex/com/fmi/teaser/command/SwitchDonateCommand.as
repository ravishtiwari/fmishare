package com.fmi.teaser.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.teaser.event.ChangeViewEvent;
    import com.fmi.teaser.model.ViewModelLocator;
    
    public class SwitchDonateCommand implements ICommand, IResponder
    {
	public function SwitchDonateCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
            var model:ViewModelLocator = ViewModelLocator.getInstance();
            
            // play audio clip
            model.soundStop("viewspecific");		
            
            model.currentView = ViewModelLocator.DONATE_VIEW;	
		
        }
    
        public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();
            
            // play audio clip
            model.soundStop("viewspecific");
            
            model.currentView = ViewModelLocator.DONATE_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "uh oh","Error" );
        }
    }

}
