package com.fmi.teaser.command
{
    import mx.rpc.IResponder;
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    import mx.rpc.events.*;
    import mx.controls.Alert;

    import com.fmi.teaser.event.ChangeViewEvent;
    import com.fmi.teaser.model.ViewModelLocator;
    
    public class SwitchFrontCommand implements ICommand, IResponder
    {
	public function SwitchFrontCommand()
        {     
        }
    
        public function execute( event:CairngormEvent ):void {
                       
		var model:ViewModelLocator = ViewModelLocator.getInstance();
		
		// play audio clip
        //model.soundStop("viewspecific");
		//model.soundPlay('assets/audio/thatsamore.mp3', "viewspecific");
        
		//model.soundFunctionOn();
		model.currentView = ViewModelLocator.FRONT_VIEW;
        }
	
	public function result( event : Object ):void {                

            var model:ViewModelLocator = ViewModelLocator.getInstance();

            
            /* I think it is best not to use these static variables to indicate current state, 
            but since this example is supposed to follow Cairngorm's methodology, I am using
            them here */
            model.currentView = ViewModelLocator.FRONT_VIEW;
        }
    
        public function fault( event : Object ) : void
        {
            var faultEvent : FaultEvent = FaultEvent( event );
            Alert.show( "We couldn't contact the server to say Hello :(","Error" );
        }
    }

}