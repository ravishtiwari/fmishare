package
{
	
    [Bindable]
	public class DataModel{
		private static var model : DataModel;

        
        //profiles
        //the list of profiles/scenarios taken from the database
        public var currentView:Number = 0;   

        
        
        
		public static function getInstance() : DataModel {
			if ( model == null ){
				model = new DataModel();
			}
			return model;
		}
	      
		public function DataModel() {    
			if ( model != null ) {
				throw new Error( "Only one Model instance should be instantiated" );    
			}
		}

	}

}
