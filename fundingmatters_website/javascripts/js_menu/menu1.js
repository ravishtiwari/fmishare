/* COOLjsMenu style + structure file */
/*For html files in ishare_project folder*/
/* wendy's reference:  menu.js for subfolders; different from index menu.js */

var STYLE_0 =
{

    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 26, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-0px', 0 ]
};



var SUB_STYLE =
{
	borderWidth:1,
    borderColor:"#bb6b24",
    backgroundColor:[ "black", "#d67e21" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 26, 100 ],
    itemoff:[ '+previousItem-1px', 0 ]
    /*eveloff:[ '+parentItem+1px', 0 ]*/
}

var color = {border:"#666666", bgON:"white",bgOVER:"#B6BDD2"};
var css = {ON:"clsCMOn", OVER:"clsCMOver"};

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0], blankImage:'http://smcscampaign.ishare.ca/js_menu/images/b.gif'},
    {code:"", size:[ 26, 110 ], url:"../home/our_company.htm",
		sub:[
			{style:SUB_STYLE, size:[24,130]},
			{code:"Our Company", url:"../home/our_company.htm", sub:[]},
			{code:"Our Approach", url:"../home/our_approach.htm", sub:[]},
			{code:"Our Team", url:"../home/our_team.htm", sub:[]},
			{code:"Careers", url:"../home/careers.htm", sub:[]},
			{code:"FAQ", url:"../home/faq.htm", sub:[]}
		]
    },
    {code:"", size:[26, 132], url:"../oursuccess/oursuccess.htm",
		sub:[
			{style:SUB_STYLE, size:[24,130]},
			{code:"Successes", url:"../oursuccess/oursuccess.htm", sub:[]},
			{code:"Our Clients", url:"../oursuccess/ourclients.htm", sub:[]},
			{code:"Testimonials", url:"../oursuccess/testimonials.htm", sub:[]}
		]
    },
    {code:"", size:[26,100], url:"../services/services.htm", 
		sub:[
			{style:SUB_STYLE, size:[24,200]},
			{code:"Our Services", url:"../services/services.htm", sub:[]}, 
			{code:"Capital Campaign", url:"../services/capital_campaign.htm", sub:[]}, 
			{code:"Annual Campaign", url:"../services/annual_campaign.htm", sub:[]},
			{code:"Planned Giving", url:"../services/planned_giving.htm", sub:[]},
			{code:"Public Private Partnerships", url:"../services/public_private_partnerships.htm", sub:[]},
			{code:"Case For Support", url:"../services/case_for_support.htm", sub:[]},
			{code:"Research", url:"../services/research.htm", sub:[]}
		]
    },
		{code:"", size:[26,110], url:"../technology/technology.htm",
		sub:[
			{style:SUB_STYLE, size:[24,200]},
			{code:"Innovations", url:"../technology/technology.htm", sub:[]}, 
			{code:"Website Design Works", url:"../technology/webdesign.htm", sub:[]}, 
			{code:"GIFTABULATOR<sup>&#174;<sup>", url:"../technology/giftabulator.htm", sub:[]}, 
			{code:"FMIQ", url:"../technology/fmiq.htm", sub:[]},
			{code:"iShare Suite", url:"../technology/ishare.htm", sub:[]},
			{code:"Online Community", url:"../technology/online_community.htm", sub:[]},
			{code:"Logistix", url:"../technology/logistix.htm", sub:[]}
		]
	},

        {code:"", size:[26,125], url:"../multimedia/news.htm",
		sub:[
			{style:SUB_STYLE, size:[24,130]},
			{code:"News", url:"../multimedia/news.htm", sub:[]},
			{code:"Events", url:"../multimedia/events.htm", sub:[]},
			{code:"Videos", url:"../multimedia/videos.htm", sub:[]},
			{code:"Gallery", url:"../multimedia/gallery.htm", sub:[]}
		]
	},

		{code:"", size:[26,112], url:"../resources/resources.htm",
		sub:[
			{}
		]

	},

		{code:"", size:[26,111], url:"../contactus/contactus.htm",
		sub:[
			{}
		]

	}

];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
