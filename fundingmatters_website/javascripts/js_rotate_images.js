		var slideShowSpeed = 7000
		var crossFadeDuration = 3

		var Pic = new Array() // don't touch this

		Pic[0] = '../home/images/banner_home.jpg'
		Pic[1] = '../home/images/banner_carecentre.jpg'
		Pic[2] = '../home/images/banner_volunteers.jpg'
		Pic[3] = '../home/images/banner_fundraising.jpg'

		// =======================================
		// do not edit anything below this line
		// =======================================

		var t
		var j = 0
		var p = Pic.length

		var preLoad = new Array()
		for (i = 0; i < p; i++){
		   preLoad[i] = new Image()
		   preLoad[i].src = Pic[i]
		}

		function runSlideShow(){
		   if (document.all){
			  document.images.SlideShow.style.filter="blendTrans(duration=2)"
			  document.images.SlideShow.style.filter="blendTrans(duration=crossFadeDuration)"
			  document.images.SlideShow.filters.blendTrans.Apply()      
		   }
		   document.images.SlideShow.src = preLoad[j].src
		   if (document.all){
			  document.images.SlideShow.filters.blendTrans.Play()
		   }
		   j = j + 1
		   if (j > (p-1)) j=0
		   t = setTimeout('runSlideShow()', slideShowSpeed)
		}