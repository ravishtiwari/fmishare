#!/usr/bin/perl
print "Content-Type: text/html\n\n";

use DBI;
use CGI ":standard";
my($cgi)=new CGI;

$personUsername=$cgi->param("personUsername");
$personPassword=$cgi->param("personPassword");
#$personUsername="admin";
#$personPassword="dev";

my $dbh = DBI->connect("DBI:mysql:funding_fundingmatters:localhost","funding_isharead", "que9rtf") or die "Couldn't connect to database: " . DBI->errstr;
my $sth = $dbh->prepare('SELECT * FROM CONTACTLIST') or die "Couldn't prepare statement: " . $dbh->errstr;
$sth->execute ();



sub registration_table{
print <<MYBODY6;
<table border='1' width='200'>
 <tr width='200'>
	<td>Created At</td>
	<td>First Name</td>
	<td>Last Name</td>
	<td>Organization</td>
	<td>Email</td>
	<td>Phonenumber</td>
	<td>Registration Event</td>
</tr>
MYBODY6

 # read results of query, then clean up 
 while (@ary = $sth->fetchrow_array ()){ 
	print "  <tr>\n"; 
	#foreach(@ary){ 
		#print "<td>$_</td>\n"; 
		my $registered_at = $ary[2];
		my $fname = $ary[3];
		my $lname = $ary[4];
		my $organization = $ary[5];
		my $email_address = $ary[6];
		my $contact_number = $ary[7];
		my $mycomment = $ary[8];
		
		print "<td>$registered_at</td><td>$fname</td><td>$lname</td><td>organization</td><td>$email_address</td><td>$contact_number</td><td>$mycomment</td>\n";
	#} 
	print "  </tr>\n"; 
 } 
print "</table>\n";
}




if(lc($personUsername) eq "admin" && lc($personPassword) eq "dev"){
	&header;
	&mainBodyTop;
	&affirmativeBody;
	&registration_table;
	&mainBodyBottom;
	&footer;

}else{
	&header;
	&mainBodyTop;
	&negativeBody;
	&mainBodyBottom;
	&footer;
}


sub affirmativeBody{

print <<MYBODY1;
<span class="bold-text-heading">Administrator Control Panel</span>

				<p>User:Admin</p>
				<p>Registration information</p>

MYBODY1
}

sub negativeBody{

print <<MYBODY1;
<span class="bold-text-heading">Access Denied</span>

				<p>Please re-enter your username and password to login</p>

		<form method="post" action="http://fundingmatters.com/multimedia/login.pl" name="message">
			<table class="signup-form" width="100%" align="center" height="429" cellspacing="0">
			  <tr valign="top"> 
			    <td valign="top"> 
			      <table width="467" border="0" cellspacing="0">
						<tr> 
							<td width="180">Username:&nbsp;</td>
							<td width="269"><input type="text" name="personUsername" value="" size="31" maxlength="90"></td>
						</tr>
						<tr> 
							<td width="180">Password:&nbsp;</td>
							<td width="269"><input type="password" name="personPassword" value="" size="31" maxlength="90"></td>
						</tr>
					</table>
					<br/><br/>
					<input type="reset" value="RESET">
			      <input type="submit" value="SUBMIT" onClick="">
				</td>
			</tr>
		</table>
	</form>

MYBODY1
}

sub header{

print <<MYBODY1;
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>FUNDING matters inc - Fundraising Consultants</title>


		<meta name="keywords" content="Fundraising consultants, fundraise, philanthropic, charitable giving, wealth management, social venture partnership, major gift campaigns, tax benefit, nonprofit, non-profit, charity, planned giving strategies" />
		<meta name="description" content="Fundraising consultants, expert capacity builders, online surveys for non-profits, informing decision making, developing communities in Canada and abroad." />
		<!-- Start of StatCounter Code -->
<script type="text/javascript">
sc_project=1272181; 
sc_invisible=1; 
sc_partition=11; 
sc_security="e6febdde"; 
</script>


<script language="JavaScript">
<!--
function checkForm() {  
	var found = 0;
	document.message.url.value=trim(document.message.url.value);
	document.message.title.value=trim(document.message.title.value);
	document.message.descript.value=trim(document.message.descript.value);
	document.message.personFirstName.value=trim(document.message.personFirstName.value);
	document.message.personLastName.value=trim(document.message.personLastName.value);
 

	if(document.message.personFirstName.value==""){
		alert("Please enter your first name");
		document.message.personFirstName.focus();
		return false;
	}
	if(document.message.personLastName.value==""){
		alert("Please enter your last name");
		document.message.personLastName.focus();
		return false;
	}
	if (document.message.personEmail.value.indexOf(/\s@\s.\s\s\s/)==-1){
		alert("Please enter a valid email address");
		document.message.personEmail.focus();
		return false;
	}
}

function trim(value) {
	startpos=0;
	while((value.charAt(startpos)==" ")&&(startpos<value.length)) {
		startpos++;
	}
	if(startpos==value.length) {
		value="";
	}
	else {
		value=value.substring(startpos,value.length);
		endpos=(value.length)-1;
		while(value.charAt(endpos)==" ") {
			endpos--;
		}
		value=value.substring(0,endpos+1);
	}
	return(value);
}
//-->
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript><div class="statcounter"><a href="http://www.statcounter.com/free_hit_counter.html" target="_blank"><img class="statcounter" src="http://c12.statcounter.com/1272181/0/e6febdde/1/" alt="counter free hit invisible" ></a></div></noscript>
<!-- End of StatCounter Code -->

		<link rel="stylesheet" type="text/css" href="../css/default.css" />
		<script type="text/javascript" src="../javascripts/COOLjsMenu_Professional_2.9.4/Scripts/coolmenupro.js"></script>
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/style.css" /> 

	</head>
<body>
MYBODY1
}

sub mainBodyTop{
print <<MYBODY1;
<div class="mainContainer">

	<table class="header-container" cellspacing="0" cellpadding="0">
		<tr>
			</td>

					<a href="../index.htm"><img class="fmi-banner-logo" src="../images/fmi_banner_logo.gif" alt="fmi_banner_logo"/></a>
			</td>
		</tr>
		<tr class="navigation_newsandmedia">
			<td>
				<script type="text/javascript" src="../javascripts/js_menu/menu1.js"></script>
			</td>
		</tr>
	</table>

	<table class="header-divider" cellpadding="0" cellspacing="0">
		<tr>
			<td class="header-divider">
				<img class="header-divider" src="../images/header_divider.png"/>
			</td>
		</tr>
	</table>

	<table class="header-container" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<img class="mainbanner" src="../images/multimedia_banner.png"/>
			</td>
		</tr>
	</table>
	<table class="main-body-container" cellspacing="0" cellpadding="0">
		<tr class="main-body-container">
			<td>
						
			<table class="news-table">
				<tr>
				<td class="news-table-article">
MYBODY1
}

sub mainBodyBottom{

print <<MYBODY1;
   </td>
			
		</tr>
	</table>

			</td>
		</tr>
	</table>


	<table cellspacing="0" cellpadding="0" align="right">
		<tr>
			<td class="footer" >
				2008 FUNDINGmatters Inc., All rights reserved<br/>
			</td>
		</tr>
	</table>

</div>
MYBODY1

}

sub footer{
	print "</body>";
	print "</html>";
	#$sth->finish (); 
	$dbh->disconnect(); 
	#exit (0);
}

