#!/usr/bin/perl
print "Content-Type: text/html\n\n";

use DBI;
use CGI ":standard";
my($cgi)=new CGI;

my $dbh = DBI->connect("DBI:mysql:funding_fundingmatters:localhost","funding_isharead", "que9rtf") or die "Couldn't connect to database: " . DBI->errstr;

#$sth1 = $dbh->prepare("INSERT INTO REGISTRATION(FIRSTNAME,LASTNAME,ORGANIZATION,EMAIL,PHONE,COMMENT,REMINDER,SIGNUP_NEWSLETTER) VALUES (?,?,?,?,?,?,?,?)");

$sth1 = $dbh->prepare("INSERT INTO CONTACTLIST(FIRSTNAME,LASTNAME,ORGANIZATION,EMAIL,PHONE,REMINDER,SIGNUP_NEWSLETTER, COMMENT) VALUES (?,?,?,?,?,?,?,?)");

$seminarDate=$cgi->param("registrationType");
$registrationType=$cgi->param("registrationType");

$personFirstName=$cgi->param("personFirstName");
$personLastName=$cgi->param("personLastName");
$personOrganization=$cgi->param("personOrganization");
$personEmail=$cgi->param("personEmail");
$personContact=$cgi->param("personContact");
#$description=$cgi->param("descript");
$newsletter=$cgi->param("newsletter");
$reminder=$cgi->param("reminder");

$defaultReminder="NO";
$defaultSignupNewsletter="NO";

#&check_checkbox;

#sub check_checkbox{
#	if($cgi->param($formdata['newsletter']) eq 'YES'){
#		$test = $cgi->param($formdata['newsletter']);
#		print "checkbox selected = $test";
#	} else {
#		print "checkbox  not selected = $test";
#	}
#}

#print "$personFirstName,$personLastName,$personOrganization,$personEmail",
#$sth1->execute($personFirstName, $personLastName, $personOrganization, $personEmail, $personContact, $description, $defaultSignupNewsletter, $defaultReminder);

$sth1->execute($personFirstName, $personLastName, $personOrganization, $personEmail, $personContact, $defaultReminder, $defaultSignupNewsletter, $registrationType);



$dbh->disconnect;
&header;
&mainBodyTop;

print <<MYBODY1;
<font size="2">Thank you, <span class="text-gold">$personFirstName</span> for signing up to FUNDING matters Inc. $seminarDate
<p>The information you submitted are the following:</p>

<span class="text-gold">You have signed up:  </span>$registrationType<br/>
<span class="text-gold">Frist Name:  </span> $personFirstName<br/>
<span class="text-gold">Last Name:  </span> $personLastName<br/>
<span class="text-gold">Organization:  </span> $personOrganization<br/>
<span class="text-gold">Email:  </span> $personEmail<br/>
<span class="text-gold">Telephone Number:  </span> $personContact<br/>
 
<br/>
<p>Thank you for your interest in attending the FUNDING matters Inc. seminar.  We will contact you shortly</p>

</font>

MYBODY1

&mainBodyBottom;
&footer;

sub header{

print <<MYBODY1;
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>FUNDING matters inc - Fundraising Consultants</title>


		<meta name="keywords" content="Fundraising consultants, fundraise, philanthropic, charitable giving, wealth management, social venture partnership, major gift campaigns, tax benefit, nonprofit, non-profit, charity, planned giving strategies" />
		<meta name="description" content="Fundraising consultants, expert capacity builders, online surveys for non-profits, informing decision making, developing communities in Canada and abroad." />
		<!-- Start of StatCounter Code -->
<script type="text/javascript">
sc_project=1272181; 
sc_invisible=1; 
sc_partition=11; 
sc_security="e6febdde"; 
</script>


<script language="JavaScript">
<!--
function checkForm() {  
	var found = 0;
	document.message.url.value=trim(document.message.url.value);
	document.message.title.value=trim(document.message.title.value);
	document.message.descript.value=trim(document.message.descript.value);
	document.message.personFirstName.value=trim(document.message.personFirstName.value);
	document.message.personLastName.value=trim(document.message.personLastName.value);
 

	if(document.message.personFirstName.value==""){
		alert("Please enter your first name");
		document.message.personFirstName.focus();
		return false;
	}
	if(document.message.personLastName.value==""){
		alert("Please enter your last name");
		document.message.personLastName.focus();
		return false;
	}
	if (document.message.personEmail.value.indexOf(/\s@\s.\s\s\s/)==-1){
		alert("Please enter a valid email address");
		document.message.personEmail.focus();
		return false;
	}
}

function trim(value) {
	startpos=0;
	while((value.charAt(startpos)==" ")&&(startpos<value.length)) {
		startpos++;
	}
	if(startpos==value.length) {
		value="";
	}
	else {
		value=value.substring(startpos,value.length);
		endpos=(value.length)-1;
		while(value.charAt(endpos)==" ") {
			endpos--;
		}
		value=value.substring(0,endpos+1);
	}
	return(value);
}
//-->
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript><div class="statcounter"><a href="http://www.statcounter.com/free_hit_counter.html" target="_blank"><img class="statcounter" src="http://c12.statcounter.com/1272181/0/e6febdde/1/" alt="counter free hit invisible" ></a></div></noscript>
<!-- End of StatCounter Code -->

		<link rel="stylesheet" type="text/css" href="../css/default.css" />
		<script type="text/javascript" src="../javascripts/COOLjsMenu_Professional_2.9.4/Scripts/coolmenupro.js"></script>
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/style.css" /> 

	</head>
<body>
MYBODY1
}

sub mainBodyTop{
print <<MYBODY1;
<div class="mainContainer">

	<table class="header-container" cellspacing="0" cellpadding="0">
		<tr>
			</td>

					<a href="../index.htm"><img class="fmi-banner-logo" src="../images/fmi_banner_logo.gif" alt="fmi_banner_logo"/></a>
			</td>
		</tr>
		<tr class="navigation_newsandmedia">
			<td>
				<script type="text/javascript" src="../javascripts/js_menu/menu1.js"></script>
			</td>
		</tr>
	</table>

	<table class="header-divider" cellpadding="0" cellspacing="0">
		<tr>
			<td class="header-divider">
				<img class="header-divider" src="../images/header_divider.png"/>
			</td>
		</tr>
	</table>

	<table class="header-container" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<img class="mainbanner" src="../images/multimedia_banner.png"/>
			</td>
		</tr>
	</table>
	<table class="main-body-container" cellspacing="0" cellpadding="0">
		<tr class="main-body-container">
			<td>
						
			<table class="news-table">
				<tr>
				<td class="news-table-article">
MYBODY1
}

sub mainBodyBottom{

print <<MYBODY1;
   </td>
			<td class="top-align">
				<script type="text/javascript">
					var xmlDoc=null;
					if (window.ActiveXObject){// code for IE
						xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
					}else if (document.implementation.createDocument){// code for Mozilla, Firefox, Opera, etc.
						xmlDoc=document.implementation.createDocument("","",null);
					}else{
						alert('Your browser cannot handle this script');
					}

					if (xmlDoc!=null){
						xmlDoc.async=false;
						xmlDoc.load("news_articles.xml");
						var x=xmlDoc.getElementsByTagName("ARTICLE");
						document.write("<ul>");
						for (var i=0;i<x.length;i++){ 
							document.write("<li>");
							document.write("<a href='" + x[i].getElementsByTagName("URLLINK")[0].childNodes[0].nodeValue + "'>");
							document.write(x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue + 
							" <span class='very-small-text-white'>" + 
							x[i].getElementsByTagName("DATE")[0].childNodes[0].nodeValue + ", " +
							x[i].getElementsByTagName("YEAR")[0].childNodes[0].nodeValue +
							"<span" +
							"</a></li>");
						}
						document.write("</ul>");
					}
				</script>	
			</td>
		</tr>
	</table>

			</td>
		</tr>
	</table>


	<table cellspacing="0" cellpadding="0" align="right">
		<tr>
			<td class="footer" >
				2008 FUNDINGmatters Inc., All rights reserved<br/>
			</td>
		</tr>
	</table>

</div>
MYBODY1

}

sub footer{
	print "</body>";
	print "</html>";
}

