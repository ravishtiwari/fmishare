﻿<?php include "header.php";?>
<div class="body">
	<h3>Annual Giving Services</h3>
	<p>
		A productive annual giving program is essential to the long-term success and sustainability of an advancement program. While new media, information technology, and a changing economy have created both challenges and opportunities for many non-profit organizations, we at FUNDING matters are committed to working with our clients to develop annual giving strategies that engage donors and build lasting communities.
	</p>
	<p>
		To help our clients understand and implement the latest trends and best practices in annual giving, FUNDING matters is pleased to offer the following services:
	</p>
	<ul>
		<li>Annual Giving Program Assessment
		<li>Online Engagement Strategy (Social Media, Email, Web Design)
		<li>Analytics (Predictive Modeling, Benchmarking, Surveys, Census)
		<li>Communications (Appeal Writing, Script Development, Website Design)
		<li>Training for Callers, Staff, and Volunteers
		<li>Mentoring and Coaching
		<li>Interim Program and Project Management
	</ul>
	<p>
		<a href="javascript: history.go(-1)">Click to go back</a>
	</p>
</div>
<?php include "footer.php";?>