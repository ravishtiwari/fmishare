﻿<?php include "header.php";?>
<div class="body">
	<h3>Campaigns</h3>
	<p>
		Campaigns are a powerful strategic tool to address essential institutional needs and opportunities, and to galvanize donor commitment within a specific time frame. When well planned and executed, they elevate fundraising performance and enlist the support of all constituencies.
	</p>
<p>
With $63 million in campaigns that range from $5 million to $15 million currently under counsel, FUNDING matters is exceptionally well qualified to assist you in planning, implementing and evaluating your next campaign. We can assist with smaller campaigns as well.
</p>
<p>
FUNDING mattes is particularly well known for our expertise in campaign readiness assessments and strategic planning studies (also known as feasibility studies), and the reliability of our recommendations about campaign goals. FUNDING matters can assist you with all aspects of campaigns, from preparation through implementation to post-campaign assessment.
</p>
<ul>
	<li>Needs assessment and priority setting
	<li>Campaign readiness assessment
	<li>Strategic planning studies
	<li>Case for support
	<li>Campaign branding and marketing
	<li>Donor identification, cultivation and solicitation
	<li>Volunteer and staff development
	<li>Campaign planning and implementation
	<li>Post-campaign assessment
</ul>
	<p>
		<a href="javascript: history.go(-1)">Click to go back</a>
	</p>
</div>
<?php include "footer.php";?>