﻿<?php include "header.php";?>

<div class="body">
	<h1>Contact Us</h1>
</div>

<div class="containertable">

<div class="left">
	<div class="contactleft">

		 <form name="form1" method="post" action="/cgi-sys/FormMail.pl" class="contact">
				<input type=hidden name="recipient" value="wpetruck@fundingmatters.com">
				<input type=hidden name="redirect" value="http://www.fundingmatters.com/contactus/messagesent.htm">
				<input type=hidden name=subject value="FUNDING matters &reg; Inc. Contact Us Comments">
				<input type=hidden name=required value="email,realname">

				<label for="name">Name:</label><input name="realname" type="text" id="name"><br/>
				<label for="organization">Organization:</label><input name="organization" type="text" id="organization"><br/>
				<label for="phone">Phone:</label><input name="phone" type="text" id="phone"><br>
				<label for="email">E-mail:</label><input name="email" type="text" id="eMail"><br>
				<label for="question">Message:</label><textarea name="comment" cols="35" rows="10" id="comment"></textarea><br/><br/>
				<input type="submit" class="submit" value="Submit" />
		</form>
	</div>
</div>

<div class="right">
	<div class="contactright">
		<b>tel:</b> (416) 249-0788<br>
		<b>toll-free:</b> 1 (800) 856-1354<br>
		<!--<b>fax:</b> (416) 249-8148<br>-->
		<b>e-mail:</b> <a href="mailto:info@fundingmatters.com">info@fundingmatters.com</a><br>
		<b>blog:</b> <a href="http://fundingmatters.blogspot.com" target="_blank">fundingmatters.blogspot.com</a>
		<br><br>
		<b>address:</b><br>
		FUNDING matters &reg; inc.<br>
		333 Dundas Street E.<br>
		Toronto, Ontario, M5A 2A2
		<br><br>
		<a href="http://www.linkedin.com/company/funding-matters-inc." target="_blank"><img src="img/linkedin.png" width="17"></a>
		<a href="https://www.facebook.com/pages/FUNDING-matters-Inc/251045471579185" target="_blank"><img src="img/facebook.png" width="18"></a>
		<a href="https://twitter.com/#!/FUNDING_matters" target="_blank"><img src="img/twitter.png" width="18"></a>
		<br><br><a href="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=333+Dundas+St+E,+Toronto,+ON+M5A+2A2,+Canada&aq=0&sll=37.0625,-95.677068&sspn=38.41771,86.572266&ie=UTF8&hq=&hnear=333+Dundas+St+E,+Toronto,+Ontario+M5A+2A2,+Canada&z=16">
		<img src="img/mapr.png">
		</a>
		</div>
</div>
</div>


<?php include "servicestext.php";?>

<?php include "footer.php";?>