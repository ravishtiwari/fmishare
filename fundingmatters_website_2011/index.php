<!DOCTYPE html> 
	<head>
		<meta charset="utf-8">
		<title>FUNDING matters inc.</title>
		<link rel="icon" type="image/png" href="favicon.ico">
		<link rel="stylesheet" type="text/css" href="style.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<link href="http://fonts.googleapis.com/css?family=Muli:300,300italic|Nunito:700" rel="stylesheet" type="text/css">
		
		
		<!-- adding more code to header.php only for the index page -->
		
		<script type="text/javascript">

		// reset images
		function reset_images(){
			document.getElementById('william_petruck').src="img/alicia1.jpg";
			document.getElementById('stephen_lavoie').src="img/alicia1.jpg";
			document.getElementById('wendy_tran').src="img/alicia1.jpg";
			document.getElementById('sarah_burd').src="img/alicia1.jpg";
			document.getElementById('alicia_moore').src="img/alicia1.jpg";
			document.getElementById('laura_ashley').src="img/alicia1.jpg";
			document.getElementById('tina_holly').src="img/alicia1.jpg";
		}


		// bio stuff
        function show_bio(bio_name){
             bio_william_petruck = "<h3>William Petruck</h3><h6>President and CEO</h6>hello my name is bill.  i'm da boss.";
             bio_stephen_lavoie = "<h3>Stephen Lavoie</h3><h6>Director of Business Development</h4>hello there.  the names stephen, stephen lavoie.";
			 bio_wendy_tran = "<h3>Wendy Tran</h3><h6>WHO IS SHE??</h6>";
             bio_sarah_burd = "<h3>Sarah Burd</h3><h6>Campaign Associate</h6>hello there.  the names burd, sarah burd. like burd's fishing.";
             bio_alicia_moore = "<h3>Alicia Moore</h3><h6>Design and Brand Communications Manager</h6>hello my name is alicia moore. P!nk and I have the same name.";
			 bio_laura_ashley = "<h3>Laura Ashley</h3><h6>Consultant</h6>Hi, my name is Laura Ashley.";
			 bio_tina_holly = "<h3>Tina Holly</h3><h6>Translator & Web Designer</h6>Hi, my name is Tina.";

             if (bio_name == "william_petruck"){
				  document.getElementById('william_petruck').src="img/orange1.jpg";
                  document.getElementById('bio_section').innerHTML = bio_william_petruck;
             }else if (bio_name == "stephen_lavoie"){
				  document.getElementById('stephen_lavoie').src="img/orange1.jpg";
                  document.getElementById('bio_section').innerHTML = bio_stephen_lavoie;    
             }else if (bio_name == "wendy_tran"){
				  document.getElementById('wendy_tran').src="img/orange1.jpg";
                  document.getElementById('bio_section').innerHTML = bio_wendy_tran;
             }else if (bio_name == "sarah_burd"){
				  document.getElementById('sarah_burd').src="img/orange1.jpg";
                  document.getElementById('bio_section').innerHTML = bio_sarah_burd;      
             }else if (bio_name == "alicia_moore"){
				  document.getElementById('alicia_moore').src="img/orange1.jpg";
                  document.getElementById('bio_section').innerHTML = bio_alicia_moore;
             }else if (bio_name == "laura_ashley"){
				  document.getElementById('laura_ashley').src="img/orange1.jpg";
                  document.getElementById('bio_section').innerHTML = bio_laura_ashley; 
			 }else if (bio_name == "tina_holly") {
				  document.getElementById('tina_holly').src="img/orange1.jpg";
				 document.getElementById('bio_section').innerHTML = bio_tina_holly;
			 }      

        }

        </script>

		<!-- end differences between header.php and this page -->

	</head>

	<body onLoad="show_bio('william_petruck');">

<div id="container">

	<div class="padding">



		<div id="header">
			
			<?php include "navigation.php" ;?>

			<a href="../"><img src="../img/logotext.png" width="550" class="logotext">
			<img src="../img/fmilogo.png" width="200" class="logo"></a>

		</div>



		<div id="cloudimg">
			cloudimg 


		</div>

		


		<div class="mm">
			<div class="missionbox">
				<p class="mission">
					<span class="o">Our mission</span><br>is to create a sustainable <br><span class="b">culture of philanthropy</span> <br>by advancing best practices in client environments.
				</p>
			</div>

			<div class="missionbox2">

				
					Mission stuff explained

			</div>
		</div>

		<div class="mm">
			<div class="leftsmall">
			something
			</div>

			<div class="rightbig">
			<h3>Our Vision</h3>
			<p align="right">
				Create sustainable, extraordinary philanthropy to advance non-profits and their missions.
			</p>
			</div>
			

		</div>

		<div id="blank">
		blank
		</div>

		
		<div class="mm">
			
			<div class="leftsmall2">
				<h3>Blog</h3>
				<div class="blog">
					<script src="http://feeds.feedburner.com/FundingMattersInc-CharitableGivingThroughEstateAndPhilanthropicPlanning?format=sigpro" type="text/javascript" ></script><noscript><p>Subscribe to RSS headline updates from: <a href="http://feeds.feedburner.com/FundingMattersInc-CharitableGivingThroughEstateAndPhilanthropicPlanning"></a><br/>Powered by FeedBurner</p> </noscript>
				</div>
			</div>
			
			<div class="rightbig2">
				<h3>About Us</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor. 
				</p>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor.
				</p> 
			</div>
		</div>
				
		
		<div class="mm">
			<div class="leftsmall2">
				<div class="achievements">
					<p>
					Achievements?
					</p>
					
					<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor. 
					</p>
					<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor.
					</p> 
					<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor.
					</p>
					<p>
					Stuff must keep going for now.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor.
					</p>
				</div>
				
			</div>
			


			<div id="bio_container">
				<div id="bio_title">
					<h1>Our Team</h1>
				</div>
				<div id="bio_pics">
				<ul class="bioimg">
					<li><a href="JavaScript:reset_images();show_bio('william_petruck');"><img id="william_petruck" src="img/alicia1.jpg" class="bioimg" alt="William Petruck" width="120" height="120"></a>
					<li><a href="JavaScript:reset_images();show_bio('stephen_lavoie');"><img id="stephen_lavoie" src="img/alicia1.jpg" class="bioimg" alt="Stephen Lavoie" width="120" height="120"></a>
					<li><a href="JavaScript:reset_images();show_bio('wendy_tran');"><img id="wendy_tran" src="img/alicia1.jpg" class="bioimg" alt="Wendy Tran" width="120" height="120"></a>
					<li><a href="JavaScript:reset_images();show_bio('sarah_burd');"><img id="sarah_burd" src="img/alicia1.jpg" class="bioimg" alt="Sarah Burd" width="120" height="120"></a>
					<li><a href="JavaScript:reset_images();show_bio('alicia_moore');"><img id="alicia_moore" src="img/alicia1.jpg" class="bioimg" alt="Alicia Moore" width="120" height="120"></a>
					<li><a href="JavaScript:reset_images();show_bio('laura_ashley');"><img id="laura_ashley" src="img/alicia1.jpg" class="bioimg" alt="Laura Ashley" width="120" height="120"></a>
					<li><a href="JavaScript:reset_images();show_bio('tina_holly');"><img id="tina_holly" src="img/alicia1.jpg" class="bioimg" alt="Tina Holly" width="120" height="120"></a>
				</ul>
				</div>

				<div id="bio_section" name="bio_section">
					
				</div>
			</div>
		</div> 
			
			
		</div>



		<div class="mm">
			
			<div class="leftsmall">
					<div class="achievements">
						Achievements? Or stretch out the money box.
					</div>
			</div>
			
			<div class="rightbig">
				<p class="money">
					$238,250,025
				</p>
				<hr>
				<p class="moneytext">
					total funds raised to date (updated quarterly)
				</p>
			</div>
			
			
		</div>



<?php include "servicestext.php";?>

<?php include "footer.php";?> 