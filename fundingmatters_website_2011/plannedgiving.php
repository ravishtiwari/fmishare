﻿<?php include "header.php";?>
<div class="body">
	<h3>Planned Giving</h3>
	<p>
		40% of High Net Worth individuals do not have a current estate plan.  The  utilizing the newest in giving strategies and the most advanced presentation techniques, the FUNDING matters Giftabulator solution builds a framework for estate and philanthropic planning. Only 4% of Estates include a Bequest.
	</p>
	<p>
		When it comes to discussing philanthropy with prospective donors and their families regarding planned giving or "leaving a legacy," FUNDING matters has developed a proven approach to the family's dynamics and their intentions. It is of the utmost importance for the family to feel as comfortable as they can about their donation.
	</p>
	<p>
		To ensure a successful legacy, we at FUNDING matters Inc. understand the need to engage in discussions with the family, their financial and legal advisors, in addition to their accountants. FUNDING matters Inc. takes great care in developing customized proposals that specifically address donors' needs and expectations. Only the most compelling and well thought-out proposals will maximize the revenues. Throughout the entire fundraising process, FUNDING matters Inc professionals provide the experience, judgement and wisdom of our counsel.
	</p>
	<p>
		FUNDING matters will work on-site providing day-to-day management and oversight of your fundraising program. Time on-site will be dependent on the needs and budget of your organization and can vary from as little as one day a month up to three days per week. The consultant's time will be hands-on and focused on activities such as:
	</p>
	<ul>
		<li>reviewing portfolios, individual assignments, and work flow
		<li>developing cultivation, solicitation, and stewardship strategies
		<li>measuring fundraising success
		<li>enhancing the effects of your annual, major gifts or planning giving programs.
	</ul>
	<p>
	Particular focus will be paid on engaging available staff to ensure progress continues between on-site visits.
	</p>
	<p>
		Services will be provided by one of FUNDING matters' senior advisors each with experience in development and major gift strategies.	Consultants on-site will tailor specific activities to the needs of your institution which may include:
	</p>
	<ul>
		<li>Building staff skills in cultivation and solicitation strategies
		<li>Reviewing the prospect pool and targets
		<li>Advising on the development of a Prospect Management system and accompanying reports 
		<li>Evaluating gift capacities and identifying ask amounts
	</ul>
	<p>
		The cost for this program varies by the consultant and on-site time.
	</p>
	
	<p>
		<a href="javascript: history.go(-1)">Click to go back</a>
	</p>
</div>
<?php include "footer.php";?>