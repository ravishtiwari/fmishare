﻿<?php include "header.php";?>
<div class="body">
	<h3>Public-Private Partnerships</h3>
	<p>
		FUNDING maters offers experienced senior consultants to assist you in strategic government relations and communications to support your philanthropic objectives. We tailor our support to your unique challenges, priorities and constituencies. We can help you develop your strategic planning to target your programs or capital initiatives to the appropriate funding agencies and their granting programs. Our broad range of services includes:
	</p>
	
	<h5>Program Grant Analysis and Counsel</h5>
	
	<ul>
		<li>Public funding reviews that can be comprehensive or focused on your highest priorities such as program or operational campaigns or capital and endowment funding
		<li>Development of Government Agency Strategic communications plans
		<li>Infrastructure financing
		<li>Constituency research
		<li>Staff and Volunteer Training
	</ul>

	<h5>Communications Execution</h5>
	
	<ul>
		<li>Writing for grants towards programming and capital campaigns
		<li>Writing for grant reports
		<li>Writing for the annual reports
		<li>Writing for infrastructure financing
		<li>Full-service execution of all advancement communications needs
	</ul>
	
	<p>
		<a href="javascript: history.go(-1)">Click to go back</a>
	</p>
</div>
<?php include "footer.php";?>