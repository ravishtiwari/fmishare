﻿<?php include "header.php";?>

	<div class="body">
		<h1>what we do</h1>
	</div>

	<div class="services-a">
		<img src="img/services_type-a.png" width="333" alt="Each client institution has successfully achieved its fundraising objectives">
		<p class="highlight-a">
		FUNDING matters has been committed to providing strategic counsel to help each client institution build	a development function that would have significant impact on its mission.
		</p>
	</div>

	<div class="services-b">
		<p>
			FUNDING matters will work on-site providing day-to-day management and oversight of your fundraising program. Time on-site will be dependent on the needs and budget of your organization and can vary from as little as one day a month up to three days per week.
		</p>
		<p>
			The consultant’s time will be hands-on and focused on activities such as reviewing portfolios,
			individual assignments, and work flow; developing cultivation, solicitation, and stewardship
			strategies; measuring fundraising success; and enhancing the effects of your annual, major gifts or planning giving programs.		
		</p>
		<p class="highlight-b">
			Particular focus will be paid on engaging available staff to ensure progress continues between on-site visits.
		</p>
		<p>
			Services will be provided by one of FUNDING matters’ senior advisors each with
			experience in development and major gift strategies. Specific activities while the consultant is on site will be tailored to the needs of your institution and may include:
		</p>
		<ul>
			<li>Building staff skills in cultivation and solicitation strategies
			<li>Reviewing the prospect pool and targets
			<li>Advising on the development of a Prospect Management system and accompanying reports 
			<li>Evaluating gift capacities and identifying ask amounts
		</ul>
		<p>
			The cost for this program varies by the consultant and on-site time.
		</p>

	</div>

	<div class="services-a">
		img goes here
	</div>

	<div class="services-b">
		<h3>Annual Giving Services</h3>
		<p>
		A productive annual giving program is essential to the long-term success and sustainability of an advancement program. While new media, information technology, and a changing economy have created both challenges and opportunities for many non-profit organizations, we at FUNDING matters are committed to working with our clients to develop annual giving strategies that engage donors and build lasting communities.
		</p>
		<p>
		<a href="annualgiving.php">Click for more information on Annual Giving Services</a>
		</p>
	</div>


	<div class="services-a">
		img goes here
	</div>

	<div class="services-b">
		<h3>Campaigns</h3>
		<p>
		Campaigns are a powerful strategic tool to address essential institutional needs and opportunities, and to galvanize donor commitment within a specific time frame. When well planned and executed, they elevate fundraising performance and enlist the support of all constituencies.
		</p>
		<p>
		<a href="campaigns.php">Click for more information on Campaigns</a>
		</p>
	</div>


	<div class="services-a">
		img here
	</div>

	<div class="services-b">
		<h3>Strategic Communications</h3>
		<p>
			Strategic communications are essential to successful fundraising. Good communications support institutional advancement by building enthusiasm and commitment among your prospects; motivating them to make gifts; and stewarding them by showing the impact of their gifts.
		</p>
		<p>
		<a href="stratcom.php">Click for more information on Strategic Communications</a>
		</p>

	</div>


	<div class="services-a">
		img here
	</div>

	<div class="services-b">
		<h3>Planned Giving</h3>
		<p>
			Blurb about planned giving.
		</p>
		<p>
		<a href="plannedgiving.php">Click for more information on Planned Giving</a>
		</p>
	</div>


	<div class="services-a">
		img goes here here
	</div>

	<div class="services-b">
		<h3>Public-Private Partnerships</h3>
		<p>
			FUNDING matters offers experienced senior consultants to assist you in strategic government relations and communications to support your philanthropic objectives. We tailor our support to your unique challenges, priorities and constituencies. We can help you develop your strategic planning to target your programs or capital initiatives to the appropriate funding agencies and their granting programs. 
		</p>
		<p>
		<a href="ppp.php">Click for more information on Public-Private Partnerships</a>
		</p>
	</div>





<!--
		<div class="leftsmall3">
			<h3>Software</h3>
		</div>

		<div class="rightbig3">
		<img src="img/giftabulator.jpg" width="280" alt="Giftabulator">
		<p>
		GIFTABULATOR® was developed to help nonprofits visualize the benefits of charitable giving with their stakeholders and engage in discussions with their advisors. GIFTABULATOR® provides complete wealth-management solutions benefiting charities and non-profits in the solicitation, cultivation, and processing of philanthropic contributions.
		</p>
		<p>
		<a href="#" class="show3">Read more</a>
		</p>
		<div class="more3">
		<p>
		By calculating tax benefits of planned giving and providing prospective donors with a comprehensive, realistic assessment of their financial wherewithal, GIFTABULATOR® can help fundraising professionals show their clients that planned giving is not only a benefit to society, but also a sound personal investment.
		</p>
		<p>
		Regardless of the organization or level of fundraising effort, GIFTABULATOR®'s patent pending solutions facilitate a clear, meaningful dialogue with donors, which can dramatically increase fundraising results.		
		</p>
		<p>
		Visit <a href="http://giftabulator.com" target="_blank">Giftabulator.com</a>.
		</p>
		</div>
	</div>



		<div class="leftsmall3">
			<h3>Web Design</h3>
		</div>

		<div class="rightbig3">
			<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor. 
			</p>
			<p>
			<a href="#" class="show2">Read more</a>
			</p>
			<div class="more2">
			<p>
			This is the more text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ipsum nisl, interdum at dapibus sit amet, elementum quis urna. Aliquam mattis dignissim diam ac fermentum. Nullam dictum egestas justo a tempor. Cras vitae sapien vitae magna luctus pellentesque. Proin eget lacus quam. Quisque at augue dolor.
			</p>
			</div>
		</div>

		-->




<?php include "testimonialslider.php";?>

<?php include "footer.php";?>