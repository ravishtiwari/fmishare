<?php include "header.php";?>
<div class="body">
	<h3>Strategic Communications</h3>
<p>
	Strategic communications are essential to successful fundraising. Good communications support institutional advancement by building enthusiasm and commitment among your prospects; motivating them to make gifts; and stewarding them by showing the impact of their gifts. 
</p>
<p>
	A strategy for success includes an understanding of your constituents that is validated through surveys and focus groups, compelling messages, careful planning that is coordinated with development and other constituency programs, and effective execution.
</p>
<p>
	FUNDING matters offers experienced senior consultants to assist you in strategic communications to support your philanthropic objectives. We tailor our support to your unique challenges, priorities and constituencies. We can help you target your mix of prospects and donors – alumni, parents, grateful patients, subscribers, members, patrons, friends, foundations corporations and government agencies. Our broad range of services includes:
</p>
<h5>Program Analysis and Counsel</h5>
	<ul>
		<li>Advancement communications reviews that can be comprehensive or focused on your highest priorities such as campaign readiness, principal and major gifts, annual giving, events, e-solicitation or online social networks
		<li>Strategic communications plans
		<li>Constituency research
		<li>Staff and Volunteer Training
	</ul>

<h5>Communications Execution</h5>
	<ul>
		<li>Writing for principal and major gift cultivation, solicitation and stewardship
		<li>Writing for annual giving letters, scripts, brochures, Web pages, and e-solicitation
		<li>Writing for the campaign prospectus, leadership case, public case, collaterals, campaign insider communications, newsletters, e-solicitation, events, and the campaign report
		<li>Full-service execution of all advancement communications needs
	</ul>

	
	
	<p>
		<a href="javascript: history.go(-1)">Click to go back</a>
	</p>
</div>
<?php include "footer.php";?>