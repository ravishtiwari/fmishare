﻿<?php include "header.php";?>

<div class="body">


<h1>Testimonials</h1>

	<div id="containertable">

		<div class="col1">

			<h3>Campaigns</h3> 
			<h4>$18 million capital gampaign. Ambitious goal realized.</h4>		
			<p class="quote1">
			"FUNDING matters Inc. has been a first rate partner in our Capital Campaign. FUNDING matter Inc's knowledge and contacts with all levels of government has been beneficial in securing their support. FUNDING matters Inc's hands on approach has developed strong bonds be-tween the JCCC and its major benefactors."
			</p>

			<p class="quote2">
			Russ Takashima
			<br>
			Campaign Chairman
			<br>
			National Building Together Campaign
			<br>
			Japanese Canadian Cultural Centre
			</p>


			<h4>$9 million in major gifts raised to build Alzheimer's Wing</h4>
			<p class="quote1">
			"Funding Matters was hired to, in essence, become our devel-opment office, responsible for our annual fundraising, special events and communications all in conjunction with the capital campaign. FMI brought a team of experts to raise our sights and connect us to the donors to make this new vision a reality. This comprehensive approach has made all the difference."
			</p>

			<p class="quote2">
			Sandy Lomaszewycz
			<br>
			Executive Director, Ukrainian Canadian Care Centre
			</p>


			<h4>Major Gift identification makes a difference</h4>
			<p class="quote1">
			"Bill Petruck and FUNDING matters provided counsel for a capital campaign at our church. The $400,000 campaign exceeded its goal by 60%. A key part of that success was the counsel we received from FUNDING matters. They helped to ensure that the fundamentals were right, that things were well timed, and the campaign pos-itioned for success. Bill and his colleagues were well received by the congregation. I heartily recommend Bill Petruck and FUNDING matters."

			<p class="quote2">
			Matthew Airhart
			<br>
			title???
			</p>

			<h4>$34 million in private-public partnership creates local cultural venue</h4>
			<p class="quote1">
			"William is an articulate, or-ganized individual who works hard for his clients."
			</p>

			<p class="quote2">
			Fraser Nelson
			<br>
			Campaign Cabinet
			<br>
			Richmond Hill Centre for the <br>Performing Arts
			</p>

			<h4>Smart funding major gift strategies</h4>
			<p class="quote1">
			"Bill and his team at FUNDING matters have created a unique space, combining proven fund-raising excellence and suc-cessful client strategies with innovative technological and in-teractive platforms and solu-tions. FUNDING matters' menu of products, particularly Giftab-ulator, provide the most compre-hensive fundraising and philan-thropic advantages to both organizations looking to raise funds and for individuals looking to make charitable choices.
			<br><br>
			As a professional, Bill is re-spected by his peers, a strong leader in his community, and always operates with integrity, kindness, compassion and en-ergy."
			</p>

			<p class="quote2">
			Peter Roman
			<br>
			Program Manager, Supply Chain Strategies
			<br>
			Ontario Hospital Association
			</p>


			<h4>Turning annual donors into major and planned giving donors</h4>
			<p class="quote1">
			"Communicating the various tax benefits of giving to donors can be challenging. We chose FUNDING matters® Inc.'s GIFTABULATOR® to help us walk donors through various giving scenarios, and illustrate that by leveraging tax credits, they can leave a more transformative gift. After using Giftabulator for one week, we secured a multi- year annual pledge totaling $110,000 and a $150,000 bequest from a donor who previously gave approximately $100-$150 per year. We are thrilled with the gift, and the donor was appreciative of our professional, educational ap-proach. We see Giftabulator as an important component of our fundraising efforts going forward."
			</p>

			<p class="quote2">
			Anita Nielsen
			<br>
			Senior Director, Annual & Planned Giving, Toronto General & Western Hospital Foundation
			</p>

		</div>


		<!-- column 2 -->
		
		<div class="col2">


			<h4>Estate and planned giving simplified</h4>
			<p class="quote1">
			"The training we received on the Giftabulator software made it easy for us to immediately en-gage in philanthropic planning discussions with our donors."
			</p>

			<p class="quote2">
			Emelita Ervin
			<br>
			Principal Gift Manager, Toronto General & Western Hospital Foundation
			</p>


			<h4>Easy planned giving presentations</h4>
			<p class="quote1">
			"Thanks you all so much for your work in putting together [the] training session. I have had wonderful feedback from the staff and we look forward to moving forward with these new tools."
			</p>

			<p class="quote2">
			Anita Nielsen
			Senior Director, Annual & Planned Giving, Toronto General & Western Hospital Foundation
			</p>

			<h4>Increased knowledge of major gift and planned giving strategies</h4>
			<p class="quote1">
			"We were very pleased with the ease of presentation of the Giftabulator product created by FUNDING matters Inc. It allowed us to walk prospective donors through the comparative ben-efits of gifts of stock versus gifts of cash. The visual demon-stration of the gift process and the potential it presents for increased giving levels, helped our donors to visualize the opportunities for more sig-nificant contributions. In some cases, the asset replacement strategies outlined aided donors in solidifying their philanthropic decisions."
			</p>

			<p class="quote2">
			Teresa Vasilopoulos
			<br>
			President, Toronto East General Hospital Foundation
			</p>


			<h4>Sell your vision - FUNDING matters thinks outside the box</h4>

			<p class="quote1">
			"Bill is innovative, forwarding thinking individual that enables a charity or any business to develop sound practices and information gathering to ensure they grow for the future."
			</p>

			<p class="quote2">
			Kimberley Bailey
			<br>
			Visibility Study Directory
			<br>
			Toronto Cristo Rey
			</p>


			<h4>$18 million gives rise to a new cultural centre</h4>

			<p class="quote1">
			"William brings considerable expertise and energy to his relationship with our organization. He fits seamlessly into our corporate culture and delivers exceptional results. Highly recommended!"
			</p>

			<p class="quote2">
			James Heron
			<br>
			Executive Director, Japanese Canadian Cultural Centre
			</p>


			<h4>Increasing donor alumni research</h4>
			<p class="quote1">
			"FUNDING matters Inc. has been very helpful in assisting St. Michael's College School set and manage the Alumni Executive online elections", says Stephanie Gough, the Alumni & Special Events Officer at St. Michael's College School, "The format for logging in and voting was simple and very user friendly and the FUNDING matters team was very responsive to any requests we may have had. Overall, dealing with FUNDING matters Inc. was a positive experience and we look forward to working with them again."
			</p>

			<p class="quote2">
			Stephanie Gough
			<br>
			Alumni & Special Events Officer, St. Michael's College School
			</p>



			<h3>Strategic Planning & Research</h3>
			
			

			<h4>Building a vision for your charity</h4>
			<p class="quote1">
			"The 'Creative Logistix Model' workshop was very thought provoking and made us realize that we have the essential information within our or-ganization to develop a tangible business plan. The 'Creative Logistix Model' is a practical way of assessing the moti-vational needs of our stakeholders, and the type of change that is required, to make this happen internally and externally. Your enthusiasm has inspired us, and your insights have definitely moved us forward in the right direction in developing sound fundraising and public relations strategies that will better profile Momiji in the community to approach our potential grantors."
			</p>

			<p class="quote2">
			Ann Ashley, President on behalf of the Momiji Health Care Society
			<br>
			Public Relations, Marketing and Fundraising Committee
			</p>


		</div>


		<!-- column 3 -->


		<div class="col3">

			<h4>Estate and philanthropic planning simplified</h4>
			<p class="quote1">
			"FUNDING matters' approach to illustratory charitable gift strat-egies provided our advisors with a markedly better understanding of philanthropy and how to help clients give (and build/protect their financial planning prac-tices). 
			<br><br>
			Once again, thank you for spending your valuable time with us. We look forward to working with each of you going forward."
			</p>

			<p class="quote2">
			Duane M. Bentley, CFP 
			<br>
			Division Director, Investors Group
			</p>

			<h4>FUNDING matters' system educating in securing larger donations</h4>
			<p class="quote1">
			"FUNDING matter Inc's Gift-abulator application is an invaluble tool in assisting potential donors to see the tax benefit that they can acheive thru their donation. It's quick, easy to use and dramatically shows the tax benefits of financial donations. The donor can see for themself the benefits without getting lost in complex tax calculations. It can really open the eyes of a donor to see how they can benefit from their donation."
			</p>

			<p class="quote2">
			Russ Takashima
			<br>
			Campaign Chairman
			<br>
			National Building Together Campaign
			<br>
			Japanese Canadian Cultural Centre
			</p>


			<h4>FUNDING matters knows what your donors are thinking</h4>
			<p class="quote1">
			"The FM-IQ donor survey undertaken by FUNDING matters Inc. has provided us with a far better insight into the attitudes and aspirations of our sup-porters. Although it confirmed certain assumptions we had about our donors, the research also highlighted one or two unexpected findings, and as such will enable us to improve the relationship we have with our supporters. However, the benefit of any research like this is only as good as the response you make to it. In combining FUNDING matters research skills with the technical knowledge of FM-IQ provided us with valuable feedback not only on how to plan and prioritize in improving our relationship with our donors."
			</p>

			<p class="quote2">
			Dwayne DiPasquale, 
			<br>
			Director, Annual Fund & Marketing North York General Hospital Foundation
			</p>


			<h4>FUNDING matters' outstanding work for ambitious organization</h4>
			<p class="quote1">
			"Bill is an energetic dynamo of enthusiasm, optimism and expertise who injects an indomitable spirit of success into any organization he works with. And having witnessed Bill and his FUNDING matters inc. team at work on a number of projects, Ican guarantee the success of whatever task you put before him. Bill combines leading edge thinking about philanthropy and organizational development with old fashioned hard work and loyalty. That combination means success. At CSB we are very picky about who we recommend to our clients and we have no hesitation recommending Bill Petruck and FUNDING matters inc."
			</p>

			<p class="quote2">
			Christopher Ballard
			<br>
			Principal, CSB Communications
			</p>



			<h4>Knowledge, Netowrks and Funding</h4>

			<p class="quote1">
			"Ihave worked with Bill on several occasions. He is bright, articulate and encourages risk and big ideas in others. He is a leader not a follower, and you better be ready to succeed to be on Bill's train or you will be left behind very quickly. He has a significant networ and uses it very effectively to assist others."
			</p>

			<p class="quote2">
			Linda Hendy
			<br>
			Principal, The Bridging Group
			</p>



			<h3>Sponsorship</h3>

			<h4>Sponsorship of information conference a success</h4>
			<p class="quote1">
			"Thanks for all of your help in making the conference a success - and from all the feedback we got it truly was a success. It has been a pleasure working with you and your team."
			</p>

			<p class="quote2">
			Linda R. Lewis
			<br>
			Chair, School of Fashion 
			<br>Ryerson University
			</p>


			<!-- WE NEED CATEGORIES FOR THESE TESTIMONIALS -->

			




			





		</div>

	</div>

</div>

<?php include "servicestext.php";?>

<?php include "footer.php";?>