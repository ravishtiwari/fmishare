/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    textClass:[ "l0_out", "l0_over" ],
    size:[ 41, 112.5 ],
    itemoff:[ 0, "+previousItem" ],
    leveloff:[ "+parentItem-5px", 4 ],
    itemFilters:[ "progid:DXImageTransform.Microsoft.Fade(duration=0.5)", "progid:DXImageTransform.Microsoft.Fade(duration=0.5)" ]
};

var STYLE_2 =
{
    /*4{*/leveloff:[ 0, '+parentItem-1px' ]/*4}*/
};

var STYLE_1 = {
    levelFilters:[ "progid:DXImageTransform.Microsoft.Fade(duration=0.5) progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=reverse,duration=0.5)", "progid:DXImageTransform.Microsoft.Fade(duration=0.5) progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.5)" ],
    itemFilters:null,
    size:[ 41, 145 ],
    backgroundClass:[ "l1_bg_other", "l1_bg_other" ],
    borderWidth:[ 1, 0, 1, 0 ],
    color:
    {
        border:"#F2EEEE"
    },
    textClass:[ "l1_out", "l1_over" ],
    size:[ 19, 142 ],
    itemoff:[ "+previousItem", 0 ],
    ifN0:{
        valign:"bottom",
        size:[ 24, 142 ],
        backgroundClass:[ "l1_bg_0", "l1_bg_0" ]
    },
    ifN1:{
        backgroundClass:[ "l1_bg_1", "l1_bg_1" ]
    },
    ifLast:{
        valign:"top",
        size:[ 30, 142 ],
        borders:[ 1, 0, 1, 1 ]
    }
};

var MENU_ITEMS = [
/*    {style:[ STYLE_0, STYLE_1, STYLE_2 ], blankImage:"http://www.jccc.on.ca/rentals/js_menu/b.gif" },*/

    {style:[ STYLE_0, STYLE_1, STYLE_2 ], blankImage:"images/b.gif" },
    {code:"Home", url:"index.htm",
        sub:[
            {}
        ]
    },
    {code:"About",url:"about/who_we_are.htm",
		sub:[
            {},
            {code:"Who we are",url:"about/who_we_are.htm",
                sub:[{}]
            },
            /*{code:"Membership",url:"about/membership.htm",
                sub:[{}]
            },*/
            {code:"Heritage",url:"about/heritage.htm",
                sub:
                [
                    {},
                    {code:"Mission statement", url:"about/heritage/mission_statement.htm"},
					{code:"Volunteering", url:"about/heritage/volunteering.htm"}
                ]
            },
            {code:"Newsletters",url:"about/newsletters.htm",
                sub:[{}]
            },		
            {code:"Giftshop",url:"about/giftshop.htm",
                sub:[{}]
            },
            /*{code:"Donate",url:"about/donate.htm",
                sub:[{}]
            },*/			
            {code:"Policies",url:"about/policies.htm",
                sub:[{}]
            },
            /*{code:"Volunteering",url:"about/volunteering.htm",
                sub:[{}]
            },*/
            {code:"Links",url:"about/links.htm",
                sub:[{}]
            }
        ]
    },
    {code:"Programs",url:"about/who_we_are.htm",
		sub:[
            {},
            {code:"Martial arts",url:"programs/martial_arts.htm",
                sub:
                [
                    {},
                    {code:"Aikido", url:"spaces/kobayashi_hall.htm"},
                    {code:"Iaido", url:"spaces/kobayashi_hall_patio.htm"},
                    {code:"Judo", url:"spaces/shokokai_court.htm"},
                    {code:"Karate", url:"spaces/heritage_court.htm"},
                    {code:"Kendo", url:"spaces/heritage_lounge.htm"},
					{code:"Naginata", url:"spaces/hosaki_room.htm"},
					{code:"Shorinji Kempo", url:"spaces/parking.htm"},
					{code:"Fees", url:"spaces/parking.htm"}

                ]
            },
            {code:"Workshops",url:"programs/workshops.htm",
                sub:
                [
                    {},
                    {code:"Chimaki", url:"programs/martial_arts/aikido.htm"},
                    {code:"Japanese Cooking", url:"programs/martial_arts/aikido.htm"},
                    {code:"Manju", url:"programs/martial_arts/aikido.htm"},
                    {code:"Origami", url:"programs/martial_arts/aikido.htm"},
                    {code:"Raku", url:"programs/martial_arts/aikido.htm"},
                    {code:"Taiko", url:"programs/martial_arts/aikido.htm"},
                    {code:"Washi", url:"programs/martial_arts/aikido.htm"}
                ]
            },
            {code:"Cultural Classes",url:"programs/martial_arts.htm",
                sub:
                [
                    {},
                    {code:"Japanese Language Classes", url:"programs/martial_arts/aikido.htm"},
                    {code:"Bunka Shishu", url:"programs/martial_arts/aikido.htm"},
                    {code:"Ikebana", url:"programs/martial_arts/aikido.htm"},
                    {code:"Koto", url:"programs/martial_arts/aikido.htm"},
                    {code:"Shodo", url:"programs/martial_arts/aikido.htm"},
                    {code:"Sumi-e", url:"programs/martial_arts/aikido.htm"},
				    {code:"Kamp Kodomo", url:"programs/martial_arts/kamp_kodomo.htm"},
                    {code:"Yoga", url:"programs/martial_arts/aikido.htm"}
                ]
            },
            {code:"Gendai Gallery",url:"programs/gendai_gallery.htm",
                sub: [{}]
            },
            {code:"Activities", url:"programs/activities.htm",
                sub: [{}]
            }			
        ]
    },
    {code:"Events", url:"contact/contact.htm",
        sub:[
            {},
            {code:"Upcoming events", url:"contact/location.htm"},
            {code:"Events calendar", url:"contact/event_inquiry_form.htm"},
            {code:"Gallery", url:"contact/event_inquiry_form.htm"}
        ]
    },
    {code:"Rentals", url:"http://www.jcccrentals.on.ca",
        sub:[{}]
    },
    {code:"Support", url:"http://www.jcccrentals.on.ca",
        sub:[
			{},
			{code:"Membership",url:"about/membership.htm"},
			{code:"Donation",url:"about/donate.htm"},
			{code:"Volunteering",url:"about/volunteering.htm"}
		
		]
    },
    {code:"Contact", url:"contact/contact.htm",
        sub:[{}]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
