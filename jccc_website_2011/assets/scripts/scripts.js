/* Change font size */

$(document).ready(function(){
  // Reset Font Size
  var originalFontSize = $('html').css('font-size');
    $(".resetFont").click(function(){
    $('html').css('font-size', originalFontSize);
  });
  // Increase Font Size
  $(".increaseFont").click(function(){
    var currentFontSize = $('html').css('font-size');
    var currentFontSizeNum = parseFloat(currentFontSize, 10);
    var newFontSize = currentFontSizeNum*1.2;
    $('html').css('font-size', newFontSize);
    return false;
  });
  // Decrease Font Size
  $(".decreaseFont").click(function(){
    var currentFontSize = $('html').css('font-size');
    var currentFontSizeNum = parseFloat(currentFontSize, 10);
    var newFontSize = currentFontSizeNum*0.8;
    $('html').css('font-size', newFontSize);
    return false;
  });
});



/* Programs navigation */

/*
more_xx = sub-navigation part that will be hidden until clicked. apply this class to the subnav class of the ul
show_xx = apply this class to the link
*/

/* show/hide martial arts sub-navigation */

$(document).ready(function(){
$(".more_ma").hide();
$(".show_ma").show();
$('.show_ma').click(function(){
$(".more_ma").slideToggle();
return false;
});
});


/* show/hide culture classes sub-navigation */

$(document).ready(function(){
$(".more_cu").hide();
$(".show_cu").show();
$('.show_cu').click(function(){
$(".more_cu").slideToggle();
return false;
});
});


/* show/hide activities & workshops sub-navigation */

$(document).ready(function(){
$(".more_ac").hide();
$(".show_ac").show();
$('.show_ac').click(function(){
$(".more_ac").slideToggle();
return false;
});
});


/* show/hide recreation sub-navigation */

$(document).ready(function(){
$(".more_rc").hide();
$(".show_rc").show();
$('.show_rc').click(function(){
$(".more_rc").slideToggle();
return false;
});
});