<?php $activeleft = " class=\"active-left\""; ?>
<div class="left-nav">
	<ul>
		<li><a href="/en/about/"<?php if($self == '/en/about/index.php') { echo $activeleft;}?>>Who We Are</a></li>
		<li><a href="/en/about/mission.php"<?php if($self == '/en/about/mission.php') { echo $activeleft;}?>>Mission Statement</a></li>
		<li><a href="/en/about/board.php"<?php if($self == '/en/about/board.php') { echo $activeleft;}?>>Board of Directors</a></li>
		<li><a href="/en/about/kobayashi_hall.php"<?php if($self == '/en/about/kobayashi_hall.php') { echo $activeleft;}?>>Kobayashi Hall</a></li>
	</ul>
</div>