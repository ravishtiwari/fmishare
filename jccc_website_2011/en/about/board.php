<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Board of Directors</title>

	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>

	<?php include "about_nav.php";?>

	<div class="right-content">

		<img src="" class="programs" alt="Picture of the members of the board of directors.">

		<h1 class="bottom">Board of Directors</h1>

		<table id="programs">
			<tr>
				<th width="30%">Title</th>
				<th>Name(s)</th>
			</tr>
			<tr>
				<td class="bold">President</td>
				<td>Gary Kawaguchi</td>
			</tr>
			<tr>
				<td class="bold">VP of Management</td>
				<td>Ann Ashley</td>
			</tr>
			<tr>
				<td class="bold">VP of Legacy</td>
				<td>Arthur Ito</td>
			</tr>
			<tr>
				<td class="bold">VP of Heritage</td>
				<td>Peter Wakayama</td>
			</tr>
			<tr>
				<td class="bold">VP of J-Comm</td>
				<td>Junko Mifune</td>
			</tr>
			<tr>
				<td class="bold">VP of Strategic Planning</td>
				<td>Donna Davis</td>
			</tr>
			<tr>
				<td class="bold">Special Ambassador</td>
				<td>Sid Ikeda</td>
			</tr>
			<tr>
				<td class="bold">Secretary</td>
				<td>Sharon Marubashi</td>
			</tr>
			<tr>
				<td class="bold">Treasurer</td>
				<td>Chris Reid</td>
			</tr>
			<tr>
				<td class="bold">Directors</td>
				<td>George Hewson, Chris Hope, Shari Hosaki, James Matsumoto, Lorene Nagata, Christine Nakamura, Yuki Nakamura, Dereck Oikawa, Russell Onizuka, Cary Rothbart, Nao Seko</td>
			</tr>
			<tr>
				<td class="bold">Advisors</td>
				<td>Miki Kobayashi, Mickey Matsubayashi, Steve Oikawa, Fred Sasaki, Connie Sugiyama</td>
			</tr>
			<tr>
				<td class="bold">Executive Director</td>
				<td>James Heron</td>
			</tr>
			<tr>
				<td class="bold">Past President</td>
				<td>Marty Kobayashi</td>
			</tr>
			<tr>
				<td class="bold">Past Foundation Chair</td>
				<td>Mark Matsumoto</td>
			</tr>
		</table>

	</div>
	<?php include "../footer.php";?>
</div>

</body>

</html>