<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Who We Are</title>

	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>

	<?php include "about_nav.php";?>

	<div class="right-content">
		<h1>Who We Are</h1>
		<img src="" class="programs" alt="Who We Are">
		<h3 class="center">The Japanese Canadian Cultural Centre is a not-for-profit organization which offers programs, services and a gathering place to celebrate Japanese and Japanese Canadian culture.</h3>
		<p>
			The Japanese Canadian Cultural Centre (JCCC) represents the fulfillment of a long held community vision; celebrating the unique culture, history, and legacy of Japanese Canadians for the benefit of all Canadians. Since 1964, the JCCC has served as a home for the community and a place for Japanese Canadians to share their heritage with the greater population. The Japanese Canadian Cultural Centre is recognized across Canada as an important and vibrant community institution.
		</p>
		<blockquote class="quote">
		The Japanese Canadian Cultural Centre is committed to building a strong and diverse community by providing a wide range of programming, activities, and special events that promote harmony, friendship and understanding.
		</blockquote>
		<p class="quoter">
		- Gary Kawaguchi, President, JCCC
		</p>
		<p>
			Open to everyone regardless of race, religion, sex or age, the JCCC brings the world stage to Canada allowing the entire community to experience never before seen performances and special exhibits.
		</p>
		<ul>
			<li>The JCCC serves 4,300 members province-wide, almost half of which are of non-Japanese ancestry, and 35% of whom are seniors.
			<li>The JCCC attracts over 200,000 people to its festivals, concerts, martial arts tournaments and special events annually.
			<li>More than 15,000 students from the GTA and beyond visit the JCCC each year to participate in appreciation seminars on Japanese history, culture, and the Japanese Canadian experience.
		</ul>
		<p>
			Registered charity # 118972967-RR-0001
		</p>
		
		
	</div>
	<?php include "../footer.php";?>

</div>



</body>

</html>