<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Kobayashi Hall</title>

	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>

	<?php include "about_nav.php";?>

	<div class="right-content">

		<img src="/assets/images/kobayashi1.png" class="programs" alt="Picture of the exterior of Kobayashi Hall.">

		<h1>Kobayashi Hall</h1>
			<p>
				The Kobayashi Hall is the JCCC's new 6,500 square foot performance hall which opened in 2004.
			</p>
			<p>
				The Hall is a truly multifunctional space and has been designed to accommodate music concerts, festivals and traditional community events, business conferences, weddings, banquets, martial arts tournaments, theatrical performances and film festivals. With retractable stadium-style seating for over 450 and superb acoustics, the Kobayashi Hall promises to play host to a wide variety of both domestic and international performances.
			</p>
			<img src="/assets/images/kobayashi2.png" class="programs" alt="Picture of the interior of Kobayashi Hall.">

	</div>
	
<?php include "../footer.php";?>
</div>




</body>

</html>