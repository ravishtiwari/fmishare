<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Mission Statement</title>

	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>

	<?php include "about_nav.php";?>

	<div class="right-content">


		<h1>Mission Statement</h1>
		<blockquote>
			<p>
				To introduce the culture, history and legacy of the Japanese Canadians to all Canadians while creating a tribute to the history of the Nikkei community and their contributions to the building of our nation.
			</p>
			<p>
				To educate all Canadians on the importance of heritage, tolerance and cultural understanding.
			</p>
			<p>
				To create a space for Pre and Post war Japanese immigrants and their descendants to congregate, thus acting as a "living museum" and a focal point for the community.
			</p>
			<p>
				To bridge generations within the Japanese Canadian community: connecting the younger generation to their roots and providing the means for the continuing evolution of the community.
			</p>
		</blockquote>
		<h2 class="center italics">Friendship through Culture.</h2>
		<p>
			In executing our mission, we take pride in our heritage, creating a lasting tribute to the unique history and contributions of past generations; we bridge generations as the means for the continuing evolution of our community and we demonstrate the importance of tolerance and acceptance of cultural diversity for the benefit of all Canadians.
		</p>

	</div>
	<?php include "../footer.php";?>
</div>
</body>
</html>