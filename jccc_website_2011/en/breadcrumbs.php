<?php
$thing = "&nbsp; &raquo; &nbsp;";

/* level one links */

// $home_link = "<a href=\"/en/\">Home</a>";	do we want breadcrumbs on the home page? we're home already, so probably not.
$about_link = "<a href=\"/en/about/\">About</a>";
$membership_link = "<a href=\"/en/membership/\">Membership</a> ";
// $volunteer_link = "<a href=\"/en/volunteer/\">Volunteer</a>";	do we want breadcrumbs on single pages like this one? probably not.
$programs_link = "<a href=\"/en/programs/\">Programs</a>";
$events_link = "<a href=\"/en/events/\">Events</a>";
$media_link = "<a href=\"/en/media/\">Media</a>";
$donate_link = "<a href=\"/en/donate/\">Donate</a>";


/* first level navigation set */

if(($self == '/index.php') || ($self == '/en/index.php')) { $level_one = $home_link; }
else if(stripos($uri,'membership')) { $level_one = $membership_link; }
else if(stripos($uri,'about')) { $level_one = $about_link; }
else if(stripos($uri,'membership')) { $level_one = $membership_link; }
else if(stripos($uri,'volunteer')) { $level_one = $volunteer_link; }
else if(stripos($uri,'programs')) { $level_one = $programs_link; }
else if(stripos($uri,'media')) { $level_one = $media_link; }
else if(stripos($uri,'donate')) { $level_one = $donate_link;}
else if(stripos($uri,'events')) { $level_one = $events_link; } 


/* level two links */

/* about */
$whoweare = "<a href=\"/en/about/\">Who We Are</a>";
$mission = "<a href=\"/en/about/mission.php\">Mission Statement</a>";
$board = "<a href=\"/en/about/board.php\">Board of Directors</a>";
$kobayashi = "<a href=\"/en/about/kobayashi_hall.php\">Kobayashi Hall</a>";

/* membership */
$membershipsindex = "<a href=\"/en/membership/\">Memberships at the JCCC</a>";
$benefits = "<a href=\"/en/membership/benefits.php\">Member Benefits</a>";
$buymembership = "<a href=\"/en/membership/payfees.php\">Become a Member / Renew your Membership</a>";

/* volunteer * nothing here, only one page */

/* events */
$aboutevents = "<a href=\"/en/events/\">About our Events</a>";
$specialevents = "<a href=\"/news_events/\">Special Events</a>";

/* media */
$mediaindex = "<a href=\"/en/media/\">Multimedia</a>";
$videos = "<a href=\"/videos/\">Videos</a>";
$audio = "<a href=\"/audio/\">Audio</a>";
$gallery = "<a href=\"/en/media/gallery/\">Image Gallery</a>";
$kimono = "<a href=\"/en/media/interactive/kimono_game.php\">Kimono App</a>";

/* donate */
$investing = "<a href=\"/en/donate/\">Investing in our Future</a>";
$needs = "<a href=\"/en/donate/needs.php\">Statement of Needs</a>";
$makedonation = "<a href=\"/en/donate/make_donation.php\">Make a Donation</a>";

/* programs */
$aboutprograms = "<a href=\"/en/programs/\">About our Programs</a>";
$cultural = "<a href=\"/en/programs/cultural/\">Cultural Classes</a>";
$martialarts = "<a href=\"/en/programs/martial_arts/\">Martial Arts</a>";
$recreation = "<a href=\"/en/programs/recreation/\">Recreation</a>";
$activities = "<a href=\"/en/programs/activities/\">Activities & Workshops</a>";
$heritage = "<a href=\"/en/programs/heritage/\">Heritage</a>";
$discoverjapan = "<a href=\"/en/programs/discover_japan/\">Discover Japan</a>";
$kidsfamilies = "<a href=\"/en/programs/kids/\">Kids & Families</a>";
$school = "<a href=\"/en/programs/school/\">School Groups</a>";
$fees = "<a href=\"/en/programs/fees/\">Pay Program Fees</a>";


/* second level navigation set */
if($self == '/en/about/index.php') { $level_two = $whoweare; }
else if($self == '/en/about/mission.php') { $level_two = $mission; }
else if($self == '/en/about/board.php') { $level_two = $board; }
else if($self == '/en/about/kobayashi_hall.php') { $level_two = $kobayashi; }

else if($self == '/en/membership/index.php') { $level_two = $membershipsindex; }
else if($self == '/en/membership/benefits.php') { $level_two = $benefits; }
else if($self == '/en/membership/payfees.php') { $level_two = $buymembership; }

if($self == '/en/events/index.php') { $level_two = $aboutevents; }
else if($self == '/en/media/index.php') { $level_two = $mediaindex; }

else if($self == '/en/media/gallery/index.php') { $level_two = $gallery; }
else if($self == '/en/media/interactive/kimono_game.php') { $level_two = $kimono; }

else if($self == '/en/donate/index.php') { $level_two = $investing; }
else if($self == '/en/donate/needs.php') { $level_two = $needs; }
else if($self == '/en/donate/make_donation.php') { $level_two = $makedonation; }

else if($self == '/en/programs/index.php') { $level_two = $aboutprograms; }
else if(stripos($uri,'cultural')) { $level_two = $cultural; }
else if(stripos($uri,'martial_arts')) { $level_two = $martialarts; }
else if(stripos($uri,'recreation')) { $level_two = $recreation; }
else if(stripos($uri,'activities')) { $level_two = $activities; }
else if($self == '/en/programs/heritage/index.php') { $level_two = $heritage; }
else if($self == '/en/programs/discover_japan/index.php') { $level_two = $discoverjapan; }
else if($self == '/en/programs/kids/index.php') { $level_two = $kidsfamilies; }
else if($self == '/en/programs/school/index.php') { $level_two = $school; }
else if($self == '/en/programs/fees/index.php') { $level_two = $fees; }



/* wordpress stuff: setting levels one and two */

if (function_exists('is_page')){
	//echo the_ID();
	if((is_page(9)) || ($post_type=='news_events')){
		$level_one = $events_link;
		$level_two = $specialevents;
		
	} else if((is_page(28)) || ($post_type=='video')){
		$level_one = $media_link;
		$level_two = $videos;
	} else if((is_page(20)) || ($post_type=='audio')){
		$level_one = $media_link;
		$level_two = $audio;
	}

}


/* level three links: programs only */
/* cultural links */
$culturalindex = "<a href=\"/en/programs/cultural/\">Cultural Classes at the JCCC</a>";
$japlanguage = "<a href=\"/en/programs/cultural/japanese.php\">Japanese Language</a>";
$bunkashishu = "<a href=\"/en/programs/cultural/bunka_shishu.php\">Bunka Shishu</a>";
$shodo = "<a href=\"/en/programs/cultural/shodo.php\">Shodo</a>";
$ikebana = "<a href=\"/en/programs/cultural/ikebana.php\">Ikebana</a>";
$sumie = "<a href=\"/en/programs/cultural/sumi-e.php\">Sumi-e</a>";
$etegami = "<a href=\"/en/programs/cultural/e-tegami.php\">E-tegami</a>";
$manju = "<a href=\"/en/programs/cultural/manju.php\">Manju</a>";
$raku = "<a href=\"/en/programs/cultural/raku.php\">Raku</a>";
$washi = "<a href=\"/en/programs/cultural/washi.php\">Washi</a>";

/* set level three links: cultural */
if($self == '/en/programs/cultural/index.php') { $level_three = $culturalindex; }
else if($self == '/en/programs/cultural/japanese.php') { $level_three = $japlanguage; }
else if($self == '/en/programs/cultural/bunka_shishu.php') { $level_three = $bunkashishu; }
else if($self == '/en/programs/cultural/shodo.php') { $level_three = $shodo; }
else if($self == '/en/programs/cultural/ikebana.php') { $level_three = $ikebana; }
else if($self == '/en/programs/cultural/sumi-e.php') { $level_three = $sumie; }
else if($self == '/en/programs/cultural/e-tegami.php') { $level_three = $etegami; }
else if($self == '/en/programs/cultural/manju.php') { $level_three = $manju; }
else if($self == '/en/programs/cultural/raku.php') { $level_three = $raku; }
else if($self == '/en/programs/cultural/washi.php') { $level_three = $washi; }

/* martial arts links */
$martialartsindex = "<a href=\"/en/programs/martial_arts/\">Martial Arts at the JCCC</a>";
$aikido = "<a href=\"/en/programs/martial_arts/aikido.php\">Aikido</a>";
$judo = "<a href=\"/en/programs/martial_arts/judo.php\">Judo</a>";
$karate = "<a href=\"/en/programs/martial_arts/karate_kobudo.php\">Karate & Kobudo</a>";
$kendo = "<a href=\"/en/programs/martial_arts/kendo.php\">Kendo</a>";
$iaido = "<a href=\"/en/programs/martial_arts/iaido.php\">Iaido</a>";
$shorinjikempo = "<a href=\"/en/programs/martial_arts/shorinji_kempo.php\">Shorinji Kempo</a>";
$kyudo = "<a href=\"/en/programs/martial_arts/kyudo.php\">Kyudo</a>";
$naginata = "<a href=\"/en/programs/martial_arts/naginata.php\">Naginata</a>";

/* set level three links: martial_arts */
if($self == '/en/programs/martial_arts/index.php') { $level_three = $martialartsindex; }
else if($self == '/en/programs/martial_arts/aikido.php') { $level_three = $aikido; }
else if($self == '/en/programs/martial_arts/judo.php') { $level_three = $judo; }
else if($self == '/en/programs/martial_arts/karate_kobudo.php') { $level_three = $karate; }
else if($self == '/en/programs/martial_arts/kendo.php') { $level_three = $kendo; }
else if($self == '/en/programs/martial_arts/iaido.php') { $level_three = $iaido; }
else if($self == '/en/programs/martial_arts/shorinji_kempo.php') { $level_three = $shorinjikempo; }
else if($self == '/en/programs/martial_arts/kyudo.php') { $level_three = $kyudo; }
else if($self == '/en/programs/martial_arts/naginata.php') { $level_three = $naginata; }

/* activities & workshops links */
$activitiesindex = "<a href=\"/en/programs/activities/\">Activities & Workshops at the JCCC</a>";
$cooking = "<a href=\"/en/programs/activities/cooking.php\">Japanese Cooking</a>";
$origami = "<a href=\"/en/programs/activities/origami.php\">Origami</a>";
$taiko = "<a href=\"/en/programs/activities/taiko.php\">Taiko</a>";
$karaoke = "<a href=\"/en/programs/activities/karaoke.php\">Karaoke</a>";
$music = "<a href=\"/en/programs/activities/music.php\">Music</a>";
$jcommjpalls = "<a href=\"/en/programs/activities/jcomm_jpalls.php\">J-Comm & J-Palls</a>";
$jsscollab = "<a href=\"/en/programs/activities/jss_collab.php\">JSS Collaboration</a>";
$westeast = "<a href=\"/en/programs/activities/west_meets_east.php\">West Meets East</a>";
$ayamekai = "<a href=\"/en/programs/activities/ayame_kai.php\">Ayame Kai</a>";
$sakurakai = "<a href=\"/en/programs/activities/sakura_kai.php\">Sakura Kai</a>";
$himawari = "<a href=\"/en/programs/activities/himawari.php\">Himawari Buyo-Kai</a>";
$tokenkai = "<a href=\"/en/programs/activities/token_kai.php\">Token Kai</a>";
$jcinema = "<a href=\"/en/programs/activities/jcinema.php\">J-Cinema</a>";

/* set level three links: activities */
if($self == '/en/programs/activities/index.php') { $level_three = $activitiesindex; }
else if($self == '/en/programs/activities/cooking.php') { $level_three = $cooking; }
else if($self == '/en/programs/activities/origami.php') { $level_three = $origami; }
else if($self == '/en/programs/activities/taiko.php') { $level_three = $taiko; }
else if($self == '/en/programs/activities/karaoke.php') { $level_three = $karaoke; }
else if($self == '/en/programs/activities/music.php') { $level_three = $music; }
else if($self == '/en/programs/activities/jcomm_jpalls.php') { $level_three = $jcommjpalls; }
else if($self == '/en/programs/activities/jss_collab.php') { $level_three = $jsscollab; }
else if($self == '/en/programs/activities/west_meets_east.php') { $level_three = $westeast; }
else if($self == '/en/programs/activities/ayame_kai.php') { $level_three = $ayamekai; }
else if($self == '/en/programs/activities/sakura_kai.php') { $level_three = $sakurakai; }
else if($self == '/en/programs/activities/himawari.php') { $level_three = $himawari; }
else if($self == '/en/programs/activities/token_kai.php') { $level_three = $tokenkai; }
else if($self == '/en/programs/activities/jcinema.php') { $level_three = $jcinema; }

/* recreation links */
$recreationindex = "<a href=\"/en/programs/recreation/\">Recreation at the JCCC</a>";
$yoga = "<a href=\"/en/programs/recreation/yoga.php\">Yoga</a>";
$taichi = "<a href=\"/en/programs/recreation/taichi.php\">Tai Chi</a>";
$ohanahula = "<a href=\"/en/programs/recreation/ohana_hula.php\">Ohana Hula</a>";
$hulahealth = "<a href=\"/en/programs/recreation/hula_for_health.php\">Hula for Health</a>";
$bridge = "<a href=\"/en/programs/recreation/bridge.php\">Bridge</a>";
$pingpong = "<a href=\"/en/programs/recreation/ping_pong.php\">Ping Pong</a>";
$wynford = "<a href=\"/en/programs/recreation/wynford.php\">Wynford Seniors' Club</a>";

/* set level three links: recreation */
if($self == '/en/programs/recreation/index.php') { $level_three = $recreationindex; }
else if($self == '/en/programs/recreation/yoga.php') { $level_three = $yoga; }
else if($self == '/en/programs/recreation/taichi.php') { $level_three = $taichi; }
else if($self == '/en/programs/recreation/ohana_hula.php') { $level_three = $ohanahula; }
else if($self == '/en/programs/recreation/hula_for_health.php') { $level_three = $hulahealth; }
else if($self == '/en/programs/recreation/bridge.php') { $level_three = $bridge; }
else if($self == '/en/programs/recreation/ping_pong.php') { $level_three = $pingpong; }
else if($self == '/en/programs/recreation/wynford.php') { $level_three = $wynford; }

?>


<div id="breadcrumbs">
	<?php 
	
	echo $level_one;	// first level of breadcrumbs
	
	if($level_two == true) { echo $thing;}		// thing will show if the next level exists, otherwise not.
	
	echo $level_two;	// second level of breadcrumbs
	
	if($level_three == true) { echo $thing; }
	
	echo $level_three;
	
	?>
	
</div>