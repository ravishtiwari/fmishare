<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Contact the JCCC</title>

	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>
	<div class="main-content">
		<div class="main-content-section">

			<h1>Contact the JCCC</h1>
			<div class="container clearfix">

				<div class="contact-left">

					<div class="address">
						<h2>Japanese Canadian Cultural Centre</h2>
						6 Garamond Court
						<br>
						Toronto, Ontario, M3C 1Z5
						<br>
						<b>tel:</b> (416) 441-2345
						<br>
						<b>fax:</b> (416) 441-2347 
					</div>

					<img src="/assets/images/map.png" class="map" alt="Map of the JCCC.">

				</div>

				<div class="contact-right">
					<h4>JCCC Building Hours</h4>
					<p>
					Monday to Sunday: 8:30am to 10:00pm
					</p>

					<h4>Reception Hours</h4>
					<p>
					Monday to Friday: 8:30am to 9:00pm
					<br>
					Saturday & Sunday: 8:30am to 4:00pm
					</p>
					
					<h4>Administrative Office Hours</h4>
					<p>
					Monday to Friday: 9:00am to 5:00pm
					<br>
					Saturday & Sunday: closed
					</p>

					<h4>Directions by TTC</h4>
					<p>
						Take the 100 Flemingdon bus from Eglinton or Broadview subway station
					</p>
				</div>

			</div>

			<h2>Departments</h2>
			<p>
			For general inquiries, including class and workshop information and registration, please contact the reception desk at extension 222, or email <a href="mailto:jccc@jccc.on.ca">jccc@jccc.on.ca</a>
			</p>

			<table id="programs">
				<tr>
					<th>Department</th>
					<th>Name</th>
					<th>Title</th>
					<th>Email</th>
					<th width="105">Phone<br>(416) 441-2345</th>
				</tr>
				<tr>
					<td class="bold">Newsletter</td>
					<td></td>
					<td>Newsletter Sign-up</td>
					<td><a href="mailto:newsletter@jccc.on.ca">newsletter@jccc.on.ca</a></td>
					<td></td>
				</tr>
				<tr>
					<td class="bold">Community Rental Inquiries</td>
					<td>Haruko Ishihara</td>
					<td>Community Rental Coordinator</td>
					<td></td>
					<td>ext. 228</td>
				</tr>
				<tr>
					<td class="bold">Corporate Rental Inquiries</td>
					<td>Christine Seki</td>
					<td>Programming & Business Development</td>
					<td></td>
					<td>ext. 231</td>
				</tr>
				<tr>
					<td class="bold">Memberships</td>
					<td>Sally Kumagawa</td>
					<td>Membership & Database Administrator</td>
					<td></td>
					<td>ext. 223</td>
				</tr>
				<tr>
					<td class="bold">Heritage</td>
					<td></td>
					<td></td>
					<td><a href="mailto:heritage@jccc.on.ca">heritage@jccc.on.ca</a></td>
					<td></td>
				</tr>
				<tr>
					<td class="bold">Community Events & Volunteering</td>
					<td>Christine Takasaki</td>
					<td>Community Event & Volunteer Coordinator</td>
					<td></td>
					<td>ext. 221</td>
				</tr>
				<tr>
					<td class="bold">Accounting & Administration</td>
					<td>Kathy Tazumi</td>
					<td>Accounting & General Administration Manager</td>
					<td></td>
					<td>ext. 229</td>
				</tr>
				<tr>
					<td class="bold">Website Maintenance</td>
					<td>Toshiko Yamashita</td>
					<td>Web Editor</td>
					<td><a href="mailto:toshikoy@jccc.on.ca">toshikoy@jccc.on.ca</a></td>
					<td></td>
				</tr>

				<tr>
					<td class="bold">Policy & Programming</td>
					<td>James Heron</td>
					<td>Executive Director</td>
					<td></td>
					<td>ext. 224</td>
				<tr>
					<td class="bold">Capital Campaign</td>
					<td>William Petruck</td>
					<td>Capital Campaign Consultant</td>
					<td><a href="mailto:wpetruck@fundingmatters.com">wpetruck@fundingmatters.com</a></td>
					<td>(416) 249-0788</td>
				</tr>
			</table>
		</div>
	</div>
	<?php include "../footer.php";?>
</div>
</body>
</html>