<?php $activeleft = " class=\"active-left\"" ?>
<div class="left-nav">
	<ul>
		<li><a href="/en/donate/"<?php if($self == '/en/donate/index.php') { echo $activeleft;}?>>Investing in our Future</a></li>
		<li><a href="/en/donate/needs.php"<?php if($self == '/en/donate/needs.php') { echo $activeleft;}?>>Statement of Needs</a></li>
		<li><a href="/en/donate/make_donation.php"<?php if($self == '/en/donate/make_donation.php') { echo $activeleft;}?>>Make a Donation</a></li>
	</ul>
</div>