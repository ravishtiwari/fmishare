<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Japanese Canadian Cultural Centre - Investing in the Future</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
<div class="main-container">
	<?php include "../header.php";?>
	<?php include "donate_nav.php";?>

	<div class="right-content">
		<h1>Investing in the Future</h1>
		<blockquote class="quote">
			The Centre (JCCC) is a living monument to the pioneering spirit of our ancestors and a symbol of our proud Japanese heritage. It is important to secure its future as a cultural resource for the next generation of Japanese Canadians.
		</blockquote>
		<p class="quoter">
			- Sid Ikeda
		</p>
		<p>
			The demand for cultural activities and programming in Canada is constantly increasing. The challenge facing the Japanese Canadian Cultural Centre is to raise the funds necessary to continue meeting the growing needs of our community while maintaining the highest quality of programs and services, operations and maintenance of the facility.
		</p>
		<p>
			Our strategic plan guides activity and focus to ensure our long-term relevance to the changing community. It defines priorities for development which ensures best-in-class performance benchmarks are maintained.
		</p>
		<h3 class="center">
			Leaving a legacy to the Japanese Canadian Cultural Centre will promote and preserve our cultural heritage for future generations.
		</h3>
		<p>
			The JCCC has established the JCCC Foundation to ensure that the Japanese Canadian Cultural Centre’s unique programs, services and facility continue to thrive long into the future. To date, the JCCC Foundation has benefited from the generosity of individuals who have left bequests in their estates. Please contact us to find out more about how you can leave a legacy and play a role in the future of the Japanese Canadian Cultural Centre.
		</p>
		<p>
			Registered charity # 118972967-RR-0001
		</p>
	</div>
	<?php include "../footer.php";?>
</div>
</body>

</html>