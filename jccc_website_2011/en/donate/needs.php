<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Statement of Needs</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../header.php";?>
		<?php include "donate_nav.php";?>
		<div class="right-content">
			<h1>Statement of Needs</h1>
			<blockquote class="quote">
				Over the past 50 years, our community has built one of the largest and most vibrant Japanese cultural centres in the world. Our final challenge is the realization of financial sustainability that will secure the JCCC’s future for generations to come.
			</blockquote>
			<p class="quoter">
				- Marty Kobayashi
			</p>
			<p>
				The JCCC is currently raising funds for the following projects:
			</p>
			<ul>
				<li>Building an endowment for long-term sustainability
				<li>Japanese Canadian heritage programming and <a href="http://sedai.ca" target="_blank">Sedai</a>, the Japanese Canadian legacy project;
				<li>Japanese cultural programs, workshops, and special events
				<li>Creation of a Japanese language library and community space
				<li>Capital Renewal: including upgrades required to the roof, parking lot, exterior facade restoration, signage, communications upgrades, completion of 2nd floor and energy conservation
				<li>Debt retirement
			</ul>
			<h3 class="center">
				Together, through the support, generosity and initiative of the entire community, we can ensure that the Japanese Canadian Cultural Centre remains a vibrant home for the community for years to come.
			</h3>
			<div class="container clearfix hidden">
				<div class="leftbig">
					<img src="/assets/images/needs-ie6.png" class="needs" alt="Statement of Needs">
				</div>
				<div class="rightsmall needs-text">
					<table id="needs">
						<tr>
							<td class="greencell"></td>
							<td>Foundation Building</td>
						</tr>
						<tr>
							<td class="tancell"></td>
							<td>On-going operations</td>
						</tr>
						<tr>
							<td class="redcell"></td>
							<td>Capital Renewal</td>
						</tr>
						<tr>
							<td class="bluecell"></td>
						<td>Debt Retirement</td>
						</tr>
					</table>
				</div>
			</div>
			<p>
			All gifts are appreciated and each will be honored in a meaningful way. Naming opportunities are available. Pledges can be spread over five years. Bequests and tax efficient donating can be discussed with the JCCC and its advisors. To support the Building a Strong Foundation Campaign, contact the JCCC Legacy Committee at (416) 249-0788 ext. 21 or email <a href="mailto:legacy@jccc.on.ca">legacy@jccc.on.ca</a>.
			</p>
		</div>
		<?php include "../footer.php";?>
	</div>
</body>
</html>