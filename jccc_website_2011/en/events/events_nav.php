<?php
/*
special way to list Special Events because it's WordPress
grab page id from Pages section in wordpress (URL)
always check if function_exists otherwise it'll break where it doesn't, i.e. on non-wordpress pages
if statement within an if
*/
$activeleft = " class=\"active-left\"";
?>

<div class="left-nav">
	<ul>
		<li><a href="/en/events/"<?php if($self == '/en/events/index.php') { echo $activeleft;}?>>About our Events</a></li>
		<li><a href="/news_events/"<?php if(function_exists('is_page')) { if((is_page(9)) || ($post_type = 'news_events')) { echo $activeleft;}}?>>Special Events</a></li>
	</ul>
</div>
