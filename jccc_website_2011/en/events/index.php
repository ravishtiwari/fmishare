<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Japanese Canadian Cultural Centre - Events</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../header.php";?>
		<?php include "events_nav.php";?>
		<div class="right-content">
			<h1>Events</h1>
			<blockquote class="quote">
				The Japanese Canadian Cultural Centre celebrates art and culture in all its forms. By providing the platform for a wide spectrum of artists, we bring Japanese culture to life for all Canadians to learn from and enjoy.
			</blockquote>
			<p class="quoter">
				- James Heron, Executive Director
			</p>
			<p>
				Each year, the Japanese Canadian Cultural Centre offers a spectacular array of events that showcase the creative energy and dedication of many Canadian and internationally renowned artists ranging from visual arts to music, dance, film and theatre. These events offer a glimpse into Japanese life, culture and art that are not often seen outside of Japan. In many cases, it may be the first or only opportunity to experience the exhibit or performance in Canada.
			</p>
			<blockquote class="quote">
				Continually evolving and growing more elaborate each year, the traditional festival and holiday celebrations encourage the exploration of culture and develop a sense of appreciation for community traditions.
			</blockquote>
			<p class="quoter">
				- Peter Wakayama
			</p>
			<p>
				Recognized both nationally and internationally, the <a href="/en/programs/activities/jcinema.php">J-Cinema Film Program</a> celebrates the art of cinema by showcasing Japanese films and filmmakers.
			</p>
		</div>
		<?php include "../footer.php";?>
	</div>
</body>
</html>