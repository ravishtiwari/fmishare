<?php include "breadcrumbs.php"; ?>

<div id="footer" class="clearfix">
	<ul>
		<li><a href="/en/rentals/">Rentals</a></li>
		<li><a href="/en/donate/">Donate</a></li>
		<li><a href="#">Press Releases</a></li>
		<li><a href="#">Calendar</a></li>
		<li><a href="/en/contact/">Contact</a></li>
		<li><a href="/en/sitemap.php">Site Map</a></li>
	</ul>
</div>

<div id="socialmedia">
	<div class="container top">
		<div class="float-left">
			<a href="http://www.facebook.com/groups/2264977361/" target="_blank"><img src="/assets/images/facebook.png" alt="Connect with us on Facebook!"></a>
			<a href="http://twitter.com/jccc_toronto" target="_blank"><img src="/assets/images/twitter.png" alt="Follow us on Twitter!"></a>
		</div>
		<div id="font-sizing">
		<?php /*
			<ul>
				<li><a href="#" class="increaseFont">A+</a></li>
				<li><a href="#" class="decreaseFont">A-</a></li>
				<li><a href="#" class="resetFont">reset text size</a></li>
			</ul>
			*/ ?>

			<ul>
				<li><a href="#" class="increaseFont">A+</a></li>
				<li><a href="#" class="decreaseFont">A-</a></li>
				<li><a href="#" class="resetFont">reset text size</a></li>
			</ul>
			
		</div>
	</div>
	<p>
		Japanese Canadian Cultural Centre
		<br>
		6 Garamond Court, Toronto, ON, M3C 1Z5
		<br>
		(416) 441-2345
		<br>
	</p>
</div>