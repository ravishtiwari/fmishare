<?php
/*
see programs_nav.php for comments on how all this PHP stuff works
found in /en/programs/
*/
$uri = $_SERVER['REQUEST_URI'];
$self = $_SERVER['PHP_SELF'];


/* media menu item active */

if (stripos($uri,'gallery')) {
		$mediatrue = true;
		$gallerytrue = true;
}
if (function_exists('is_page')) {
	if ( (is_page(20)) || (is_page(28)) || ($post_type == 'video') || ($post_type == 'audio')) { 
		$mediatrue = true;
	}
}


$classtop = " class=\"active-top\"";

?>

<div id="header">
	<a href="/en/"><img class="logo" src="/assets/images/jccc-logo-300.png" alt="JCCC Logo"></a>
	<div class="languages">
		<a href="/jp/"><img src="/assets/images/jp.png"></a> <a href="/en/"><img src="/assets/images/en.png"></a>
	</div>
	<div class="search">
		<form>
			<input type="text" placeholder="Search...">
		</form>
	</div>
</div>
<!--[if IE]>
<div id="nav-ie">
<![endif]-->
<![if !IE]>
<div id="nav">
<![endif]>
	<ul>
		<li><a href="/en/"<?php if($self == '/en/index.php') { echo $classtop;}?>>Home</a></li>
		<li><a href="/en/about/"<?php if(stripos($uri,'about')) { echo $classtop;}?>>About</a></li>
		<li><a href="/en/membership/"<?php if(stripos($uri,'membership')) { echo $classtop;}?>>Membership</a></li>
		<li><a href="/en/volunteer/"<?php if(stripos($uri,'volunteer')) { echo $classtop;}?>>Volunteer</a></li>
		<li><a href="/en/programs/"<?php if(stripos($uri,'programs')) { echo $classtop;}?>>Programs</a></li>
		<li><a href="/en/events/"<?php if(stripos($uri,'events')) { echo $classtop;}?>>Events</a></li>
		<li><a href="/en/media/"<?php if($mediatrue == true) {echo $classtop;}?>>Media</a></li>
		<li><a href="/en/donate/"<?php if(stripos($uri,'donate')) { echo $classtop;}?>>Donate</a></li>
	</ul>
</div>
<?php include "breadcrumbs.php"; ?>