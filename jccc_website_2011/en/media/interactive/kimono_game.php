<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Sakura Kai</title>
	<link rel="icon" href="/../../assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../media_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/media/kimono_app.jpg" class="programs" alt="Kimono App">
			<h1>Create a Kimono</h1>
			<p>
				"Create a Kimono" game is an online game that will educate youth about Japanese culture and will allow them to create and experiment creatively as they design their own version of the traditional garment. <?php /* Kids can also choose from special collections contributed by local fashion designers.  */?>
			</p>

			<?php /*<p>
			Additional features are available only on the iPad version.  Click <a href="/en/media/interactive/kimono_app_mobile.php" target="_blank">here</a> to download it.
			</p>*/ ?>

			<p>
				<a href="/en/media/interactive/kimono_app_start.php" target="_blank">Play "Create a Kimono"</a> 
			</p>

			<p>
				Note, in order to play the game, you must have Adobe Flash installed.  Please visit <a href="http://get.adobe.com/flashplayer/" target="_blank">http://get.adobe.com/flashplayer/</a>
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>