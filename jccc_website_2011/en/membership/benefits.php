<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Member Benefits</title>
	<link rel="icon" href="/../../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>

	<?php include "membership_nav.php";?>

	<div class="right-content">
		<h1>Benefits of Becoming a Member at the JCCC</h1>
		<p>
		There are many benefits to becoming a member at the JCCC. Members can enjoy discounted rates on class and activity fees, as well as blah blah blah more content required.
		</p>
		<p>
		Become a member of the JCCC and receive 10 issues of the newsletter annually and enjoy discounts on giftshop items and special admission prices to JCCC events. The JCCC welcomes people of all backgrounds, regardless of ethnicity or religion.
		</p>

		<h2>Member Discounts in the Community</h2>
		<div class="container clearfix top">
			<div class="left-benefits first">
				<img src="/assets/images/edo.png" alt="Edo Sushi">
			</div>
			<div class="right-benefits first">
			<p>
			<b>Edo Sushi</b>
			<br>484 Eglinton Ave. W. Toronto
			<br>(416) 322-3033
			</p>
			<p>
				10% Discount for all JCCC members who present a valid membership card
			</p>
				<ul>
					<li>Discounts not offered on Alcoholic Beverages
					<li>Membership card must be presented prior to ordering
					<li>Cash payment only
					<li>Must order a minimum of $25
					<li>This offer is valid until Dec. 31, 2009
				</ul>
			</div>
			<div class="left-benefits">
				<a href="http://www.takarabune.ca/" target="_blank"><img src="/assets/images/takarabune.jpg" alt="Takarabune"></a>
			</div>
			<div class="right-benefits">
				<p>
					<b>Takarabune</b>
					<br>2450 Sheppard Ave. East, Unit 106
					<br>North York
					<br>(416) 491-6688
					<br><a href="http://www.takarabune.ca/" target="_blank">takarabune.ca</a>
				</p>
				<p>
					10% Discount at Takarabune Japanese Restaurant for all JCCC members who present a valid membership card
				</p>
			</div>
			<div class="left-benefits">
				<a href="http://www.tanabatagreentea.com/" target="_blank"><img src="/assets/images/tanabata.jpg" alt="Tanabata"></a>
			</div>
			<div class="right-benefits">
				<p>
					<b>Tanabata Green Tea</b>
					<br><a href="http://www.tanabatagreentea.com/" target="_blank">tanabatagreentea.com</a>
				</p>
				<p>
					10% Discount for all JCCC members who present a valid membership card
				</p>
				<ul>
					<li>Membership card must be presented prior to ordering
					<li>Cash payment only
					<li>Must order a minimum of $25
					<li>This offer is valid until Dec. 31, 2009
				</ul>
			</div>

			<div class="left-benefits">
				<a href="http://www.chordscanada.com/" target="_blank"><img src="/assets/images/chords.gif" alt="Chords Canada"></a>
			</div>

			<div class="right-benefits">
				<p>
					<b>Chords Canada</b>
					<br><a href="http://www.chordscanada.com/" target="_blank">chordscanada.com</a>
				</p>
				<p>
					The JCCC is happy to welcome Chords Canada (div. of Cords Canada Ltd.) as a member of our member benefit program. 
				</p>
				<p>
					Chords Canada is the only stocking distributor of traditional Japanese musical instruments and accessories outside of Japan. In addition to the instruments (including koto, shamisen, jushichigen (bass koto), nijugen (21-string koto), and shakuhachi), Chords stocks a wide range of accessories, sheet music, reference books, CD, audio-visual items, and novelties with a musical theme at their warehouse in Toronto.
				</p>
				<p>
					Chords is offering JCCC members a 5% discount on all products (excluding sheet music, audio-visual products, and custom-ordered items) for orders of $30 or more. If you’d like to know more, call Chords Canada at (416) 242-4967 or 1-877-9CHORDS (toll-free).
				</p>
			</div>

			<div class="left-benefits">
				<a href="http://www.nba.com/raptors/" target="_blank"><img src="/assets/images/raptors.jpg" alt="Toronto Raptors"></a>
			</div>

			<div class="right-benefits">
				<p>
					<b>Toronto Raptors</b>
				</p>
				<p>
					The Toronto Raptors organization is offering discount to all JCCC members for basketball games in the 2008-2009 season.
				</p>
				<p>
					To order tickets at these special JCCC rates, <a href="http://www.ticketmaster.ca/promo/j9hype?brand=raptors" target="_blank">click here</a>. The JCCC member password is: <b>CLOCK</b>
				</p>
			</div>



	</div>
</div>
<?php include "../footer.php";?>


</body>

</html>