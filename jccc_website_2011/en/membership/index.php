<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<title>Japanese Canadian Cultural Centre - Membership</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../header.php";?>

	<?php include "membership_nav.php";?>

	<div class="right-content">
		<h1>Memberships at the JCCC</h1>
		<p>
		Members are very important here at the JCCC.
		</p>

		<h2>Membership Categories</h2>
		<p>
		There are several categories of membership at the JCCC from which to choose.
		</p>
		<table id="programs">
			<tr>
				<th>Membership Type</th>
				<th>Level of Support</th>
				<th>Price</th>
			</tr>
			<tr>
				<td class="bold" rowspan="2">Regular</td>
				<td>Individual (18 or older)</td>
				<td>$30</td>
			</tr>
			<tr>
				<td>Family (including children 17 and younger)</td>
				<td>$50</td>
			</tr>

			<tr>
				<td class="bold" rowspan="2">Senior (65 or older)</td>
				<td>Individual</td>
				<td>$20.60</td>
			</tr>
			<tr>
				<td>Couple</td>
				<td>$35</td>
			</tr>
			<tr>
				<td class="bold" rowspan="3">Supporting Member</td>
				<td>Partner</td>
				<td>$150</td>
			</tr>
			<tr>
				<td>Leader</td>
				<td>$500</td>
			</tr>
			<tr>
				<td>Ambassador</td>
				<td>$1,000</td>
			</tr>
			<tr>
				<td class="bold" rowspan="4">Corporate Member</td>
				<td>Director's Circle</td>
				<td>$150</td>
			</tr>
			<tr>
				<td>Leader's Circle</td>
				<td>$500</td>
			</tr>
			<tr>
				<td>President's Club</td>
				<td>$1,000</td>
			</tr>
			<tr>
				<td>Chairman's Club</td>
				<td>$5,000</td>
			</tr>
		</table>

		<p>
			* An income tax receipt will be issued for the amount in excess of a regular membership fee, i.e. for membership fees of $150 and more.
		</p>
	</div>
	<?php include "../footer.php";?>
</div>


</body>

</html>