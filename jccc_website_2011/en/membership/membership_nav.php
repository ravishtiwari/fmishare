<?php $activeleft = " class=\"active-left\""; ?>
<div class="left-nav">
	<ul>
		<li><a href="/en/membership/"<?php if($self == '/en/membership/index.php') { echo $activeleft;}?>>Memberships at the JCCC</a></li>
		<li><a href="/en/membership/benefits.php"<?php if($self == '/en/membership/benefits.php') { echo $activeleft;}?>>Benefits of Becoming a Member</a></li>
		<li><a href="/en/membership/payfees.php"<?php if($self == '/en/membership/payfees.php') { echo $activeleft;}?>>Become a Member<br>Renew your Membership</a></li>
	</ul>
</div>