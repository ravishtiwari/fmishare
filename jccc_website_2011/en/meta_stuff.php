<?php

$uri = $_SERVER['REQUEST_URI'];
$self = $_SERVER['PHP_SELF'];

/* titles */

$index_title = "";

$about_index_title = "";
$about_board_title = "";
$about_kobayashi_title = "";
$about_mission_title = "";

$contact_title = "";

$donate_index_title = "";
$donate_needs_title = "";
$donate_make_donation_title = "";

$events_index_title = "";

$memb_index_title = "";
$memb_benefits_title = "";
$memb_pay = "";

$volunteer_title = "";

$programs_index_title = "";

$cult_index_title = "";
$cult_japanese_title = "";
$cult_bunka_shishu_title = "";
$cult_shodo_title = "";
$cult_ikebana_title = "";
$cult_sumie_title = "";
$cult_etegami_title = "";
$cult_manju_title = "";
$cult_raku_title = "";
$cult_washi_title = "";

$ma_index_title = "";
$ma_aikido_title = "";
$ma_judo_title = "";
$ma_karate_title = "";
$ma_kendo_title = "";
$ma_iaido_title = "";
$ma_kempo_title = "";
$ma_kyudo_title = "";
$ma_naginata_title = "";

$heritage_title = "";

$discover_japan_title = "";

$act_index_title = "";
$act_cooking_title = "";
$act_origami_title = "";
$act_taiko_title = "";
$act_karaoke_title = "";
$act_music_title = "";
$act_jcomm_title = "";
$act_jsscollab_title = "";
$act_west_east_title = "";
$act_ayame_kai_title = "";
$act_sakura_kai_title = "";
$act_himawari_title = "";
$act_token_kai_title = "";
$act_jcinema_title = "";

$rec_index_title = "";
$rec_yoga_title = "";
$rec_taichi_title = "";
$rec_ohana_hula_title = "";
$rec_hula_health_title = "";
$rec_bridge_title = "";
$rec_pingpong_title = "";
$rec_wynford_title = "";

$kids_title = "";

$school_title = "";

$pay_program_fees_title = "";

$media_index_title = "";
$media_newsletter_title = "";
$media_cookbook_title = "";
$media_kimono_title = "";

$sitemap_title = "";


/* subjects aka keywords */

$index_keywords = "";

$about_index_keywords = "";
$about_board_keywords = "";
$about_kobayashi_keywords = "";
$about_mission_keywords = "";

$contact_keywords = "";

$donate_index_keywords = "";
$donate_needs_keywords = "";
$donate_make_donation_keywords = "";

$events_index_keywords = "";

$memb_index_keywords = "";
$memb_benefits_keywords = "";
$memb_pay = "";

$volunteer_keywords = "";

$programs_index_keywords = "";

$cult_index_keywords = "";
$cult_japanese_keywords = "";
$cult_bunka_shishu_keywords = "";
$cult_shodo_keywords = "";
$cult_ikebana_keywords = "";
$cult_sumie_keywords = "";
$cult_etegami_keywords = "";
$cult_manju_keywords = "";
$cult_raku_keywords = "";
$cult_washi_keywords = "";

$ma_index_keywords = "";
$ma_aikido_keywords = "";
$ma_judo_keywords = "";
$ma_karate_keywords = "";
$ma_kendo_keywords = "";
$ma_iaido_keywords = "";
$ma_kempo_keywords = "";
$ma_kyudo_keywords = "";
$ma_naginata_keywords = "";

$heritage_keywords = "";

$discover_japan_keywords = "";

$act_index_keywords = "";
$act_cooking_keywords = "";
$act_origami_keywords = "";
$act_taiko_keywords = "";
$act_karaoke_keywords = "";
$act_music_keywords = "";
$act_jcomm_keywords = "";
$act_jsscollab_keywords = "";
$act_west_east_keywords = "";
$act_ayame_kai_keywords = "";
$act_sakura_kai_keywords = "";
$act_himawari_keywords = "";
$act_token_kai_keywords = "";
$act_jcinema_keywords = "";

$rec_index_keywords = "";
$rec_yoga_keywords = "";
$rec_taichi_keywords = "";
$rec_ohana_hula_keywords = "";
$rec_hula_health_keywords = "";
$rec_bridge_keywords = "";
$rec_pingpong_keywords = "";
$rec_wynford_keywords = "";

$kids_keywords = "";

$school_keywords = "";

$pay_program_fees_keywords = "";

$media_index_keywords = "";
$media_newsletter_keywords = "";
$media_cookbook_keywords = "";
$media_kimono_keywords = "";

$sitemap_keywords = "";



/* set title */






/* set full title */
$full_title = $title." - Japanese Canadian Cultural Centre";

?>


<meta name="DC.title" content="<?php echo $full_title; ?>">
<meta name="DC.creator" content="Japanese Canadian Cultural Centre">
<meta name="DC.language" scheme="ISO639-2/T" content="eng">
<meta name="DC.date.created" content="2012-01-07"<?php /* YYYY-MM-DD */ ?>>
<meta name="DC.identifier" scheme="uri" content="<?php echo $self; ?>">
<meta name="DC.subject" scheme="gccore" content="<?php echo $keywords; ?>">




