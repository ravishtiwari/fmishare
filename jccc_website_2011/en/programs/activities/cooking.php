<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Japanese Cooking Classes</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/sushi.jpg" class="programs" alt="Sushi">
			<h1>Japanese Cooking Classes</h1>
			<p>
				Chef Shoji is offering 5 hands-on cooking workshops at the JCCC. Focusing on taste, techniques and special ingredients that made Japanese cuisine unique, "Chef Shoji" wishes to show that Japanese food is not only easy to prepare and serve, but is also healthy and delicious. Choose one or two workshops, or sign up for them all!
			</p>
			<p>
				5 workshops. Each workshop is limited to 20 students.
			</p>
			<img src="/assets/images/miso.jpg" class="programs-small" alt="Miso">

			<h4>Instructor</h4>
			<p>
				Chef Shoji has been teaching cooking classes at the Canadian Japanese Cultural Centre in Hamilton for the past two years. Over this time, he built up quite a repertoire of Japanese reciptes. He has recently developed some new and scrumptious recipes for Mochi and Manju (Japanese rice cake sweets).
			</p>
			<h4>Classes</h4>
			<p>
				Each workshop consists of 1 hour instruction and demonstration, 1 hour hands-on cooking and last hour for tasting and cleaning.
			</p>

			<h4>Fees</h4>
			<p>
				Each workshop is $30 for members and $40 for the general public. A material fee of $10 is payable to the instructor.
			</p>

			<h4>Workshop Time</h4>
			<p>
				All workshops are held from 7:00 pm to 10:00 pm on the specified dates.
			</p>

			<h4>Materials</h4>
			<p>
				Please bring an apron, a large sharp knife and a paring knife.
			</p>
			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Subject</th>
				</tr>
				<tr>
					<td>Wednesday, October 26, 2011</td>
					<td>Steamed Dishes</td>
				</tr>
				<tr>
					<td>Wednesday, November 23, 2011</td>
					<td>Advanced Sushi</td>
				</tr>
				<tr>
					<td>Wednesday, January 25, 2012</td>
					<td>Bento</td>
				</tr>
				<tr>
					<td>Wednesday, February 22, 2012</td>
					<td>Noodles</td>
				</tr>
			</table>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>