<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - J-Cinema</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="J-Cinema">
			<h1>J-Cinema</h1>
			<p>
				Recognized both nationally and internationally, the J-Cinema Film Program celebrates the art of cinema by showcasing Japanese films and filmmakers. Participants expand their knowledge of cinema and enhance their appreciation of the filmmaking process. The J-Cinema Program is comprised of four elements: 
			</p>
			<ol>
				<li>Monthly J-Cinema Film Screenings in the JCCC’s Kobayashi Hall
				<li>The Annual Shinsedai Cinema Festival
				<li>Special Film Events such as screenings and Q&A’s with top Japanese directors
				<li>Family Anime Days
			</ol>
			<p>
				Every month, as part of our ongoing cultural programming, the Japanese Canadian Cultural Centre screens various Japanese films using state-of-the-art facilities in the Kobayashi Hall. These screenings include a diverse offering of contemporary hit films by prominent filmmakers and directors such as Kitano Takeshi, Hayao Miyazaki, Miwa Nishikawa, Yojiro Takita and Hirokazu Koreeda.
			</p>
			<img src="/assets/images/?.jpg" class="programs" alt="J-Cinema">
			<p>
				These films are usually premiered in Canadian or Toronto screenings and are selected to reflect "popular" films rather than "art house" films. The aim of these screenings is to provide accessible and entertaining films, while at the same time, offer insights into the interests and viewing habits of the average Japanese person. Many of these films are also selected from the top winners of the annual Japanese Academy awards and international film festivals. In particular, the J-Cinema monthly film screenings focus on films that are favourites in Japan but do not have a North American release. As a result, the J-Cinema film nights are entirely unique and rare opportunities for Canadians to experience the cinema that are loved by the Japanese.
			</p>
			<p>
				All films in Japanese language are shown with English subtitles.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>