<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Music</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Yamaha Music">
			<h1>Yamaha Music Course</h1>
			<h3>Keyboard</h3>
			<p>
				Free introductory lessons! LEarn to play the electronic keyboard using the world renowned Yamaha Music Education System. Music of all styles is taught in a motivating group setting. Class times are available for both seniors and adults. 
			</p>
			<p>
			Beginner, Intermediate and Advanced, and Basic Theory Classes available. Please call for up to date information on specific times. There are 10 classes per session. Sessions run year round.
			</p>
			<?php /*
			<table id="programs">
				<tr>
					<th>Start Date</th>
					<th>Times</th>
					<th>Daytime Fee</th>
					<th>Evening Fee</th>
					<th>Private Lesson</th>
				</tr>
				<tr>
					<td>Please call</td>
					<td>Wednesday 10:00am-7:00pm
				</tr>
			</table>
			*/ ?>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>