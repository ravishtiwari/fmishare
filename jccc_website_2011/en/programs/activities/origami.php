<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Origami</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/origami.jpg" class="programs" alt="Paper Crane">
			<h1>Origami</h1>
			<p>
				This unique new series of Origami workshops kicks off again with John Jay Guppy from the Origami Society of Toronto. Folding both traditional favourites and the latest new models, this gives everyone a chance to try their skill with plenty of expert help available. All paper is provided.
			</p>
			<table id="programs">
				<tr>
					<th>Dates</th>
					<th>Time</th>
					<th>Fee</th>
				</tr>
				<tr>
					<td>Thursday October 27, 2011</td>
					<td>7:00-9:00pm</td>
					<td>$7 per class for members<br>$10 per class for the general public</td>
				</tr>
			</table>
			<p>
				To learn more about the Origami Society of Toronto please visit their <a href="http://origamitoronto.org" target="_blank">website</a>.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>