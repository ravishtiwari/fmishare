<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Sakura Kai</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Sakura Kai">
			<h1>Sakura Kai</h1>
			<p>
				For over fifty years, Sakura Kai has taught the graceful skills of odori dancing to students 5 years and older. Learn traditional folk (minyo) and semi-classical (buyo) dances largely influenced by the Fujima-ryu style.
			</p>
			<p>
				Classes are held on Sunday afternoons. 
			</p>
			<p>
				Please contact Irene Iseki at by <a href="mailto:sakurakai_odori@yahoo.ca">email</a> or call (416) 267-0270 for more information.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>