<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Taiko</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/taiko1.jpg" class="programs" alt="Taiko">
			<h1>Taiko</h1>
			<p>
				The JCCC and the Yakudo Japanese Drummers are joining forces to offer an exciting hands-on Japanese drumming workshop. Sign up for a unique and rare opportunity in North America to learn and play on real authentic Japanese Taiko. Try out Taiko for the first time or build on basic Taiko skills that you already have.Pre-registration required. All equipment and necessities are provided.
			</p>
			<h2>Fundamentals of Taiko</h2>
			<p>
				Learn the fundamentals of taiko playing Yakudo style – a combination of martial arts and music. Develop the taiko spirit with a solid base. This is highly recommended if you continue with more taiko lessons or specialized workshops with Yakudo. 10 people max.
			</p>
			<h2>Taiko Plus 1: Rhythm and Tempo</h2> 
			<p>
				Using the Fundamentals of Taiko Playing, this workshop focuses on developing your sense of rhythm and gaining control. 10 people max. 
			</p>
			<h2>Taiko Plus 2: Form and Endurance</h2>
			<p>
				Using the fundamentals of Taiko Playing, this workshop focuses on developing form and endurance for the visual aspect of Taiko ensemble playing. This workshop has also been termed as Taiko Bootcamp! 14 people max. This will be a vigorous workout!
			</p>
			<h2>Kodaiko Workshop</h2>
			<p>
				Learn all about Taiko and its ties to Japanese culture. Also learn fundamentals of taiko and rhythm, through fun activities and games played on authentic instruments. Create and take home your own taiko souvenir. 10 people max. For children ages 4 to 7.
			</p>
			<h2>Sumodaiko Workshop</h2>
			<p>
				Learn the fundamentals of taiko playing Yakudo style – a combination of martial arts and music. Develop the taiko spirit with a solid base and be able to perform a fun traditional taiko piece at the end of the workshop. 10 people max. For ages 8 to 14.
			</p>
			<table id="programs">
				<tr>
					<th>Workshop</th>
					<th>Date</th>
					<th>Time</th>
					<th>Fee</th>
				</tr>
				<tr>
					<td rowspan="3" class="bold">Fundamentals of Taiko</td>
					<td rowspan="2">Saturday, February 11, 2012</td>
					<td>8:30am-11:30am</td>
					<td rowspan="3">General: $65<br>Members: $60</td>
				</tr>
				<tr>
					<td>12:00-3:00pm</td>
				</tr>
				<tr>
					<td>Sunday, February 12, 2012</td>
					<td>9:00am-12:00pm</td>
				</tr>
				<tr>
					<td class="bold">Taiko Plus 1</td>
					<td>Saturday, February 11, 2012</td>
					<td>3:30-5:30pm</td>
					<td rowspan="4">General: $45<br>Members: $40</td>
				</tr>
				<tr>
					<td class="bold">Taiko Plus 2</td>
					<td>Saturday, February 11, 2012</td>
					<td>5:30-7:30pm</td>
				</tr>
				<tr>
					<td class="bold">Kodaiko</td>
					<td>Sunday, February 12, 2012</td>
					<td>12:30-2:00pm</td>
				</tr>
				<tr>
					<td class="bold">Sumodaiko</td>
					<td>Sunday, February 12, 2012</td>
					<td>2:00-3:30pm</td>
				</tr>
			</table>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>