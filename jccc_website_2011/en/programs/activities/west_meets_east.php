<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - West Meets East</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/??.jpg" class="programs" alt="West Meets East">
			<h1>West Meets East</h1>
			<h2>Japanese Business and Protocol Training</h2>
			<p>
				Through West Meets East human resource training seminars, the JCCC provides Canadian businesses with the knowledge and strategies crucial to establishing and maintaining productive relationships with Japanese organizations and individuals. 
			</p>
			<p>
				Since 1987, West Meets East has been helping North American firms, as well as educational and government organizations to deal effectively with obstacles and create profitable relationships. 
			</p>
			<p>
				Please call James Heron at (416) 441-2345 ext. 224 for more information.
			</p>
		</div>
	<?php include "../../footer.php";?>
	</div>
</body>
</html>