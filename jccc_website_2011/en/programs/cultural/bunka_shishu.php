<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Bunka Shishu</title>
	<link rel="icon" href="/../../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>

		<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Bunka Shishu">
			<h1>Bunka Shishu</h1>
			<p>
				Bunka Shishu, Japanese “Needle Punch” embroidery, uses rayon lily thread combined with specific stitching techniques to create lovely pieces of art. Students begin by stitching a small pictoral sampler and progress on to kits depicting traditional Japanese scenes, flowers, animals, or people. 10 Classes.
			</p>
			<h2>Fees</h2>
			<p>
				Course is $72.50 for adults and $45 for seniors. Materials are extra and are payable to the instructor. In order to attend this course, you must hold a valid JCCC membership.
			</p>
			<h2>Instructor</h2>
			<p>
				Patricia Bremner-Ikeno
			</p>
			<table id="programs">
				<tr>
					<th>Start Date</th>
					<th>Time</th>
				</tr>
				<tr>
					<td>Monday, September 26, 2011</td>
					<td>7:30-9:30pm</td>
				</tr>
				<tr>
					<td>Tuesday, September 27, 2011</td>
					<td>10:00am-12:00pm</td>
				</tr>
			</table>
		</div>
	<?php include "../../footer.php";?>
</div>



</body>

</html>