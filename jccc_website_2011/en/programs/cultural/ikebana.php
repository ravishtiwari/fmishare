<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Ikebana</title>
	<link rel="icon" href="/../../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>

		<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Ikebana">
			<h1>Ikebana</h1>
			<p>
				Ikebana is the art of arranging flowers or plants to reflect their natural beauty. Its originated in early Buddhist years and developed into a distinctive art form in the 15th century, with many different styles and schools. The JCCC offers classes by the Ikenobo School and the Sogetsu School.
			</p>

			<h2>Ikenobo</h2>
			<p>
				Like a poem or painting made with flowers, Ikenobo expresses both the beauty of flowers and the beauty of longing in your heart. Ikenobo considers a flower bud most beautiful ... past, present and future ... responding to an everchanging environment. You will take home your creations.
			</p>
			<h3>Instructor</h3>
			<p>
				Betty Lou Arai is a Professor of Ikebana-Ikenobo School, headquartered in Kyoto. She has studied in Japan and has been teaching since 1977. The styles of Ikenobo are always changing and Mrs. Arai teaches both modern and traditional styles.
			</p>
			
			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Level</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>

			<h2>Sogetsu</h2>
			<p>
				The Sogetsu School of Ikebana believes anyone can arrange Ikebana anywhere, with anything. As people are different from each other, Sogetsu encourages students to be individual and imaginative. There are no limitations and a variety of materials are used depending on the season. You will take home your creations.
			</p>
			<h3>Instructor</h3>
			<p>
				Kyoko Abe took her first Ikebana lesson in junior high school in Sendai, Japan where she received her diploma and is currently director of the Toronto East Branch of Sogetsu school. She was awarded the "Merit Award" in 1975 and "Honorable Mention" in 1997 for her achievements from the headmaster in Japan.
			</p>
			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Level</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>

			<h2>Fees</h2>
			<p>
				$75 for members. Materials are extra. Starter Kit, as well as scissors, containers, kenzan and kenzan case are available at JCCC Giftshop.
			</p>
		</div>
<?php include "../../footer.php";?>
</div>



</body>

</html>