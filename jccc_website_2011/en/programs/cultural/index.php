<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Cultural Classes</title>
	<link rel="icon" href="/../../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>

		<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="" class="programs" alt="Cultural Classes">
			<h1>Cultural Classes</h1>
			<blockquote class="quote">
				Japanese Canadian culture will continue to thrive and evolve as a new generation unlocks their creativity and discovers the beauty, grace and balance in learning traditional and contemporary Japanese arts.
			</blockquote>
			<p class="quoter">
				- Raymond Moriyama
			</p>
			<p>
				For over forty years, the cultural arts program at the Japanese Canadian Cultural Centre, has introduced traditional Japanese arts to the Canadian community at large. Led by many dedicated and talented volunteer instructors, the JCCC cultural arts program has endeavored to cultivate an appreciation for Japanese arts and develop the skills necessary to enjoy participation.
			</p>
		</div>

<?php include "../../footer.php";?>
</div>



</body>

</html>