<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Manju</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Manju">
			<h1>Manju</h1>
			<p>
				The ever-popular Manju (Japanese rice cake sweets) is back! "Chef Shoji" will be conducting 2 hands-on workshops at the JCCC.
			</p>
			<p>
				Chef Shoji has been teaching cooking classes at the Canadian Japanese Cultural Centre in Hamilton for the past few years. Over this time, he built up quite a repertoire of Japanese recipes. He has recently developed some new and scrumptious recipes for Mochi and Manju (Japanese rice cake sweets).
			</p>
			<h2>Chef Shoji's Introductory Manju Workshop</h2>
			<p>
				Learn to make Dorayaki (pancake manju), Ohagi (red bean paste covered sweet rice), Kinako Mochi (toasted soy bean flour covered sweet rice), Azuki (red beans) and Lima bean filling for Manju. See Chef Shoji demonstrate his 10-minute Mochi Manju technique. Limited to 20 students.
			</p>
			<h2>Chef Shoki’s Advanced Manju Workshop</h2>
			<p>
				Learn to make Mushi Manju (steamed), Yaki Manju (baked) and next generation Mochi Manju. See Chef Shoji demonstrate his 10-minute Mochi Manju technique (advanced). Limited to 20 students.
			</p>
			<h3>Fee</h3>
			<p>
				$25 per workshop for members. $30 for non-members. $10 for materials.
			</p>
			<h3>Notes</h3>
			<p>
				Please bring an apron. Schedule is subject to change; please call to confirm before booking.
			</p>

			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Wednesday March 23</td>
					<td>7:00-10:00pm</td>
					<td>Introductory</td>
				</tr>
				<tr>
					<td>Wednesday April 20</td>
					<td>7:00-10:00pm</td>
					<td>Introductory</td>
				</tr>
				<tr>
					<td>Wednesday May 25</td>
					<td>7:00-10:00pm</td>
					<td>Advanced</td>
				</tr>
			</table>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>