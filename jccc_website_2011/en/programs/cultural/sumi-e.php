<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Sumi-e</title>
	<link rel="icon" href="/../../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>

		<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Sumi-E">
			<h1>Sumi-e</h1>
			<p>
				This traditional painting technique uses black ink or subtle watercolours. The quality of the painting lies in the strength of the line, which depends on the artists ability to control the brush; shading or colours and the composition.
			</p>
			<p>
				This course is open for students who have 3 years or more experience in Sumi-e. During this course, students will develop their styles and create artworks of their own and also study Sumi-e techniques of specific historical Japanese artists.
			</p>
			<h2>Instructor</h2>
			<p>
				Hiroshi Yamamoto studied Japanese Traditional Art and Art History at the Kyoto Industrial Design Institute Japan and the Kyoto Japanese Art School. Hiroshi has held more than 20 Sumi-e and Nihonga exhibitions in Canada as well as in Japan. He has taught Sumi-e and Nihonga for 10 years.
			</p>
			<p>
				Natalie Griller (Haku-Sho) - Studied sumi-e in Toronto with Mrs. Aiko Morton and Mr. Hiroshi Yamamoto; studied painting at the Winnipeg Forum Art Institute and the Unviersity of Manitoba. She has sold her art for over 20 years, with sumi-e paintings currently being represented at galleries in Yellowknife and Toronto. She has taught painting since 2002.
			</p>
			<p>
				8 classes
			</p>
			<h2>Fees</h2>
			<p>
				$70 for members. Materials are extra. Starter Kit (brushes, brush holder, ink stick, inkstone and rice paper) available at the JCCC Giftshop.
			</p>
			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Monday, September 19</td>
					<td>7:30-9:30pm</td>
					<td>Beginner</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>