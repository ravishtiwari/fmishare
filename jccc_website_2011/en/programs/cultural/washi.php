<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Washi</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	
	<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Washi">
			<h1>Washi Greeting Cards & Nakayoshi</h1>
			<h2>Washi Greeting Cards Craft</h2>
			<p>
				Many people admire the beautiful hand-made Japanese cards that are sold at the JCCC gift shop. You may be one of them. If you are, you will be interested in a workshop on making greeting cards using Japanese Washi and Origami paper. Kyoko Sugita will again be conducting this workshop. You will learn how to fold origami such as Japanese kimono boys and girls. During the workshop you will make two cards with Kyoko’s step by step instruction. What you will also take away from the workshop is the ability to make cards that will be cherished by your family and friends. Limited to 12 students. Pre-registration required.
			</p>
			<h4>Instructor</h4>
			<p>
				Kyoko Sugita
			</p>

			<h3>Fee</h3>
			<p>
				$30 for members, $35 for non-members. Includes 2 kids, each including 2 cards, 4 dolls (2 dolls for in-class, 2 to take home to make your own cards).
			</p>

			<h3>Materials</h3>
			<p>
				Please bring scissors and school, craft or stick glue.
			</p>

			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Level</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>


			<h2>Washi Nakayoshi</h2>
			<p>
				Create this exquisite washi craft depicting two dear friends, a boy and a girl. which is mounted on a shikishi board. They are made with special textured chiyogami [washi] from Japan and are mounted on a shikishi board. The finished picture would be suitable for framing or may be displayed as is. Participants will need to bring sharp scissors, white glue, a pencil and ruler. The JCCC’s own popular washi craft teacher, Kyoko Sugita will instruct this full day class. Limited to 12 students.
			</p>

			<table id="programs">
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Level</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>

		</div>
<?php include "../../footer.php";?>
</div>
</body>
</html>