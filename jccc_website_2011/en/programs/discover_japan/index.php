<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Discover Japan</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<h1>The JCCC Hakken Discover Japan Program</h1>
			<p>
				Hakken means Discover in Japanese and this is symbolic of the JCCC Hakken Japan Program. In many cases this program will be the first step for many in experiencing Japan and Japanese Canadian Culture. The JCCC Hakken Japan Program is important because it will provide various communities in Ontario the opportunity to experience the community history and cultural heritage of the Japanese in Canada.
			</p>
			<p>
				Although schools are able to teach children the history of the Japanese in Canada, there is currently no other program or school board who can afford to train or to teach children about the art and culture of the Japanese in Canada. The JCCC Hakken Japan program is an important program that teaches children the Culture of Japan, not only providing knowledge and skills, but giving children the philosophy and understanding that will better equip Canadians for the 21st century.
			</p>
			<img src="d" class="programs" alt="Discover Japan">
			<p>
				This is a collaborative effort led by the Japanese Canadian Cultural Centre and involves reaching out to the broader community within the province of  Ontario.  Partnerships will be developed by way of the Hakken (Discover) Japan Advisory Committee (HJC), comprised of various stakeholders and potential stakeholders , to ensure an effective program delivery mechanism exists. 
			</p>
			<p>
				This program also assists Ontario in building more partnerships within the Japanese community both at home and abroad. Within 1.5 hour radius of the JCCC, our current geographic outreach, represents more than $11 billion of Japanese investment in the province of Ontario. This program will both acknowledge and reinforce the importance of Japanese investment that has been made to this region. By letting Japanese corporations participate in the hosting of the JCCC Hakken Japan Programming it will allow for stronger goodwill within the existing partnerships and hopefully allow for the creation of other partnerships. Japanese culture, like Canadian culture, places a very high regard for education and cultural understanding. A program such as the JCCC Hakken Japan Program reinforces these values and displays to international businesses that Ontario is a well educated and welcoming environment, and is a worthwhile environment for business. By supporting the JCCC Hakken Japan Program the Ontario Trillium Foundation would support not only the further education of Ontario communities, but also a more secure and welcoming environment for foreign investment in Ontario.
			</p>
			<img src="d" class="programs" alt="Discover Japan">
			<p>
				Each Committee member has two roles to play:  identify and bring their community’s needs to the Hakken Japan Program at the JCCC and across the province, and take the work of the Hakken Japan program back to their communities.  School boards will help build the curriculum component for the overall outreach programs into the schools and organize visits to the regions in the province.
			</p>
			<p>
				The JCCC Hakken Japan Program will help increase awareness of Japan to school children as related tourism by 10% each year.  620,000 annual Japanese tourists may prolong their visit and thus increase their financial impact on the economy.
			</p>
			<p>
				The Japanese-Canadian community has displayed its support for the Hakken Japan Program by donating valuable time and experience to the project.
			</p>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>