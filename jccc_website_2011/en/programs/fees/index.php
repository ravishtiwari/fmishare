<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>

	<title>Japanese Canadian Cultural Centre - Program Fees</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<h1>Pay your Program Fees Online</h1>
			<p>
				To register for programming and to pay your program fees online, please visit our <a>secure site</a>.
			</p>
			<p>
				You may also call us or visit us in person during administrative hours.
			</p>
			
			<h2>Refunds & Reimbursement</h2>
			<h3>Policies for Classes & Workshops</h3>
			Students are required to be members of JCCC. Membership and class/workshop fees are payable in advance. A $10 administration fee will be charged on returned cheques. Students whose class fees are not up to date (i.e. martial arts students who have not paid in full for the current month by the 15th of that month) will not be permitted to participate in classes until their fees are current. Waiver forms must be completed for each martial arts student.
			
			<h3>Refund</h3>
			Class refunds are available before the 2nd class only, a 15% administrative fee will be charged. Membership fees are not refundable.

			<h3>Reimbursement</h3>
			Classes cancelled by the JCCC will be rescheduled, however classes missed by students will not be reimbursed. If you know in advance that you will be absent for an extended period, please let us know in writing. A credit may be given for that period.

			<h3>Cancellation</h3>
			A written notice is required should you decide to withdraw from a class. Without formal notification of withdrawal, a student's status will continue to be considered active and program fees will continue to accumulate.
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>