<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Heritage</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>

	<div class="right-content">
		<h1>Heritage</h1>
		<p>
			Based out of the JCCC’s Nikkei Heritage Resource Centre, the JCCC Heritage Committee strives to introduce the culture, history and legacy of the Japanese Canadians to all Canadians, while creating a tribute to the history of the Nikkei community and their contributions to the building of our nation. Heritage activities serve as an important focal point for the community.
		</p>
		<blockquote class="quote">
			Through my experiences at JCCC Heritage events, I have reconnected with the community. I look forward to hearing the unique stories of the Nisei and eating the food of my childhood.
		</blockquote>
		<p class="quoter">
			- Ann Ashley
		</p>
		<p>
			<a href="/en/programs/discover_japan/">Discover Japan</a> is a vital educational program serving, in many cases, as a student’s first exposure to Japan and Japanese Canadian culture. The program attracts students from across the province of Ontario with a primary focus on the Greater Toronto Area. Participants are fully immersed in Japanese and Japanese Canadian culture, traditions, and history through informative lectures, videos and hands on demonstrations. Discover Japan offers introductory classes on the art of the Tea Ceremony, Japanese writing and calligraphy, haiku, story-telling, kimono, toys, games, crafts, origami, cuisine, festivals, traditional music, dance, the history of Japan and Japanese Canadians, and Japan-Canada trade relations.
		</p>
	</div>
<?php include "../../footer.php";?>
</div>
</body>
</html>