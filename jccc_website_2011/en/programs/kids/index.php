<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Japanese Canadian Cultural Centre - Kids and Families</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/kids_families.jpg" class="programs" alt="Kids & Families">
			<h1>Activities for Kids & Families</h1>
			<p>Content is required for this section.</p>

			<h2>Kamp Kodomo</h2>
			<p>
				Kamp Kodomo is a stimulating and educational Japanese and Japanese-Canadian cultural experience for children, ages 6 to 12 years. The day camp features hands-on sessions including:
			</p>
			<ul>
				<li>Drama
				<li>Martial Arts 
				<li>Music 
				<li>Cooking 
				<li>Language
				<li>History
				<li>Arts and Crafts
				<li>Geography
				<li>Dance and more fun!
			</ul>

			<h3>Dates</h3>
			<p>
				There are several one-week sessions of camp held on March break and in July. Please contact the JCCC for more information.
			</p>

			<h3>Time</h3>
			<p>
				Camp is held from 9:00am to 4:30pm. 
			</p>
			<p>
				Extended camp care is available daily from 8:00am to 9:00am and from 4:30pm to 6:00pm.
			</p>

			<h3>Fees</h3>
			<p>
				$180 per five-day session.
			<br>
				10% discount for members.
			<br>
				5% discount for additional siblings attending the same session
			</p>
			<p>
				There are two snacks each day and one special activity day per week that may include an offsite trip. Children must bring a nutritious lunch and drink each day.
			</p>

			<h3>Registration Deadline</h3>
			<p>
				Please register by March 10th, 2012
			</p>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>