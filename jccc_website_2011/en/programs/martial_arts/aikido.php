<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Aikido</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/aikido1.jpg" class="programs" alt="Aikido">
			<h1>Aikido</h1>
			<p>
				Aikido's founder, Morihei Ueshiba (1883-1969) or O-sensei, "the great teacher", was a Japanese soldier and philosopher. He founded The Art of Peace or, "way of harmonizing energy" after many years of studying traditional Japanese martial arts including Kendo and Ju-jitsu. Morihei developed the circular, flowing movements of Aikido to blend the energy of the attack and redirect these forces harmlessly. Practioners train to develop their Ki or inner energy as well as their body during vigorous activity.
			</p>
			<p>
				The JCCC Aikikai first opened in 1969 and is a member of the Aikikai Honbu Dojo, Japan (the world headquarters of the International Aikido Foundation), the Ontario Aikido Federation, and the Canadian Aikido Federation. The dojo's chief instructor is Osamu Obata Shihan, 7th Dan. Obata Shihan began teaching at the JCCC in 1972, after learning Aikido in Japan. Obata Shihan teaches circular movements that blend the power of the attacker with the power of the defender. The secret of Aikido is revealed in the Japanese proverb: one does not have to fight in order to win
			</p>
			
			<img src="/assets/images/aikido2.jpg" class="programs" alt="Aikido">

			<p>
				Aikido offers both physical training and discipline. The JCCC Aikikai offers both regular classes and specialized weapons training using the traditional bokken or practice wooden sword, jo or staff, an tanto or short sword/knife. Aikido is a non-competitive, defensive martial art open to both genders, all ages and abilities (starting age for children is 6).
			</p>
			<h4>Obata Shihan leads a team of instructors:</h4>
			<blockquote>
				Osamu Obata, 7th Dan Shihan<br>
				Paul Sunn, 6th Dan, <br>
				Yoshi Ichida, 5th Dan, <br>
				Camber Muir, 3rd Dan, <br>
				Masoud Shahidzaedeh, 3rd Dan, <br>
				Adrian Illescu, 3rd Dan, <br>
				Masaru Matsubara, 2nd Dan, <br>
				Collin Brown, 2nd Dan,
			</blockquote>
			<p>
				For more information and to obtain a beginner's instruction kit, please visit <a href="http://www.jcccaikikai.ca/" target="_blank">JCCC Aikika</a>.
			</p>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Monday, Wednesday, Friday</td>
					<td>7:00-8:00am</td>
					<td>all ages</td>
					<td>General</td>
				</tr>
				<tr>
					<td rowspan="2">Tuesday, Thursday</td>
					<td>6:30-8:00pm</td>
					<td>* see below</td>
					<td>General</td>
				</tr>
				<tr>
					<td>8:00pm-9:30</td>
					<td>all ages</td>
					<td>Advanced</td>
				</tr>
				<tr>
					<td>Saturday</td>
					<td>11:30am-12:30pm</td>
					<td>all ages</td>
					<td>General, Women Only</td>
				</tr>
				<tr>
					<td rowspan="3">Sunday</td>
					<td>9:00-10:00am</td>
					<td>all ages</td>
					<td>Weapons (Beginner/General)</td>
				</tr>
				<tr>
					<td>10:00-11:30am</td>
					<td>all ages</td>
					<td>General</td>
				</tr>
				<tr>
					<td>11:40am-1:00pm</td>
					<td>children only</td>
					<td>General</td>
				</tr>
			</table>
			<p class="italics">
				* General: able to do aikido front and back roll comfortably <br>
				* Advanced: 5th kyu rank required <br>
				* Children are welcome to attend the Tuesday General class <br>
				* New aikido students accepted only at the start of the month.
			</p>
			<p>
				All classes are open to youth (15-21 years old), adults (22-64 years old) and seniors (65 years and older) unless otherwise indicated.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>