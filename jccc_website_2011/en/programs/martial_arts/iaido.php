<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Iaido</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<img src="/assets/images/iaido.jpg" class="programs" alt="Iaido">
			<h1>Iaido</h1>
			<p>
				The art of Iaido would seem to be a simple one. The student sits or stands quietly, draws out a blade and cuts through the air all in one stroke, then puts the blade away again. To the casual observer there is not a lot to see.
			</p>
			<img src="/assets/images/iaido3.jpg" class="programs-small" alt="Iaido">
			<p>
				However, Iaido is an exacting art that demands a high level of mental concentration. It is an art of precise motions with only centimeters of tolerance and split second timing.It is also an individual art that involves the student and the sword struggling to achieve perfection of form.
			</p>
			<p>
				Beyond this physical aspect, Iaido is perhaps the most philosophically oriented of all of the Japanese budo arts. The name "Iaido" itself is composed of three ideograms "I, "AI" and "DO"."I" means to "reside" or "be" in a certain place; "AI" means "harmony"; and "DO" means "road" or "path". Hence, "Iaido" means the path to finding harmony in any situation in which one finds oneself - to be prepared for any eventuality.
			</p>
			
			<p>
				Iaido is a very traditional martial art. Japanese culture, etiquette and history are all important aspects of Iaido taught at the JCCC. Both the traditional forms of Muso Jikiden Eishin Ryu, which originate in the late 16th century, and the modern forms developed by the All Japan Kendo Federation are taught.
			</p>
			<img src="/assets/images/iaido2.jpg" class="programs-small" alt="Iaido">
			<p>
				Classes at the JCCC are taught by Goyo Ohmi 7th Dan renshi; Carole Galligan, 6th Dan; Enore Gardonio, 5th Dan; Peter Schramek, 5th Dan; Tracy Sheppard, 5th Dan and Michael Gan, 4th Dan.
			</p>
			<p>
				Beginners need only wear loose clothing. Kneepads are recommended.A limited supply of bokuto are available for those who do not have one. All levels are welcome.
			</p>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Sunday</td>
					<td>1:00-3:00pm</td>
					<td>all ages</td>
					<td>General</td>
				</tr>
				<tr>
					<td>Thursday</td>
					<td>9:15-10:45pm</td>
					<td>all ages</td>
					<td>General</td>
				</tr>
			</table>
			<p class="italics">
				* New iaido students accepted only during an orientation class (January 9, April 3, July 3 and October 2). 
			</p>
			<p>
				For more information please visit <a href="http://jccciaido.com" target="_blank">JCCC Iaido</a>.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>