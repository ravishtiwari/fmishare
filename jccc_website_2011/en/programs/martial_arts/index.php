<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Martial Arts</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/martial_arts.jpg" class="programs" alt="Martial Arts">
			<h1>Martial Arts</h1>
			<p>
				Martial arts is a fundamental program of the Japanese Canadian Cultural Centre. Research shows that people who regularly practice martial arts demonstrate incredible levels of physical fitness in comparison to people the same age who don’t exercise. As a result, they live longer, healthier, happier lives.
			</p>
			<blockquote class="quote">
			Over the years, martial arts has been one of the JCCC’s cornerstones in introducing Japanese culture to all Canadians. Today that tradition continues in our expanded program, resulting in one of the biggest martial arts programs in North America.
				
			</blockquote>
			<p class="quoter">
					- Russ Takashima, JCCC Judo Instructor
				</p>
			<p>
				Over the past four decades, the JCCC has hosted many martial arts tournaments and has proudly produced many winning and skilled competitors. With classes geared to different skill levels and age groups, our sensei’s do more than teach skills and techniques - they educate students in the tradition and discipline that are associated with these sports.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>