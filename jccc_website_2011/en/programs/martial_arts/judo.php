<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Judo</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/judo1.jpg" class="programs" alt="Judo">
			<h1>Judo</h1>
			<p>
				Judo – “the Way of Gentleness” was founded by Dr. Jigoro Kano in 1882 to develop a martial art into a way of life, and a sport, based on overarching principles concerning the development of physical, mental, and moral culture.  The first principle of judo, as articulated by Dr. Kano, is Seiryoku zenyo or “Maximum efficiency in physical and mental activity.”  The JCCC Judo Kai’s goal is to develop its members’ awareness of this principle through physical, cultural and spiritual discipline and development.  The second principle of judo is Jita kyoei or “Mutual welfare and prosperity.”  Judo instruction emphasizes respect for oneself, others, and responsible care for each other in class, and in daily life.
			</p>
			<p>
				The sport of Judo is best known for its spectacular throwing techniques. Judo also involves many specialized grappling techniques including controlled hold-downs, elbow joint-locks and choking submission techniques.  Children, youths or adults can practice and enjoy the sport regardless of age or gender.  The study of Judo can improve balance, flexibility, coordination, aerobic fitness and physical strength, with inherent improvement in self-confidence, concentration, and self-discipline. 
			</p>
			<img src="/assets/images/judo2.jpg" class="programs" alt="Judo">
			<p>
				The JCCC Judo program began over 40 years ago, and has continued under the direction of dedicated instructors who have brought Judo to thousands of students, along the way producing black belts, national medalists, champions and international competitors.  The JCCC Judo Kai program is overseen by Head Instructor Ken Fukushima and Russ Takashima, Club Administrator, both 3rd Dan judoka who began as students at the JCCC in the 1960’s, and who have been volunteer instructors at the JCCC since the 1980’s. Class instruction is provided by numerous highly qualified black belt instructors committed to the dissemination of Kodokan Judo, and who have experience studying and training in Canada, Japan and throughout the world, including Robert Varga, 7th Dan (competitors) and Gerald Okimura, 4th Dan (women’s judo class).
			</p>
			<h3>September to June</h3>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td rowspan="2">Monday, Friday</td>
					<td>7:00-9:00pm</td>
					<td>all ages</td>
					<td>Orange-Black belt</td>
				</tr>
				<tr>
					<td>8:00-10:00pm</td>
					<td>youth, adult</td>
					<td>Competitors</td>
				</tr>
				<tr>
					<td rowspan="3">Wednesday</td>
					<td>7:00-8:30pm</td>
					<td>all ages</td>
					<td>Beginners *</td>
				</tr>
				<tr>
					<td>7:00-9:00</td>
					<td>all ages</td>
					<td>White-Yellow belt</td>
				</tr>
				<tr>
					<td>8:30-10:00pm</td>
					<td>youth, adult</td>
					<td>Competitors</td>
				</tr>
				<tr>
					<td rowspan="2">Saturday</td>
					<td>9:00-10:30am</td>
					<td>all ages</td>
					<td>Beginners *</td>
				</tr>
				<tr>
					<td>9:00-11:00am</td>
					<td>all ages</td>
					<td>White-Yellow belt</td>
				</tr>
				<tr>
					<td>Sunday</td>
					<td>1:30-3:00pm</td>
					<td>Women's Judo or Special club activities **</td>
					<td></td>
				</tr>
			</table>
			<h3>July to August</h3>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td rowspan="2">Monday, Wednesday</td>
					<td>7:00-8:30pm</td>
					<td>all ages</td>
					<td>Beginners *</td>
				</tr>
				<tr>
					<td>7:00-9:00pm</td>
					<td>all ages</td>
					<td>all belts</td>
				</tr>
				<tr>
					<td>Friday</td>
					<td>7:00-9:00pm</td>
					<td>all ages</td>
					<td>all belts</td>
				</tr>
				<tr>
					<td>Sunday</td>
					<td>1:30-3:00pm</td>
					<td>Women's Judo</td>
					<td></td>
				</tr>
			</table>
			<p class="italics">
				* 3-month beginner sessions start on the first Wednesday or Saturday of the following months: January, February, March, April, May, June, July, October, November. <br>
				** Mudansha grading clinics/technical seminars on select Sundays.
			</p>
			<p>
				The JCCC Judo Kai offers a full range of classes and training for recreational and competitive students of all ages throughout the week. For further information, please visit <a href="http://jccc.on.ca/judo" target="_blank">jccc.on.ca/judo</a>.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>