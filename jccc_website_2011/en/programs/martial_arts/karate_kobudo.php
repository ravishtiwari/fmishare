<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Shito-ryu Itosu-Kai Karate and Kobudo</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/karate1.jpg" class="programs" alt="Karate and Kobudo">
			<h1>Shito-ryu Itosu-Kai Karate and Kobudo</h1>
			<p>
				Shito-Ryu Itosu-Kai is one of the main karate styles from Japan. Like all martial arts, Karate is physically and mentally demanding. Beginners are not expected to be in top condition; they are expected only to try their best and to have a positive mental attitude. Great physical and mental development will take place as the student progresses. Students are advised to explore any physical limitations with their physician before beginning.
			</p>
			<p>
				Adults and children (as young as 6) excel at Karate, receiving great physical and emotional benefits. They experience greater self-confidence and achievement at school or work. Members in their sixties have earned black belts and continue active participation into their eighties. Shito-Ryu Itosu-Kai combines the greatest traditions of Okinawan and Japanese Karate, the styles evolving from the life works of such legendary masters as Yasutsune Itosu (1830-1915), Kanryo Higashionna (?-1915), Kenwa Mabuni (1888-1952) and Ryusho Sakagami (1915-1992).
			</p>
			<img src="/assets/images/karate2.jpg" class="programs" alt="Karate">
			<p>
				Shito-Ryu Itosu-Kai Karate and Kobudo has been at the JCCC since 1963 under the instruction of Kei Tsumura Shihan, 8th Dan, chief instructor of Canada. Shito-Ryu Itosu-Kai Karate and Kobudo is an affiliate member of the All Japan Shito-Ryu Itosu-Kai Karate Association. The chief instructor in Japan is Sadaaki
			</p>
			<p>
				Sakagami Soke, 9th Dan. Here at the JCCC, the chief instructor is Kei Tsumura Shihan, 8th Dan. JCCC sensei include Shito-Ryu Itosu-Kai Karate and Kobudo, Mark Uyeda, 6th Dan; Daniel Tsumura, 5th Dan; Helmut Grossmann, 5th Dan; Nada Bendoff, 5th Dan; Marjorie Hunter, 4th Dan; Peter Hanzal, 4th Dan. All students at the JCCC are registered directly with the All Japan Shito-Ryu Itosu-Kai Association in Japan.
			</p>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Tuesday</td>
					<td>7:00;9:00pm</td>
					<td>all ages</td>
					<td>all levels</td>
				</tr>
				<tr>
					<td>Thursday</td>
					<td>6:30-9:00pm</td>
					<td>all ages</td>
					<td>all levels</td>
				</tr>
				<tr>
					<td>Saturday</td>
					<td>9:30-11:00am</td>
					<td>children only</td>
					<td>beginner</td>
				</tr>
			</table>
			<p class="italics">
				* New karate students accepted only at the start of the month.
			</p>
			<p>
				For more information about the Canadian Shito-Ryu Itosu-Kai Honbu please visit <a href="http://www.karatetoronto.com/" target="_blank">karatetoronto.com</a>.
			</p>
			<p>
				The All Japan Shito-Ryu Itosu-Kai Association page is available <a href="http://www.karatedo.co.jp/itosu-kai/english/" target="_blank">here</a>.
			</p>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>