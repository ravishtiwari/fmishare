<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Martial Arts</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/kendo1.jpg" class="programs" alt="Kendo">
			<h1>Kendo</h1>
			<p>
				"The Way of the Sword," Kendo, is the Japanese art of fencing based on the techniques of the two-handed sword of the samurai. The term "Kendo" implies spiritual discipline, as well as fencing technique. Wearing protective equipment and using bamboo training swords, students practice a variety of movements of attack and defence. Most fundamental are stance, footwork, cuts, thrust and parries. The JCCC Kendo Club has become a mecca of Kendo in Canada. Several members have been selected to represent Canada in international competition including the World Kendo Championship held every 3 years. Instructors of the class are held in high esteem, both nationally and internationally.
			</p>
			<img src="/assets/images/kendo2.jpg" class="programs" alt="Kendo">
			<p>
				The JCCC Kendo Club is proud to have Morito Tsumura, Chief Instructor, 8th Dan Kyoshi; Kiyoshi Hao, 7th Dan Kyoshi; Roy Asa, 7th Dan; Sanji Kanno, 7th Dan; Goyo Ohmi, 6th Dan; Tak Yoshida, 5th Dan; Bryan Asa, 6th Dan; Hideki Sumi, 6th Dan; Steve Nakatsu, 5th Dan; and Shane Asa, 5th Dan; as sensei (instructors). Andrew Asa, 4th Dan; Lawrence Tsuji, 4th Dan; Ryo Tamaru, 4th Dan; Serge Antonenok, 3rd Dan; and Dennis Daley, 3rd Dan are assistant instructors. These sensei exemplify the spirit of kendo and have experience in Japan and throughout the world.
			</p>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td rowspan="2">Sunday</td>
					<td>8:45-10:00am *</td>
					<td>8 years and older</td>
					<td>Non-Bogu **</td>
				</tr>
				<tr>
					<td>10:00am-12:00pm</td>
					<td>8 years and older</td>
					<td>Bogu *** (drills, lessons, keiko)</td>
				</tr>
				<tr>
					<td rowspan="2">Monday</td>
					<td>7:00-8:00pm</td>
					<td>Seniors (60+)</td>
					<td>Bogu (keiko)</td>
				</tr>
				<tr>
					<td>8:00-9:00pm</td>
					<td>8 years and older</td>
					<td>Bogu (keiko, sparring)</td>
				</tr>
				<tr>
					<td rowspan="2">Wednesday</td>
					<td>6:45-8:00pm</td>
					<td>8 years and older</td>
					<td>Non-Bogu</td>
				</tr>
				<tr>
					<td>8:00-9:30pm</td>
					<td>8 years and older</td>
					<td>Bogu (drills, lessons, keiko)</td>
				</tr>
				<tr>
					<td>Friday</td>
					<td>7:00-8:30pm</td>
					<td>Juniors (15 years and younger)</td>
					<td>Bogu (drills, lessons, keiko)</td>
				</tr>
			</table>
			<p class="italics">
				* Please arrive 15 minutes early for warm up exercises <br>
				** Non-bogu - without equipment (beginner) <br>
				*** Bogu - with equipment 
			</p>
			<p>
				New Kendo registrants accepted only on the first Sunday or Wednesday in September 2011, January 2012 and March 2012.
			</p>

			<p>
				For more information, please visit <a href="http://www.jccckendo.com/" target="_blank">JCCC Kendo</a>. Site is independently managed by the JCCC Kendo Club.
			</p>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>