<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Japanese Canadian Cultural Centre - Kyudo (Japanese Archery)</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
<style>
</style>
</head>
<body>
<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	

		<div class="right-content">
			<h1>Kyudo (Japanese Archery)</h1>
			<img src="/assets/images/kyudo1.jpg" class="programs-small" alt="Kyudo">
			<p>
				Kyudo literally means “the Way of the Bow” and is the Japanese martial art of archery. Until the 15th century the bow and arrow were the primary weapons of the samurai. Historically there were two main lineages, archery on foot which focused on ceremonial tradition emphasizing ritual and etiquette and mounted archery, the warrior style, setting priority on technique and skill for hitting the target.
			</p>
			<p>
				In modern Kyudo, the bow no more serves as an implement of war, rather it is used as a means for self-development and moral and spiritual growth. Kyudo is often described as “standing meditation”. The student of Kyudo learns to control the breathing and the mind, not becoming oblivious to his surroundings, but rather becoming acutely aware of them. This teaches the practitioner to react calmly and resolutely to his environment. 
			</p>
			<p>
				Regular Kyudo practices will be held once a week. Introductory workshops will be held at intervals; students completing the workshop will be streamed in to the regular program. All necessary basic equipment is provided by the JCCC during the student's first year. Please contact the JCCC for details.
			</p>
			
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Saturday</td>
					<td>11:30am-2:00pm</td>
					<td>16 and over</td>
					<td>General</td>
				</tr>
			</table>
			<p class="italics">
				Students without prior experience must complete an introductory workshop to be accepted in the regular program. Students with experience please inquire with JCCC.
			</p>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>