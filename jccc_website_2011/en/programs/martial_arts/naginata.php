<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>

	<title>Japanese Canadian Cultural Centre - Naginata</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre">
	<meta name="DC.creator" content="FUNDING matters Inc.">
	<meta name="DC.language" scheme="ISO639-2/T" content="eng">
	<meta name="DC.date.created" content="2009-10-01">
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation">

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca">


</head>


<body>

<div class="main-container">
	<?php include "../../header.php";?>
	<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="d" class="programs-small" alt="Naginata">
			<h1>Naginata</h1>
			<p>
				Naginata is a traditional Japanese martial art and modern day sport popular in Japan. It is practiced with a halberd, essentially a short Japanese sword mounted on the end of a 6' oak staff.
			</p>
			<p>
				The Naginata was used by soldiers as a weapon during early Japanese periods (600-1500). At that time, the Naginata because of its length, was the most advantageous weapon being used by soldiers. But in the Oda, Toyotomi periods (1500), use of the Yari (spear) became very popular and widespread. Due to this and the introduction of firearms, the methods of battle, and hence the weapons used, started to change.
			</p>
			<p>
				The Edo period (1603-1867) was a relatively peaceful period in Japanese history. As men trained Kenjutsu (sword techniques), women trained in Naginata as self-defense. Persistently training in Naginata, and adhering to the etiquette of practice was a way of cultivating the character. Thus Naginata became a way for women of Samurai families to study morals, honour and so on. Naginatas were also used as ornaments for the entrances of homes of high ranking Samurai and as an ornament in Daimyos' processions, and has become a decoration customarily used in wedding ceremonies. In Naginata, the various battle technique aspects faded as the training of spirit, body, and mind became the main objective.
			</p>
			<img src="/assets/images/naginata2.jpg" class="programs" alt="Naginata">
			<p>
				At the beginning of the Showa period (1923 - 1989), Naginata was introduced to public schools as part of the curriculum for female students. The various styles of Naginata were combined to form an appropriate "School Naginata" for educational purposes. This "School Naginata" had very strong contest aspects.
			</p>
			<p>
				The All Japan Naginata Federation was formed in 1955. New forms were introduced; kata competitions and matches between those who practice Naginata were also started. In 1990, the International Naginata Federation was formed. In 2003, the
			</p>
			<p>
				Canadian Naginata Federation was formed and admitted to the International Naginata Federation. Nowadays not only women but also men of all ages are practicing Naginata. 
			</p>
			<p>
				Marija Landekic (2nd dan), is a member of the Canadian Naginata Federation and has represented Canada as a member of the National Team at the 4th World Naginata Championships (2007).  She is the current instructor of the JCCC Naginata Club and is assisted by Susan Davis (1st dan) and Marianne Matchuk (1st dan).  A small supply of naginata are available for beginners.
			</p>

			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Sunday</td>
					<td>3:30-5:00pm</td>
					<td>all ages</td>
					<td>General</td>
				</tr>
				<tr>
					<td>Tuesday</td>
					<td>9:30-10:45pm</td>
					<td>all ages</td>
					<td>General</td>
				</tr>
			</table>

			<p class="italics">
				New students are always welcome but we would prefer that you come on the first Sunday or Tuesday of the month.
			</p>
		</div>
	<?php include "../../footer.php";?>
</div>
</body>
</html>