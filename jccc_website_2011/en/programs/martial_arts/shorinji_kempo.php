<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Shorinji Kempo</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/kempo1.jpg" class="programs" alt="Kempo">
			<h1>Shorinji Kempo</h1>
			<p>
				Shorinji Kempo was established in Tadotsu, Japan, by the founder So Doshin (1911-1980) in 1947, as a way of “developing people - for people to learn establishing mutual trust and cooperation.
			</p>
			<p>
				After returning to Japan from China, where he learned various martial arts, So Doshin re-established the techniques he learned and added his own. He started teaching them to local people in response to the moral crisis facing Japanese society.
			</p>
			<p>
				There are no competitors or opponents in Shorinji Kempo. So Doshin says, “share happiness with another”. This is his philosophy. The purpose of practice is to obtain conﬁdence and courage and to learn techniques through cooperation
			</p>
			<img src="/assets/images/kempo2.jpg" class="programs" alt="Kempo">
			<p>
				The Basic Principles of Shorinji Kempo are: Ken Zen Ichinyo (Body and Mind are the Same), Riki Ai Funi (Strength and Love Stands together), Shushu Koju (Defend First, Attack After), Kumite Shutai (Pair Work is Fundamental), Fusatsu Katsujin (Protect People Without Injury). This technique is based on defense and counterattack; therefore rational use of tactics, techniques and strength based on the principles of the discipline allows one to gain large effects from small amounts of force
			</p>
			<p>
				The Shorinji Kempo Federation of Japan, Inc. contributes to Japanese society by popularizing Shorinji Kempo. Kongo Zen Sohonzan Shorinji was founded by So Doshin which teaches developing student fundamentals. There is also Zenrin Gakuen College which helps the society. The World Shorinji Kempo Organization (WSKO) was established to unite and support Shorinji Kempo. Shorinji Kempo has 1.5 million registered members and branches in 32 countries around the world. Various seminars and international conventions are held by WSKO. There are currently 8 branches in Canada, with 4 of the branches located in Ontario.
			</p>
			<table id="programs">
				<tr>
					<th>Day</th>
					<th>Time</th>
					<th>Age</th>
					<th>Level</th>
				</tr>
				<tr>
					<td>Wednesday</td>
					<td>7:00-9:00pm</td>
					<td>7 years and over</td>
					<td>all levels</td>
				</tr>
				<tr>
					<td>Sunday</td>
					<td>5:30-8:00pm</td>
					<td>7 years and over</td>
					<td>all levels</td>
				</tr>
			</table>
			<p>
				This course is led by instructor Ryoichi Oka, 5th Dan and Dai-Kenshi.
			</p>
			<p>
				For more information, click <a href="http://www.skny-ca.com/" target="_blank">here</a> to visit the Shorinji Kempo North York website.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>