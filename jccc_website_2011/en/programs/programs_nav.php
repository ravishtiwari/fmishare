<div class="left-nav">

<?php /*
sub-navigation show/hide functionality using jQuery (jQuery lib, scripts.js) amd PHP
if we're on this page, do this (for each menu item) in order to show active states
*/

$uri = $_SERVER['REQUEST_URI'];	/* setting uri for subcategory drops */
$activeleft = " class=\"active-left\"";

/*
if(stripos($uri,'part-of-uri-searching-for')==false)
if(stripos($haystack,'needle')==false) - looking for a needle in a haystack

if 2nd parameter is not in 1st parameter, echo "more_xx"
if javascript is enabled, will hide the class more_xx because 'hide' is enabled on that class (using jquery and scripts.js)
*/


$self = $_SERVER['PHP_SELF'];	/* grabbing exact uri for subnav active items */

/*
$self = exact URI
if($self == 'specified-uri') { echo $activeleft;}

if the exact URL is the specified URL, echo the class
specified-uri is the link you're editing, obviously, as shown

do not add a space before or after<?php ... ?>tags so that the source code looks neat. instead add the space in the echo, after the first "
e.g.	echo " echoed-text";
this keeps source code neat whether or not the text is echoed.

don't forget to \escape quotes with backslash or you'll break it :)

don't mess up order of ;} ... it is not }; ... i do this all the time, don't do it :)
*/

?>

	<ul>
		<li><a href="/en/programs/"<?php if($self == '/en/programs/index.php') { echo $activeleft;}?>>About our Programs</a>

		<li><a href="#" class="show_cu">Cultural Classes</a>
			<ul class="subnav<?php if(stripos($uri,'cultural')==false) { echo " more_cu";}?>">
				<li><a href="/en/programs/cultural/"<?php if($self == '/en/programs/cultural/index.php') { echo $activeleft;}?>>Cultural Classes at the JCCC</a>
				<li><a href="/en/programs/cultural/japanese.php"<?php if($self == '/en/programs/cultural/japanese.php') { echo $activeleft;}?>>Japanese Language</a>
				<li><a href="/en/programs/cultural/bunka_shishu.php"<?php if($self == '/en/programs/cultural/bunka_shishu.php') { echo $activeleft;}?>>Bunka Shishu</a>
				<li><a href="/en/programs/cultural/shodo.php"<?php if($self == '/en/programs/cultural/shodo.php') { echo $activeleft;}?>>Shodo</a>
				<li><a href="/en/programs/cultural/ikebana.php"<?php if($self == '/en/programs/cultural/ikebana.php') { echo $activeleft;}?>>Ikebana</a>
				<li><a href="/en/programs/cultural/sumi-e.php"<?php if($self == '/en/programs/cultural/sumi-e.php') { echo $activeleft;}?>>Sumi-e</a>
				<li><a href="/en/programs/cultural/e-tegami.php" <?php if($self == '/en/programs/cultural/e-tegami.php') { echo $activeleft;}?>>E-Tegami</a>
				<li><a href="/en/programs/cultural/manju.php"<?php if($self == '/en/programs/cultural/manju.php') { echo $activeleft;}?>>Manju</a>
				<li><a href="/en/programs/cultural/raku.php" <?php if($self == '/en/programs/cultural/raku.php') { echo $activeleft;}?>>Raku</a>
				<li><a href="/en/programs/cultural/washi.php" <?php if($self == '/en/programs/cultural/washi.php') { echo $activeleft;}?>>Washi</a>
			</ul>

		<li><a href="#" class="show_ma">Martial Arts</a>
			<ul class="subnav<?php if(stripos($uri,'martial_arts')==false) { echo " more_ma";}?>">
				<li><a href="/en/programs/martial_arts/"<?php if($self == '/en/programs/martial_arts/index.php') { echo $activeleft;}?>>Martial Arts at the JCCC</a>
				<li><a href="/en/programs/martial_arts/aikido.php"<?php if($self == '/en/programs/martial_arts/aikido.php') { echo $activeleft;}?>>Aikido</a>
				<li><a href="/en/programs/martial_arts/judo.php"<?php if($self == '/en/programs/martial_arts/judo.php') { echo $activeleft;}?>>Judo</a>
				<li><a href="/en/programs/martial_arts/karate_kobudo.php"<?php if($self == '/en/programs/martial_arts/karate_kobudo.php') { echo $activeleft;}?>>Karate & Kobudo</a>
				<li><a href="/en/programs/martial_arts/kendo.php"<?php if($self == '/en/programs/martial_arts/kendo.php') { echo $activeleft;}?>>Kendo</a>
				<li><a href="/en/programs/martial_arts/iaido.php"<?php if($self == '/en/programs/martial_arts/iaido.php') { echo $activeleft;}?>>Iaido</a>
				<li><a href="/en/programs/martial_arts/shorinji_kempo.php"<?php if($self == '/en/programs/martial_arts/shorinji_kempo.php') { echo $activeleft;}?>>Shorinji Kempo</a>
				<li><a href="/en/programs/martial_arts/kyudo.php"<?php if($self == '/en/programs/martial_arts/kyudo.php') { echo $activeleft;}?>>Kyudo</a>
				<li><a href="/en/programs/martial_arts/naginata.php"<?php if($self == '/en/programs/martial_arts/naginata.php') { echo $activeleft;}?>>Naginata</a>
			</ul>

		<li><a href="/en/programs/heritage/"<?php if($self == '/en/programs/heritage/index.php') { echo $activeleft;}?>>Heritage</a>

		<li><a href="/en/programs/discover_japan/"<?php if($self == '/en/programs/discover_japan/index.php') { echo $activeleft;}?>>Discover Japan</a>

		<li><a href="#" class="show_ac">Activities & Workshops</a>
			<ul class="subnav <?php if(stripos($uri,'activities')==false) { echo " more_ac";} ?>">
				<li><a href="/en/programs/activities/"<?php if($self == '/en/programs/activities/index.php') { echo $activeleft;}?>>Activities & Workshops at the JCCC</a>
				<li><a href="/en/programs/activities/cooking.php"<?php if($self == '/en/programs/activities/cooking.php') { echo $activeleft;}?>>Japanese Cooking</a>
				<li><a href="/en/programs/activities/origami.php"<?php if($self == '/en/programs/activities/origami.php') { echo $activeleft;}?>>Origami</a>
				<li><a href="/en/programs/activities/taiko.php"<?php if($self == '/en/programs/activities/taiko.php') { echo $activeleft;}?>>Taiko Drumming</a>
				<li><a href="/en/programs/activities/karaoke.php"<?php if($self == '/en/programs/activities/karaoke.php') { echo $activeleft;}?>>Karaoke</a>
				<li><a href="/en/programs/activities/music.php"<?php if($self == '/en/programs/activities/music.php') { echo $activeleft;}?>>Music</a>
				<li><a href="/en/programs/activities/jcomm_jpalls.php"<?php if($self == '/en/programs/activities/jcomm_jpalls.php') { echo $activeleft;}?>>J-Comm & J-Palls</a>
				<li><a href="/en/programs/activities/jss_collab.php"<?php if($self == '/en/programs/activities/jss_collab.php') { echo $activeleft;}?>>JSS Collaboration</a>
				<li><a href="/en/programs/activities/west_meets_east.php"<?php if($self == '/en/programs/activities/west_meets_east.php') { echo $activeleft;}?>>West Meets East</a>
				<li><a href="/en/programs/activities/ayame_kai.php"<?php if($self == '/en/programs/activities/ayame_kai.php') { echo $activeleft;}?>>Ayame Kai</a>
				<li><a href="/en/programs/activities/sakura_kai.php"<?php if($self == '/en/programs/activities/sakura_kai.php') { echo $activeleft;}?>>Sakura Kai</a>
				<li><a href="/en/programs/activities/himawari.php"<?php if($self == '/en/programs/activities/himawari.php') { echo $activeleft;}?>>Himawari Buyo-Kai</a>
				<li><a href="/en/programs/activities/token_kai.php"<?php if($self == '/en/programs/activities/token_kai.php') { echo $activeleft;}?>>Token Kai</a>
				<li><a href="/en/programs/activities/jcinema.php"<?php if($self == '/en/programs/activities/jcinema.php') { echo $activeleft;}?>>J-Cinema Screenings</a>
			</ul>
		
		<li><a href="#" class="show_rc">Recreation</a>
			<ul class="subnav <?php if(stripos($uri,'recreation')==false) { echo " more_rc";} ?>">
				<li><a href="/en/programs/recreation/"<?php if($self == '/en/programs/recreation/index.php') { echo $activeleft;}?>>Recreation at the JCCC</a>
				<li><a href="/en/programs/recreation/yoga.php"<?php if($self == '/en/programs/recreation/yoga.php') { echo $activeleft;}?>>Yoga</a>
				<li><a href="/en/programs/recreation/taichi.php"<?php if($self == '/en/programs/recreation/taichi.php') { echo $activeleft;}?>>Tai chi</a>
				<li><a href="/en/programs/recreation/ohana_hula.php"<?php if($self == '/en/programs/recreation/ohana_hula.php') { echo $activeleft;}?>>Ohana Hula</a>
				<li><a href="/en/programs/recreation/hula_for_health.php"<?php if($self == '/en/programs/recreation/hula_for_health.php') { echo $activeleft;}?>>Hula for Health</a>
				<li><a href="/en/programs/recreation/bridge.php"<?php if($self == '/en/programs/recreation/bridge.php') { echo $activeleft;}?>>Bridge</a>
				<li><a href="/en/programs/recreation/ping_pong.php"<?php if($self == '/en/programs/recreation/ping_pong.php') { echo $activeleft;}?>>Ping Pong</a>
				<li><a href="/en/programs/recreation/wynford.php"<?php if($self == '/en/programs/recreation/wynford.php') { echo $activeleft;}?>>Wynford Seniors' Club</a>
			</ul>

		<li><a href="/en/programs/kids/"<?php if($self == '/en/programs/kids/index.php') { echo $activeleft;}?>>Kids & Families</a>

		<li><a href="/en/programs/school/"<?php if($self == '/en/programs/school/index.php') { echo $activeleft;}?>>School Groups</a>

		<li><a href="/en/programs/fees/"<?php if($self == '/en/programs/fees/index.php') { echo $activeleft;}?>>Pay Program Fees</a>
	</ul>
</div>
