<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Recreation</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Recreation">
			<h1>Recreation</h1>
			<p>
				In recent years, the JCCC has also added yoga, hula and tai chi to its repertoire of recreational programming. All disciplines teach strength, balance, flexibility, endurance, and harmony. 
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>