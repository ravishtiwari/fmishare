<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css">
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css">
<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="/assets/scripts/jquery.js" type="text/javascript"></script>
	<script src="/assets/scripts/scripts.js" type="text/javascript"></script>
	<title>Japanese Canadian Cultural Centre - Ohana Hula</title>
	<link rel="icon" type="image/ico" href="/assets/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<?php include "meta_stuff.php" ;?>
</head>
<body>
	<div class="main-container">
		<?php include "../../header.php";?>
		<?php include "../programs_nav.php";?>	
		<div class="right-content">
			<img src="/assets/images/?.jpg" class="programs" alt="Ohana Hula">
			<h1>Ohana Hula</h1>
			<p>
				The JCCC Ohana Hula, in the spirit of the Centre’s motto, "Friendship through Culture," aims to build friendships through participation in Hula dancing and performances, as well as to promote good physical and spiritual health.
			</p>
			<p>
				For inquiries, please contact Yuki Hipsh through <a href="mailto:yukih@jccc.on.ca">email</a> or call (416) 441-2345 ext 235.
			</p>
		</div>
		<?php include "../../footer.php";?>
	</div>
</body>
</html>