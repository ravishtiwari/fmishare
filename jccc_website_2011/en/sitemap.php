<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 
<head>

	<link id="cssdefault" href="/assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/assets/css/jccc-ie6.css" />
<![endif]-->
	<script src="/assets/scripts/jquery.js"></script>
	<script src="/assets/scripts/scripts.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<title>Japanese Canadian Cultural Centre - Site Map</title>

	<link rel="icon" href="/../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre" />
	<meta name="DC.creator" content="FUNDING matters Inc." />
	<meta name="DC.language" scheme="ISO639-2/T" content="eng" />
	<meta name="DC.date.created" content="2009-10-01" />
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation" />

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca" />
</head>


<body>

<div class="main-container">

	<?php include "header.php";?>

	<div class="main-content-section">
		
		<div class="main-content">

			<h1>Site Map</h1>
			
			<ul id="sitemap">
				<li><a href="/en/">Home</a></li>
				
				<li><a href="/en/about/">About
					<ul>
						<li>Who We are</a></li>
						<li><a href="/en/about/mission.php">Mission</a></li>
						<li><a href="/en/about/board.php">Board of Directors</a></li>
						<li><a href="/en/about/kobayashi_hall.php">Kobayashi Hall</a></li>
					</ul>
				</li>
				
				<li><a href="/en/membership/">Membership
					<ul>
						<li>Memberships at the JCCC</a></li>
						<li><a href="/en/membership/benefits.php">Benefits of Becoming a Member</a></li>
						<li><a href="/en/membership/payfees.php">Become a Member / Renew your Membership</a></li>
					</ul>
				</li>
				
				<li><a href="/en/volunteer">Volunteer</a></li>
				
				<li><a href="/en/programs">Programs
					<ul>
						<li>About our Programs</a></li>
						
						<li><a href="/en/programs/cultural/">Cultural Classes
							<ul>
								<li>Cultural Classes at the JCCC</a></li>
								<li><a href="/en/programs/japanese.php">Japanese Language</a></li>
								<li><a href="/en/programs/bunka_shishu.php">Bunka Shishu</a></li>
								<li><a href="/en/programs/shodo.php">Shodo</a></li>
								<li><a href="/en/programs/ikebana.php">Ikebana</a></li>
								<li><a href="/en/programs/sumi-e.php">Sumi-e</a></li>
								<li><a href="/en/programs/e-tegami.php">E-tegami</a></li>
								<li><a href="/en/programs/manju.php">Manju</a></li>
								<li><a href="/en/programs/raku.php">Raku</a></li>
								<li><a href="/en/programs/washi.php">Washi</a></li>
							</ul>
						</li>
						
						<li><a href="/en/programs/martial_arts/">Martial Arts
							<ul>
								<li>Martial Arts at the JCCC</a></li>
								<li><a href="/en/programs/martial_arts/aikido.php">Aikido</a></li>
								<li><a href="/en/programs/martial_arts/judo.php">Judo</a></li>
								<li><a href="/en/programs/martial_arts/karate_kobudo.php">Karate & Kobudo</a></li>
								<li><a href="/en/programs/martial_arts/kendo.php">Kendo</a></li>
								<li><a href="/en/programs/martial_arts/iaido.php">Iaido</a></li>
								<li><a href="/en/programs/martial_arts/shorinji_kempo.php">Shorinji Kempo</a></li>
								<li><a href="/en/programs/martial_arts/kyudo.php">Kyudo</a></li>
								<li><a href="/en/programs/martial_arts/naginata.php">Naginata</a></li>
							</ul>
						</li>
						
						<li><a href="/en/programs/heritage/">Heritage</a></li>
						
						<li><a href="/en/programs/discover_japan/">Discover Japan</a></li>
						
						<li><a href="/en/programs/activities/">Activities & Workshops
							<ul>
								<li>Activities & Workshops at the JCCC</a></li>
								<li><a href="/en/programs/activities/cooking.php">Japanese Cooking</a></li>
								<li><a href="/en/programs/activities/origami.php">Origami</a></li>
								<li><a href="/en/programs/activities/taiko.php">Taiko</a></li>
								<li><a href="/en/programs/activities/karaoke.php">Karaoke</a></li>
								<li><a href="/en/programs/activities/music.php">Music</a></li>
								<li><a href="/en/programs/activities/jcomm_jpalls.php">J-Comm & J-Palls</a></li>
								<li><a href="/en/programs/activities/jss_collab.php">JSS Collaboration</a></li>
								<li><a href="/en/programs/activities/west_meets_east.php">West Meets East</a></li>
								<li><a href="/en/programs/activities/ayame_kai.php">Ayame Kai</a></li>
								<li><a href="/en/programs/activities/sakura_kai.php">Sakura Kai</a></li>
								<li><a href="/en/programs/activities/himawari.php">Himawari Buyo-Kai</a></li>
								<li><a href="/en/programs/activities/token_kai.php">Token Kai</a></li>
								<li><a href="/en/programs/activities/jcinema.php">J-Cinema</a></li>
							</ul>
						</li>
						
						<li><a href="/en/programs/recreation/">Recreation</a></li>
						
						<li><a href="/en/programs/kids/">Kids & Families</a></li>
						
						<li><a href="/en/programs/school/">School Groups</a></li>
						
						<li><a href="/en/programs/fees/">Pay Program Fees</a></li>
					
					</ul>
					
					<li><a href="/en/events/">Events
						<ul>
							<li>About our Events</a></li>
							<li><a href="/news_events/">Upcoming Events</a></li>
						</ul>
					</li>
					
					<li><a href="/en/media/">Media
						<ul>
							<li>Multimedia</a></li>
							<li><a href="/videos/">Videos</a></li>
							<li><a href="/audio/">Audio</a></li>
							<li><a href="/en/media/virtualtour.php">Virtual Tour</a></li>
							<li><a href="/en/media/gallery.php">Image Gallery</a></li>
							<li><a href="/en/media/newsletter.php">Newsletters</a></li>
							<li><a href="/en/media/cookbook.php">Cookbook</a></li>
							<li><a href="/en/media/interactive/kimono_app.php">Kimono App</a></li>
						</ul>
					</li>
					
					<li><a href="/en/donate/">Donate
						<ul>
							<li>Investing in our Future</a></li>
							<li><a href="/en/donate/needs.php">Statement of Needs</a></li>
							<li><a href="/en/donate/make_donation.php">Make a Donation</a></li>
						</ul>
					</li>
				
			</ul>
		
		</div>
		
		<?php include "footer.php";?>

	</div>

</div>

</body>

</html>