<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 
<head>

	<link id="cssdefault" href="assets/css/jccc-default.css" media="screen, print" rel="stylesheet" type="text/css" />
	<!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/qa/jccc/assets/css/jccc-ie6.css" />
<![endif]-->
	<script src="/qa/jccc/assets/scripts/jquery.js"></script>
	<script src="/qa/jccc/assets/scripts/scripts.js"></script>
	<link rel="icon" href="../assets/images/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<title>Japanese Canadian Cultural Centre - Homepage</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
 
	<meta name="DC.title" content="Japanese Canadian Cultural Centre" />
	<meta name="DC.creator" content="FUNDING matters Inc." />
	<meta name="DC.language" scheme="ISO639-2/T" content="eng" />
	<meta name="DC.date.created" content="2009-10-01" />
	<meta name="DC.subject" scheme="gccore" content="japanese culture, kyudo, aikido, laido, judo, karate, kendo, japanese language, japanese canadian, culture, shodo, Ikebana, naginata, classes, language, gallery, history, generation" />

	<meta name="DC.Identifier" scheme="URI" content="http://www.jccc.on.ca" />

</head>

<body>

<div class="splash-header">
<img class="logo" src="/qa/jccc/assets/images/JCCC_logo_whitebg.png">
</div>

<div class="main-container, center">
	<a href="en/">
		<img src="assets/images/splash_img.png" width="798"/><br/>
	</a>
</div>

<div class="main-container">
	<div class="splashboxes">
		<div class="col1" onClick="location.href='en/';" title="http://jccc.on.ca/en/">
			<h1 class="splash redback">JCCC</h1> 
			<p class="pad5">
			<a class="plain" href="en">Japanese Canadian Cultural Centre</a>
			</p>
		</div>
	</div>

	<div class="col2" onClick="location.href='http://sedai.ca/';" title="http://sedai.ca/">
		<h1 class="splash blueback">Sedai</h1>
		<p class="pad5">
		<a class="plain" href="http://sedai.ca" target="_blank">Sedai: The Japanese Canadian Legacy Project</a>
		</p>
	</div>

	<div class="col3" onClick="location.href='#';" title="#">
		<h1 class="splash yellowback">JCCC Foundation</h1>
		<p class="pad5">
		<a class="plain" href="#">The Japanese Canadian Cultural Centre Foundation.</a>
		</p>
	</div>
	
</div>

<?php include "en/footer.php";?>

</body>

</html>