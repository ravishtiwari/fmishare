/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    textClass:[ "l0_out", "l0_over" ],
    size:[ 41, 200 ],
    itemoff:[ 0, "+previousItem" ],
    leveloff:[ "+parentItem-5px", 4 ],
    itemFilters:[ "progid:DXImageTransform.Microsoft.Fade(duration=0.5)", "progid:DXImageTransform.Microsoft.Fade(duration=0.5)" ]
};

var STYLE_2 =
{
    /*4{*/leveloff:[ 0, '+parentItem-1px' ]/*4}*/
};

var STYLE_1 = {
    levelFilters:[ "progid:DXImageTransform.Microsoft.Fade(duration=0.5) progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=reverse,duration=0.5)", "progid:DXImageTransform.Microsoft.Fade(duration=0.5) progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.5)" ],
    itemFilters:null,
    size:[ 41, 145 ],
    backgroundClass:[ "l1_bg_other", "l1_bg_other" ],
    borderWidth:[ 1, 0, 1, 0 ],
    color:
    {
        border:"#F2EEEE"
    },
    textClass:[ "l1_out", "l1_over" ],
    size:[ 19, 142 ],
    itemoff:[ "+previousItem", 0 ],
    ifN0:{
        valign:"bottom",
        size:[ 24, 142 ],
        backgroundClass:[ "l1_bg_0", "l1_bg_0" ]
    },
    ifN1:{
        backgroundClass:[ "l1_bg_1", "l1_bg_1" ]
    },
    ifLast:{
        valign:"top",
        size:[ 30, 142 ],
        borders:[ 1, 0, 1, 1 ]
    }
};

var MENU_ITEMS = [
    {style:[ STYLE_0, STYLE_1, STYLE_2 ], blankImage:"images/b.gif" },
    {code:"Home", url:"index.htm",
        sub:[
            {}
        ]
    },
    {code:"Spaces", url:"spaces/spaces.htm",
		sub:[
            {},
            {code:"First Floor Spaces",
                sub:
                [
                    {},
                    {code:"Kobayashi Hall", url:"spaces/kobayashi_hall.htm"},
                    {code:"Kobayashi Hall Patio", url:"spaces/kobayashi_hall_patio.htm"},
                    {code:"Shokokai Court", url:"spaces/shokokai_court.htm"},
                    {code:"Heritage Court", url:"spaces/heritage_court.htm"},
                    {code:"Heritage Lounge", url:"spaces/heritage_lounge.htm"},
					{code:"Hosaki Room", url:"spaces/hosaki_room.htm"},
					{code:"Parking", url:"spaces/parking.htm"}
                ]
            },
            {code:"Second Floor Spaces",
                sub:
                [
                    {},
                    {code:"Hamasaki Room", url:"spaces/hamasaki_room.htm"},
                    {code:"Board Room", url:"spaces/board_room.htm"},
                    {code:"Multi Room 2A, 2B", url:"spaces/multi_room_2a_2b.htm"},
                    {code:"Multi Room 2B", url:"spaces/multi_room_2b.htm"},
                    {code:"Multi Room 2C", url:"spaces/multi_room_2c.htm"},
                    {code:"Multi Room 2D", url:"spaces/multi_room_2d.htm"}
                ]
            }			
        ]
    },
    {code:"Suppliers", url:"suppliers/suppliers.htm",
				sub:[
            {},
			{code:"Catering", url:"suppliers/catering.htm"},
			{code:"Rentals", url:"suppliers/rental_companies.htm"},
			{code:"Decor", url:"suppliers/decor.htm",
                sub:
                [
                    {},
                    {code:"Florals", url:"suppliers/florals.htm"}
                ]
            },
			{code:"Entertainment", url:"suppliers/entertainment.htm"},
			{code:"Hotels", url:"suppliers/hotels.htm"},
			{code:"Transportation", url:"suppliers/transportation.htm"},
            
			
        ]
    },
    {code:"Contact", url:"contact/contact.htm",
        sub:[
            {},
            {code:"Location", url:"contact/location.htm"},
            {code:"Event Inquiry Form", url:"contact/event_inquiry_form.htm"}
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
