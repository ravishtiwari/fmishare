function showNamingSection(section)
{
    var Pic = new Array();
    Pic[0] = 'images/firstfloor/plan_floor1.jpg';
    Pic[1] = 'images/firstfloor/plan_khall.jpg';
    Pic[2] = 'images/firstfloor/plan_kgarden.jpg';
    Pic[3] = 'images/firstfloor/plan_martial_court.jpg';
    Pic[4] = 'images/firstfloor/plan_martial1.jpg';
    Pic[5] = 'images/firstfloor/plan_martial2.jpg';
    Pic[6] = 'images/firstfloor/plan_culinary.jpg';
    Pic[7] = 'images/firstfloor/plan_wynford.jpg';
    Pic[8] = 'images/firstfloor/plan_shokokai.jpg';
    Pic[9] = 'images/firstfloor/plan_motodori.jpg';
    Pic[10] = 'images/firstfloor/plan_reception.jpg';
    Pic[11] = 'images/firstfloor/plan_hosaki.jpg';
    Pic[12] = 'images/firstfloor/plan_lobby.jpg';
    Pic[13] = 'images/firstfloor/plan_egarden.jpg';
    Pic[14] = 'images/firstfloor/plan_ikeda.jpg';
    Pic[15] = 'images/firstfloor/plan_heritagewing.jpg';
    Pic[16] = 'images/firstfloor/plan_elevator.jpg';
    Pic[17] = 'images/firstfloor/plan_gallery.jpg';
    Pic[18] = 'images/firstfloor/plan_cgarden.jpg';
    Pic[19] = 'images/firstfloor/plan_display1.jpg';
    Pic[20] = 'images/firstfloor/plan_screen.jpg';
    Pic[21] = 'images/firstfloor/firstfloorplan.jpg';
    Pic[22] = 'images/secondfloor/plan_floor2.jpg';
    Pic[23] = 'images/secondfloor/plan_multia.jpg';
    Pic[24] = 'images/secondfloor/plan_multib.jpg';
    Pic[25] = 'images/secondfloor/plan_multic.jpg';
    Pic[26] = 'images/secondfloor/plan_multid.jpg';
    Pic[27] = 'images/secondfloor/plan_hamasaki.jpg';
    Pic[28] = 'images/secondfloor/plan_community1.jpg';
    Pic[29] = 'images/secondfloor/plan_community2.jpg';
    Pic[30] = 'images/secondfloor/plan_resource.jpg';
    Pic[31] = 'images/secondfloor/plan_boardroom.jpg';
    Pic[32] = 'images/secondfloor/plan_administration.jpg';
    Pic[33] = 'images/secondfloor/plan_display3.jpg';
    Pic[34] = 'images/secondfloor/secondfloorplan.jpg';


    
    if (section == "firstfloor"){
        document.getElementById("picSection").innerHTML='<img src="'+Pic[0]+'" id="firstfloor" class="theatrepics" />';
    }else if (section == "khall"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("khall")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/khall.jpg" title="Kobayashi Hall"><img src="'+Pic[1]+'" id="khall" class="theatrepics" /><a/>';
    }else if (section == "kgarden"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("kgarden")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/kgarden.jpg" title="Kobayashi Garden"><img src="'+Pic[2]+'" id="kgarden" class="theatrepics" /></a>';
    }else if (section == "martial_court"){ 
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("martial_court")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/martial_court.jpg" title="Martial Arts "><img src="'+Pic[3]+'" id="martial_court" class="theatrepics" /><a/>';
    }else if (section == "martial1"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("martial1")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/martial1.jpg" title="Martial Arts Court 1"><img src="'+Pic[4]+'" id="martial1" class="theatrepics" /><a/>';
    }else if (section == "martial2"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("martial2")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/martial2.jpg" title="Martial Arts Court 2"><img src="'+Pic[5]+'" id="martial2" class="theatrepics" /></a>';
    }else if (section == "culinary"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("culinary")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/culinary.jpg" title="Culinary Arts Centre"><img src="'+Pic[6]+'" id="culinary" class="theatrepics" /></a>'; 
    }else if (section == "wynford"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("wynford")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/wynford.jpg" title="Wynford Room"><img src="'+Pic[7]+'" id="wynford" class="wynford" /></a>';
    }else if (section == "shokokai"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("shokokai")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/shokokai.jpg" title="Shokokai Court"><img src="'+Pic[8]+'" id="shokokai" class="theatrepics" /></a>';
    }else if (section == "motodori"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("motodori")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/motodori.jpg" title="Moto Dori"></a><img src="'+Pic[9]+'" id="motodori" class="theatrepics" /></a>';
    }else if (section == "reception"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("reception")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/reception.jpg" title="Gift Shop and Reception"><img src="'+Pic[10]+'" id="reception" class="theatrepics" /></a>';
    }else if (section == "hosaki"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("hosaki")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/hosaki.jpg" title="Hosaki Room"><img src="'+Pic[11]+'" id="hosaki" class="theatrepics" /></a>';
    }else if (section == "lobby"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("lobby")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/lobby.jpg" title="Lobby"><img src="'+Pic[12]+'" id="lobby" class="theatrepics" /></a>';
    }else if (section == "egarden"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("egarden")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/egarden.jpg" title="Entrance Garden"><img src="'+Pic[13]+'" id="egarden" class="theatrepics" /></a>';
    }else if (section == "ikeda"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("ikeda")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/ikeda.jpg" title="Ikeda Tower"><img src="'+Pic[14]+'" id="ikeda" class="theatrepics" /></a>';
    }else if (section == "heritagewing"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("heritagewing")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/heritagewing.jpg" title=""><img src="'+Pic[15]+'" id="heritagewing" class="theatrepics" /></a>'; 
    }else if (section == "elevator"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("elevator")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/elevator.jpg" title="Elevator and Stairway"><img src="'+Pic[16]+'" id="elevator" class="theatrepics" /></a>';
    }else if (section == "gallery"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("gallery")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/gallery.jpg" title="JCCC Gallery"><img src="'+Pic[17]+'" id="gallery" class="theatrepics" /></a>';
    }else if (section == "cgarden"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("cgarden")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/cgarden.jpg" title="Courtyard Garden"><img src="'+Pic[18]+'" id="cgarden" class="theatrepics" /></a>';
    }else if (section == "display1"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("display1")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/display1.jpg" title="Permanent Display"><img src="'+Pic[19]+'" id="display1" class="theatrepics" /></a>';
    }else if (section == "screen"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("screen")&#39; rel="lightbox[firstfloor]" href="images/firstfloor/screen.jpg" title="Wood Screen"><img src="'+Pic[20]+'" id="screen" class="theatrepics" /></a>';
    }else if (section == "secondfloor"){
        document.getElementById("picSection").innerHTML='<img src="'+Pic[22]+'" id="theatrepic1" class="theatrepics" />';
    }else if (section == "multia"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("multia")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/multia.jpg" title="Multipurpose Room 2A"><img src="'+Pic[23]+'" id="multia" class="theatrepics" /></a>';
    }else if (section == "multib"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("multib")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/multib.jpg" title="Multipurpose Room 2B"><img src="'+Pic[24]+'" id="multib" class="theatrepics" /></a>';
    }else if (section == "multic"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("multic")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/multic.jpg" title="Multipurpose Room 2C"><img src="'+Pic[25]+'" id="multic" class="theatrepics" /></a>';
    }else if (section == "multid"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("multid")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/multid.jpg" title="Multipurpose Room 2D"><img src="'+Pic[26]+'" id="multid" class="theatrepics" /></a>';
    }else if (section == "hamasaki"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("hamasaki")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/hamasaki.jpg" title="Hamasaki Room"><img src="'+Pic[27]+'" id="hamasaki" class="theatrepics" /></a>';
    }else if (section == "community1"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("community1")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/secondfloorplan.jpg" title="Community Hallway 1"><img src="'+Pic[28]+'" id="community1" class="theatrepics" /></a>';
    }else if (section == "community2"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("community2")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/community2.jpg" title="Community Hallway 2"><img src="'+Pic[29]+'" id="community2" class="theatrepics" /></a>';
    }else if (section == "resource"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("resource")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/resource.jpg" title="Nikkei Heritage Resource Centre"><img src="'+Pic[30]+'" id="resource" class="theatrepics" /></a>';
    }else if (section == "boardroom"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("boardroom")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/boardroom.jpg" title="Board Room"><img src="'+Pic[31]+'" id="boardroom" class="theatrepics" /></a>';
    }else if (section == "administration"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("administration")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/secondfloorplan.jpg" title="Administration Centre"><img src="'+Pic[32]+'" id="administration" class="theatrepics" /></a>';
    }else if (section == "display3"){
        document.getElementById("picSection").innerHTML='<a class="naming" onMouseOver=&#39;showNamingSection("display3")&#39; rel="lightbox[secondfloor]" href="images/secondfloor/display3.jpg" title="Permanent Display"><img src="'+Pic[33]+'" id="display3" class="theatrepics" /></a>';
    }
}

function showImages()
{
    document.getElementById("picSection").innerHTML=
    '<a href="images/partscentre/artscentre1.jpg" title="" ><img src="images/partscentre/artscentre1 - thumb.jpg" id="theatrepic1" class="theatrepics" /></a><a href="images/partscentre/artscentre2.jpg" title=""><img src="images/partscentre/artscentre2 - thumb.jpg" id="theatrepic2" class="theatrepics" /></a>	<a href="images/partscentre/artscentre3.jpg" title=""><img src="images/partscentre/artscentre3 - thumb.jpg" id="theatrepic3" class="theatrepics" /></a>';
}

