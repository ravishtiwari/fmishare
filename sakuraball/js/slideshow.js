var slideShowSpeed = 5000;
var sponsorsSpeed = 10000;
var slideShow2Speed = 7000;
var slideShow3Speed = 13000;
var crossFadeDuration = 5;

var Pic = new Array();
var Pic2 = new Array();
var Pic3 = new Array();
var Sponsor = new Array();

Pic[0] = '../ishareImages/homepage/homepage_home.jpg';
Pic[1] = '../ishareImages/homepage/homepage_support.jpg';
Pic[2] = '../ishareImages/homepage/homepage_campaigns.jpg';
Pic[3] = '../ishareImages/homepage/homepage_foundation.jpg';
Pic[4] = '../ishareImages/homepage/homepage_sakura.jpg';
Pic[5] = '../ishareImages/homepage/homepage_newsletter.jpg';


var t;
var j = 0;

var p = Pic.length;
var preLoad = new Array();

for (i = 0; i < p; i++){
   preLoad[i] = new Image();
   preLoad[i].src = Pic[i];
}

function runSlideShow(){
   if (document.all){
     document.images.SlideShow.style.filter="blendTrans(duration=2)";
     document.images.SlideShow.style.filter="blendTrans(duration=crossFadeDuration)";
     document.images.SlideShow.filters.blendTrans.Apply();  
   }
   document.images.SlideShow.src = preLoad[j].src;
   if (document.all){
     document.images.SlideShow.filters.blendTrans.Play();
   }
   j = j + 1;
   if (j > (p-1)) j=0;
   t = setTimeout('runSlideShow()', slideShowSpeed);
}