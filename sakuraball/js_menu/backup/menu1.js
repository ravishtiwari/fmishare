/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem+1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem+1px', 0 ],
};

var color = {border:"#666666", bgON:"white",bgOVER:"#B6BDD2"};
var css = {ON:"clsCMOn", OVER:"clsCMOver"};

var SUB_STYLE = 
{
borderWidth:1, 
shadow:2, 
color:color, 
css:css, 
itemoff:[22,0], 
};

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0, STYLE_1 ], blankImage:'js_menu/images/b.gif'},
    {code:"", size:[ 22, 54 ],
        sub:[
		{style:SUB_STYLE, size:[24,175]},
		{code:"Mission&nbsp;Statement", url:"home/mission_statement.htm"},
		{code:"About&nbsp;SMCS", url:"home/about_smcs.htm"},
		{code:"Campaign&nbsp;Leadership", url:"home/campaign_leadership.htm"},
		{code:"Strategic&nbsp;Plan&nbsp;2006-2011", url:"home/strategic_objectives.htm"},
		{code:"St.&nbsp;Michael&#39;s&nbsp;College&nbsp;School", url:"http://www.stmichaelscollegeschool.com/hm"}
        ]
    },
    {code:"", size:[22,66],
        sub:[
		{style:SUB_STYLE, size:[24,130]},
		{code:"Investment&nbsp;Options"},
		{code:"Giftabulator"},
		{code:"Make&nbsp;a&nbsp;Donation"},
        ]
    },
    {code:"", size:[22,60],
        sub:[
		{style:SUB_STYLE, size:[24,150]},
		{code:"Performing&nbsp;Arts&nbsp;Centre"},
		{code:"Building&nbsp;Team"},
		{code:"Naming&nbsp;Opportunities"},
		{code:"Timeline"},
        ]
    },
        {code:"", size:[22,108],
        sub:[
		{style:SUB_STYLE, size:[24,178]},
		{code:"Learning&nbsp;Centre&nbsp;Introduction"},
		{code:"Learning&nbsp;Centre&nbsp;Initiative"},
        ]
    },
	{code:"", size:[22,72],
		sub:[
			{style:SUB_STYLE, size:[24,133]},
			{code:"Bursaries&nbsp;Information"},
		]
	},
	{code:"", size:[22,43],
		sub:[
			{style:SUB_STYLE, size:[24,90]},
			{code:"Videos"},
			{code:"Photo&nbsp;Gallery"},
		]
	},
		{code:"", size:[22,43],
		sub:[
			{style:SUB_STYLE, size:[24,65]},
			{code:"Register"},
			{code:"Forum"},
		]
	}

];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
