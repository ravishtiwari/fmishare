/* COOLjsMenu style + structure file */
/*For html files in ishare_project folder*/

var STYLE_0 =
{

    backgroundClass:[ "mainOn", "mainOver" ],
    size:[ 23, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem', 0 ]
};



var SUB_STYLE =
{
	borderWidth:1,
    borderColor:"#711212",
    backgroundColor:[ "white", "#961a19" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ]
    /*leveloff:[ '+parentItem+1px', 0 ]*/
}

var SUB_STYLE2 =
{
	borderWidth:1,
    borderColor:"#711212",
    backgroundColor:[ "white", "#961a19" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]

}

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0], blankImage:'http://www.fundingmatters.com/jko/jccccampaign/js_menu/images/b.gif'},
    {code:"Home", size:[ 28, 70 ], url:"../home/index.htm",
        sub:[
		{style:SUB_STYLE, size:[24,150]},
		{code:"Legacy Campaign", url:"../home/legacy.htm", sub:[]}, 
		{code:"About&nbsp;The JCCC", url:"../home/about.htm", sub:[]},
		{code:"Campaign&nbsp;Leadership", url:"../home/campaign_leadership.htm", sub:[]},		
		{code:"JCCC Website", target: "_blank", url:"http://jccc.on.ca/", sub:[]}
        ]
    },
    {code:"Support JCCC", size:[28,125], url:"../support/index.htm",
        sub:[
		{style:SUB_STYLE, size:[24,215]},		
		{code:"Planned and Endowed Gifts", url:"", 
            sub:[
                {style:SUB_STYLE2, size:[24,180]},
                {code:"Charitable Bequest", url:"", sub:[]},
                {code:"Stocks and Mutual Funds", url:"", sub:[]},
                {code:"Life Insurance Gift", url:"", sub:[]},
                {code:"Gift-Plus Annuity", url:"", sub:[]},
                {code:"Charitable Remainder Trust", url:"", sub:[]},
                {code:"Residual Interest &amp; Real Estate", url:"", sub:[]},
            ]},
		{code:"Memorial and Honourary Gifts", url:"", 
            sub:[
                {style:SUB_STYLE2, size:[24,140]},
                {code:"In Memory Of", url:"", sub:[]},
                {code:"In Honour Of", url:"", sub:[]},
            ]},
      {code:"Corporate Sponsorship", url:"", 
            sub:[
                {style:SUB_STYLE2, size:[24,140]},
                {code:"Cultural Programs", url:"", sub:[]},
                {code:"Special Events", url:"", sub:[]},
            ]},
      {code:"Naming Opportunities", url:"http://www.fundingmatters.com/jko/jccccampaign/expansions/firstfloor.htm", 
           sub:[
            {style:SUB_STYLE2, size:[24,140]},				
            {code:"First&nbsp;Floor", url:"http://www.fundingmatters.com/jko/jccccampaign/expansions/firstfloor.htm", sub:[]},
            {code:"Second&nbsp;Floor", url:"http://www.fundingmatters.com/jko/jccccampaign/expansions/secondfloor.htm", sub:[]},		
            {code:"Other", url:"http://www.fundingmatters.com/jko/jccccampaign/expansions/secondfloor.htm", sub:[]},	
              ]
          },
        ]
    },
    {code:"Campaigns", size:[28,105], url:"../campaigns/index.htm", 
        sub:[
		{style:SUB_STYLE, size:[24,145]},		
        {code:"Cranes", url:"../campaigns/cranes.htm", 
            sub:[
                {style:SUB_STYLE2, size:[24,140]},
                {code:"Installation", url:"../campaigns/cranes.htm", sub:[]},
                {code:"Dedications", url:"../campaigns/dedications.htm", sub:[]},
            ]},
		{code:"Mamoru Nishi Fund", url:"../campaigns/mamoru.htm", sub:[]},
		{code:"Just Add Shoyu", url:"../campaigns/shoyu.htm", sub:[]},
      {code:"Operation Tatami", url:"../campaigns/tatami.htm", sub:[]},
        ]
        
    },
    {code:"Foundation", size:[28,105], url:"../foundation/index.htm",
        
    },
	{code:"Sakura Ball", size:[28,103], url:"../sakura/index.htm",
         sub:[
            {style:SUB_STYLE, size:[24,155]},		
               {code:"Sakura Ball Committee", url:"../sakura/sakura_committee.htm", sub:[]},
               {code:"Sakura Award", url:"../sakura/sakura_award.htm", 
                  sub:[
                      {style:SUB_STYLE2, size:[24,160]},
                      {code:"About the Award", url:"../sakura/sakura_award.htm", sub:[]},
                      {code:"This Year's Honouree", url:"../sakura/honouree.htm", sub:[]},
                      {code:"Past Recipients", url:"../sakura/recipients.htm", sub:[]},
                  ]},
               {code:"Event Details", url:"../sakura/event_details.htm", 
                  sub:[
                      {style:SUB_STYLE2, size:[24,160]},
                      {code:"The Facility", url:"../sakura/event_details.htm", sub:[]},
                      {code:"Date and Time", url:"../sakura/date.htm", sub:[]},
                      {code:"Food", url:"../sakura/food.htm", sub:[]},
                      {code:"Entertainment", url:"../sakura/entertainment.htm", sub:[]},
                      {code:"Hotel and Travel", url:"../sakura/travel.htm", sub:[]},
                  ]},
               {code:"Our Sponsors", url:"../sakura/sponsors.htm", sub:[]},
               {code:"Tickets and Sponsorship", url:"../sakura/tickets.htm", 
                  sub:[
                      {style:SUB_STYLE2, size:[24,160]},
                      {code:"Individual", url:"../sakura/tickets.htm", sub:[]},
                      {code:"Presenting Sponsor", url:"../sakura/sponsorship_presenting.htm", sub:[]},
                      {code:"Reception Sponsor", url:"../sakura/sponsorship_reception.htm", sub:[]},
                      {code:"Entertaining Sponsor", url:"../sakura/sponsorship_entertaining.htm", sub:[]},
                      {code:"Participating Sponsor", url:"../sakura/sponsorship_participating.htm", sub:[]},
                      {code:"Gala Patron", url:"../sakura/sponsorship_patron.htm", sub:[]},
                      {code:"Gift in Kind", url:"../sakura/sponsorship_gift.htm", sub:[]},
                  ]},
               {code:"RSVP", url:"../sakura/rsvp.htm", sub:[]},
         ]  
	},    
    {code:"Newsletter", size:[28,99], url:"../newsletter/index.htm",
		sub:[
			{style:SUB_STYLE, size:[24,125]},
            {code:"Current Issue", url:"", sub:[]},
            {code:"Meet Our Donors", url:"", sub:[]},
            {code:"Archives", url:"", sub:[]},
            {code:"Subscribe", url:"", sub:[]},
		]
	},
    {code:"Contact Us", size:[28,100], url:"../contact/index.htm",
		
	}
    	

];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
