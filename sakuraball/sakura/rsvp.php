<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Sakura Ball - RSVP </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link rel="stylesheet" type="text/css" href="../css/default.css" />

<script type="text/javascript" >
   function validateForm() {
      var theForm = document.forms[0];

      var item=document.getElementById("sponsor_name")
      if(item.value=="") {
         alert("Please fill in the sponsor name.");    
         return false;
      }
      
      item=document.getElementById("sponsor_phone")
      if(item.value=="") {
         alert("Please fill in the sponsor phone #.");    
         return false;
      }
      
      item=document.getElementById("sponsor_email")
      if(item.value=="") {
         alert("Please fill in the sponsor email.");    
         return false;
      }
      
      for(i=0; i<theForm.elements.length; i++){
      var alertText = ""

         if((theForm.elements[i].type == "text") && (theForm.elements[i].value =="") ){
            return confirm("The form is incomplete, do you wish to submit anyway?")          
         }

      
      }
      
      return true;
   }
</script>

<link rel="stylesheet" type="text/css" href="../js_menu/style.css" />
<link rel="stylesheet" type="text/css" href="../css/print_subpages.css" media="print" />
</head>

<body>

<div class="navigation">
  <!--Header-->
  <table class="main" cellspacing="0" cellpadding="0">

    <tr>
      <td class="mainheader">      
         <a id="header-jccc-logo" href="http://www.jccc.on.ca/"><img src="../ishareImages/jccclogo.jpg" alt="JCCC"></a>
      </td>
    </tr>
    <tr>
      <td class="subnavigation" colspan="3">    
		<a href="index.htm"><b>Home</b></a>
        <a href="sakura_committee.htm"><b>Sakura Commitee</b></a>
        <a href="sakura_award.htm"><b>Sakura Award </b></a>
        <a href="event_details.htm"><b>Event Details </b></a>
        <a href="sponsors.htm"><b>Sponsors </b></a>
        <a href="tickets.htm"><b>Tickets &amp; Sponsorship</b></a>
        <a href="contact.php"><b>Contact</b></a>
      </td>
    </tr>
  </table>
  
  <!--Main Section-->
  <table class="subpages-main" cellspacing="0" cellpadding="0" align="center">
    <tr>
    
    <!--Left Menu-->
    <td class="left-menu-section">
     </td> 
    
    <!--Content Section-->
      <td class="subpages-main"> 
          <table  cellspacing="0" cellpadding="0">
            <tr>
               <td class="mainPanel-top"></td>
            </tr>
            <tr>
               <td class="mainPanel-mid">
        <img class="heading" src="images/headings/rsvp.jpg" alt="RSVP" />
        
<p>Please use the below form to register to receive information about the 2011 Sakura Ball.</p>        
        <div>
        
<?php      
   if(!$_REQUEST["submitted"]){
?>  
 
            <form method="post" onsubmit="return validateForm();">  
               Sponsor Name:<br />
               <input type="text" name="sponsor_name" id="sponsor_name"/>
               <br />
               Contact Phone #:<br />
               <input type="text" name="sponsor_phone"id="sponsor_phone" /> 
               <br />               
               email:<br />
               <input type="text" name="sponsor_email" id="sponsor_email" />
               <br />                
               
			   <?php
               for($i=1;$i<=10;$i++) {
               
               echo "
                  <div class='guestform'>  
                  <h4 class='guestheading'> Guest $i </h4>
				  Honorific (Mr./Mrs./Ms.):<br />
                  <input type='text' name='guest$i"."_prefix' id='guest$i"."_prefix'/>
                  <br />
                  First name:<br />
                  <input type='text' name='guest$i"."_firstname' id='guest$i"."_firstname'/>
                  <br />
                  Last name:<br />
                  <input type='text' name='guest$i"."_lastname' id='guest$i"."_lastname' /> 
                  <br />
                  Allergies/Dietary Restrictions:<br />
                  <input type='text' name='guest$i"."_restrictions' id='guest$i"."_restrictions' />
                  <br />
                  
               </div>";
               
             }?>
			   
               <br/>
               <input type="hidden" name="submitted" value="true" />
               <input type="submit" value="Submit" />
            </form>
            
            <p>For further inquiries, please contact James Heron at 416-441-2345 ex 224, <a href="mailto:jamesh@jccc.on.ca"><u>jamesh@jccc.on.ca</u></a>.</p>            
<?php 
   }
   else {  //we are now submitting a form
      $isSecure=true;
      
      if ( ereg( "[\r\n]", $_REQUEST[sponsor_name] ) || ereg( "[\r\n]", $_REQUEST[sponsor_phone] ) || ereg( "[\r\n]", $_REQUEST[sponsor_email] )) {
            $isSecure=false;
      }
      $content=
"
Sponsor name: $_REQUEST[sponsor_name]
Sponsor phone: $_REQUEST[sponsor_phone]
Sponsor email: $_REQUEST[sponsor_email]

";

      for($i=1;$i<=10;$i++){
         $firstname="guest$i"."_firstname";
         $lastname="guest$i"."_lastname";
         $prefix="guest$i"."_prefix";
         $restrictions="guest$i"."_restrictions";
         
         if(ereg( "[\r\n]", $_REQUEST[$firstname] ) || ereg( "[\r\n]", $_REQUEST[$lastname] ) || ereg( "[\r\n]", $_REQUEST[$prefix] ) || ereg( "[\r\n]", $_REQUEST[$restrictions] )) {
            $isSecure=false;
            break;
         }
            
         $content.=
"
GUEST $i
   Prefix: $_REQUEST[$prefix]
   First name: $_REQUEST[$firstname]
   Last name: $_REQUEST[$lastname]
   Restrictions: $_REQUEST[$restrictions]

";   
      }
      //echo $content;
      if($isSecure) 
         $successful=mail("jko@fundingmatters.com", "Sakura Ball Submission", $content) && mail("lashley@fundingmatters.com", "Sakura Ball Submission", $content);
         
      if($isSecure && $successful) {
?>
   <p> Thank you for your submission, an email has been sent to the administrator. </p>
<?php
      }
      else
         echo "<p> An error has occurred, please try again later. </p>";
   }
?>   
                  </div>
               </td>
            </tr>
            <tr>
               <td class="mainPanel-bottom"></td>
            </tr>
         </table>
        
        </td>
      
    </tr>
            <tr>
	<td class="page-footer" colspan="2">2009 FUNDINGmatters Inc., All rights reserved</td>
    </tr>
  </table>

</div>
<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_project=6568070; 
var sc_invisible=1; 
var sc_security="9d6ce9bb"; 
</script>

<script type="text/javascript"
src="http://www.statcounter.com/counter/counter.js"></script><noscript><div
class="statcounter"><a title="joomla site stats"
href="http://statcounter.com/joomla/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/6568070/0/9d6ce9bb/1/"
alt="joomla site stats" ></a></div></noscript>
<!-- End of StatCounter Code -->
</body>
</html>
