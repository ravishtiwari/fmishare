var STYLE1 = {"border":1, "shadow":1, "color":{"border":"silver", "shadow":"#333333", "bgON":"#F0F0F0","bgOVER":"navy"}, "css":{"ON":"clsCMOn", "OVER":"clsCMOver"}};
var STYLE_TOP = {"border":0, "shadow":0, "color":{"border":"gray", "shadow":"black", "bgON":"silver","bgOVER":"navy"}, "css":{"ON":"", "OVER":""}};

var MENU_ITEMS =
[
	{"hidden_top":1, "pos":[0,10], "size":[0,0],"itemoff":[0,100],"leveloff":[0,0],"delay":500,"style":STYLE_TOP},
	{code:"",
		sub:[
			{"size":[20,200], "itemoff":[19,0], "leveloff":[0,0], "style":STYLE1},
			{code:'CoolDev.Com', url:"http://www.cooldev.com"},
			{code:'JavaScript.CoolDev.Com', url:"http://javascript.cooldev.com"}
			]
	},
	{code:"",
		sub:[
			{"size":[20,110], "itemoff":[19,0], "leveloff":[0,0], "style":STYLE1},
			{code:'Free',
				sub:[
					{"leveloff":[10,99]},
					{code:'COOLjsTree'},
					{code:'COOLjsMenu'},
					{code:'CoolRollover'},
					{code:'CoolScroller'},
					{code:'CoolValidator'},
					]
			},
			{code:'Professional',
				sub:[
					{"size":[20,140], "leveloff":[10,99]},
					{code:'COOLjsTree PRO'},
					{code:'COOLjsMenu PRO'},
					]
			}
			]
	},
	{code:'',
		sub:[
			{"size":[20,110], "itemoff":[19,0], "leveloff":[0,0], "style":STYLE1},
			{code:'Download'},
			{code:'Pricing'},
			{code:'Ordering'}
			]
	},
	{code:'',
		sub:[
			{"size":[20,110], "itemoff":[19,0], "leveloff":[0,0], "style":STYLE1},
			{code:'Online help'},
			{code:'FAQ'},
			{code:'Visual builders',
				sub:[
					{"size":[20,160], "leveloff":[0,109], "style":STYLE1},
					{code:'COOLjsTree builder'},
					{code:'COOLjsMenu builder'}
					]
			}
			]
	}

];
