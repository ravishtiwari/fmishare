/* COOLjsMenu style + structure file */

BLANK_IMAGE = "images/b.gif";

var STYLE = {
    borderWidth:1,
    borderColor:"#666666",
    shadow:1,
    shadowColor:"#DBD8D1",
    backgroundColorOut:"white",
    backgroundColorOver:"#B6BDD2",
    textClassOver:"clsCMOver",
    textClassOut:"clsCMOn"
};

var MENU_ITEMS = [
    {pos:"relative", delay:[0, 800], itemoff:[0,"+previousItem-1px"], leveloff:["+parentItem-1px",0], style:STYLE, size:[23,120], wrapoff:["+item-1px","level"], frames:[ "topFrame", "mainFrame" ], blankImage:'../images/b.gif'},
    {code:"Item 1",
        sub:[
            {itemoff:["+previousItem-1px",0]},
            {code:"SubItem 1",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
        ]
    },
    {code:"Item 2",
        sub:[
            {itemoff:["+previousItem-1px",0]},
            {code:"SubItem 1",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
        ]
    },
    {code:"Item 3",
        sub:[
            {itemoff:["+previousItem-1px",0]},
            {code:"SubItem 1",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
        ]
    },
    {code:"Item 4",
        sub:[
            {itemoff:["+previousItem-1px",0], delay:[1000,1000,-1]},
            {code:"SubItem 1",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
        ]
    },
    {code:"Item 5",
        sub:[
            {itemoff:["+previousItem-1px",0]},
            {code:"SubItem 1",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {leveloff:[0,"+parentItem-1px"]},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"},
                    {code:"SubItem 4"},
                    {code:"SubItem 5"},
                ]
            },
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
