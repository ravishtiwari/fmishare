/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    backgroundStyle:[ "background: white left bottom repeat-x url(images/bg.gif);", "background: white left bottom repeat-x url(images/bg.gif);" ],
    textStyle:[ "color: #7B7A7A; font: 8pt Verdana, sans-serif; text-align: right; padding: 0 25px;", "color: #1F02B5; font: 8pt Verdana, sans-serif; text-align: right; padding: 0 25px;" ],
    size:[ 23, "+maxItem" ],
    itemoff:[ "+previousItem", 0 ],
    leveloff:[ 0, "+parentItem+1px" ]
};

var STYLE_1 =
{
    textClass:[ "l1_out", "l1_over" ],
    backgroundClass:"l1_bg_other",
    itemoff:[ "+previousItem", 0 ]
};

var MENU_ITEMS = [
    {style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif" },
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 2", url:"#",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
