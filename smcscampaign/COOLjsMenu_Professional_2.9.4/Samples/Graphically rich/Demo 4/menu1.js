/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    textStyle:[ "background: left bottom repeat-x url(images/l0_bg_out.gif); color: #101010; font: bold 12pt/29px Arial, sans-serif; text-align: center;", "background: left bottom repeat-x url(images/l0_bg_over.gif); color: #FF6000; font: bold 12pt/29px Arial, sans-serif; text-align: center;" ],
    size:[ 29, 159 ],
    itemoff:[ 0, "+previousItem-21px" ],
    leveloff:[ "+parentItem", 22 ],
    itemFilters:[ "progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=reverse,duration=0.2)", "progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.2)" ]
};

var STYLE_1 =
{
    borderWidth:1,
    borderColor:"#99B8E6",
    backgroundColor:[ "#CDEBFD", "white" ],
    textStyle:[ "color: #1E71E9; font: bold 9pt Arial, sans-serif; text-align: center;", "color: #1E71E9; font: bold 9pt Arial, sans-serif; text-align: center;" ],
    size:[ 24, 137 ],
    itemoff:[ "+previousItem-1px", 0 ],
    itemFilters:"progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=reverse,duration=0.2)",
    ifFirst:
    {
        size:[ 23, 137 ],
        borderWidth:[ 1, 0, 1, 1 ]
    }
};

var MENU_ITEMS = [
    {style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif" },
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 2", url:"#",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
