/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    textStyle:[ "background: right top url(images/l0_text_bg_out.gif); color: #101010; font: bold 11pt/36px Arial, sans-serif; text-align: right; padding-right: 31px;", "background: right top url(images/l0_text_bg_over.gif); color: #FF6000; font: bold 11pt/36px Arial, sans-serif; text-align: right; padding-right: 16px;" ],
    size:[ 36, 179 ],
    imgsize:[ 36, 44 ],
    itemoff:[ "+previousItem+1px", 0 ],
    leveloff:[ -1, "+parentItem+2px" ]
};

var STYLE_1 =
{
    itemFilters:'progid:DXImageTransform.Microsoft.Fade(duration=0.2)',
    backgroundStyle:[ "background: url(images/l1_bg_middle.gif);", "background: url(images/l1_bg_middle.gif);" ],
    textStyle:[ "color: #2B8EBF; font: 8pt/20px Tahoma, sans-serif; text-align: center;", "color: #DF0000; font: 8pt/20px Tahoma, sans-serif; text-align: center;" ],
    size:[ 20, 106 ],
    itemoff:[ "+previousItem", 0 ],
    image:null,
    ifFirst:
    {
        size:[ 30, 106 ],
        valign:"bottom",
        backgroundStyle:[ "background: url(images/l1_bg_top.gif);", "background: url(images/l1_bg_top.gif);" ]
    },
    ifLast:
    {
        size:[ 30, 106 ],
        valign:"top",
        backgroundStyle:[ "background: url(images/l1_bg_bottom.gif);", "background: url(images/l1_bg_bottom.gif);" ]
    }
};

var MENU_ITEMS = [
    {style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif" },
    {code:"Item 1", image:[ "images/ico1_out.gif", "images/ico1_over.gif" ],
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 2", url:"#", image:[ "images/ico2_out.gif", "images/ico2_over.gif" ],
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 3", image:[ "images/ico3_out.gif", "images/ico3_over.gif" ],
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 4", image:[ "images/ico4_out.gif", "images/ico4_over.gif" ],
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
