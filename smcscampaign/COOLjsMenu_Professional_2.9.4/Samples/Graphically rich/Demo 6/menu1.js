/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    backgroundStyle:[ "background: url(images/l0_bg_middle_out.gif);", "background: url(images/l0_bg_middle_over.gif);" ],
    textStyle:[ "color: #2C5E94; font: bold 8pt/21px Verdana, sans-serif; text-align: right; padding-right: 8px;", "color: #2C5E94; font: bold 8pt/21px Verdana, sans-serif; text-align: right; padding-right: 8px;" ],
    size:[ 21, 139 ],
    ifFirst:
    {
        backgroundStyle:[ "background: url(images/l0_bg_top_out.gif);", "background: url(images/l0_bg_top_over.gif);" ],
        size:[ 25, 139 ],
        leveloff:[ 3, "+parentItem-1px" ],
        valign:"bottom"
    },
    ifLast:
    {
        backgroundStyle:[ "background: url(images/l0_bg_bottom_out.gif);", "background: url(images/l0_bg_bottom_over.gif);" ],
        size:[ 24, 139 ],
        valign:"top"
    },
    itemoff:[ "+previousItem", 0 ],
    leveloff:[ -1, "+parentItem-1px" ],
    itemFilters:[ "progid:DXImageTransform.Microsoft.Fade(duration=0.3)", "progid:DXImageTransform.Microsoft.Fade(duration=0.3)" ]
};

var STYLE_1 =
{
    backgroundStyle:[ "background: url(images/l1_bg_middle_out.gif);", "background: url(images/l1_bg_middle_over.gif);" ],
    textStyle:[ "color: #2C5E94; font: bold 8pt/21px Verdana, sans-serif; text-align: right; padding-right: 21px;", "color: #2C5E94; font: bold 8pt/21px Verdana, sans-serif; text-align: right; padding-right: 21px;" ],
    size:[ 21, 126 ],
    ifFirst:
    {
        backgroundStyle:[ "background: url(images/l1_bg_top_out.gif);", "background: url(images/l1_bg_top_over.gif);" ],
        size:[ 22, 126 ],
        valign:"bottom"
    },
    ifLast:
    {
        backgroundStyle:[ "background: url(images/l1_bg_bottom_out.gif);", "background: url(images/l1_bg_bottom_over.gif);" ],
        valign:"top"
    }
};

var MENU_ITEMS = [
    {style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif" },
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 2",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 5",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 6",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 7",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
