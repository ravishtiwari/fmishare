/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    itemFilters:"progid:DXImageTransform.Microsoft.Fade(duration=0.3)",
    valign:"top",
    size:[ 73, 81 ],
    backgroundStyle:[ "background: white url(images/l0_bg_out.gif);", "background: #F5F5F5 url(images/l0_bg_over.gif);" ],
    textStyle:[ "color: #808080; font: bold 8pt Verdana, sans-serif; padding-left: 11px;", "color: #D00303; font: bold 8pt Verdana, sans-serif; padding-left: 11px;" ],
    itemoff:[ 0, "+previousItem" ],
    leveloff:[ "+parentItem-9px", 3 ]
};

var STYLE_1 =
{
    levelFilters:[ "progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=reverse,duration=0.3)", "progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.3)"],
    itemFilters:null,
    borderWidth:[ 1, 0, 1, 0 ],
    borderColor:"#D8D7D7",
    backgroundStyle:"background-color: #E9E9E9;",
    textStyle:[ "padding-bottom: 1px; text-align: center; color: #41648A; font: 8pt Verdana, sans-serif; background: center bottom no-repeat url(images/l1_line_out.gif);", "padding-bottom: 1px; text-align: center; color: #DB1616; font: 8pt Verdana, sans-serif; background: center bottom no-repeat url(images/l1_line_over.gif);" ],
    size:[ 19, 75 ],
    itemoff:[ "+previousItem", 0 ],
    ifFirst:
    {
        backgroundStyle:"background: #E9E9E9 left top no-repeat url(images/l1_bg_first.gif);",
        textStyle:[ "padding-top:10px; padding-bottom: 1px; text-align: center; color: #41648A; font: 8pt Verdana, sans-serif; background: center bottom no-repeat url(images/l1_line_out.gif);", "padding-top:10px; padding-bottom: 1px; text-align: center; color: #DB1616; font: 8pt Verdana, sans-serif; background: center bottom no-repeat url(images/l1_line_over.gif);" ],
        size:[ 29, 75 ]
    },
    ifLast:
    {
        borderWidth:[ 1, 0, 1, 1 ],
        size:[ 23, 75 ]
    }
};

var MENU_ITEMS = [
    {exclusive:true, style:[ STYLE_0, STYLE_1 ], delay:[ 200, 800 ], blankImage:"images/b.gif" },
    {code:"<img src=\"images/ico0.gif\" width=\"61\" height=\"48\" style=\"vertical-align: bottom;\" /><br />Item 1",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"<img src=\"images/ico1.gif\" width=\"61\" height=\"48\" style=\"vertical-align: bottom;\" /><br />Item 2",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"<img src=\"images/ico2.gif\" width=\"61\" height=\"48\" style=\"vertical-align: bottom;\" /><br />Item 3",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"<img src=\"images/ico3.gif\" width=\"61\" height=\"48\" style=\"vertical-align: bottom;\" /><br />Item 4",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"<img src=\"images/ico4.gif\" width=\"61\" height=\"48\" style=\"vertical-align: bottom;\" /><br />Item 5",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
