/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem-1px', 0 ]
};

var STYLE_2 =
{
    leveloff:[ 0, '+parentItem-1px' ]
};

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0, STYLE_1, STYLE_2 ], blankImage:'images/b.gif'},
    {code:"Transparency",
        sub:[
            {/*1{*/levelFilters:'progid:DXImageTransform.Microsoft.Alpha(opacity=40)'/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Shadow",
        sub:[
            {/*2{*/levelFilters:'progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#aaaaaa,strength=3)'/*2}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Waves",
        sub:[
            {/*3{*/levelFilters:'progid:DXImageTransform.Microsoft.Wave(freq=1,LightStrength=10,Phase=-17,Strength=5)'/*3}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Combined",
        sub:[
            {/*4{*/levelFilters:'progid:DXImageTransform.Microsoft.Alpha(opacity=60) progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#CCCCCC,strength=5) progid:DXImageTransform.Microsoft.Wave(freq=1,LightStrength=10,Phase=-17,Strength=5)'/*4}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
