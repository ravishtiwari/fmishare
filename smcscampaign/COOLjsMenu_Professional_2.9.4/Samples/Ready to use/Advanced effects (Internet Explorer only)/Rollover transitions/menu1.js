/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem-1px', 0 ]
};

var STYLE_2 =
{
    leveloff:[ 0, '+parentItem-1px' ]
};

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0, STYLE_1, STYLE_2 ], delay:[ 350, 800 ], blankImage:'images/b.gif'},
    {code:"Fade",
        sub:[
            {/*1{*/itemFilters:[ 'progid:DXImageTransform.Microsoft.Fade(duration=0.3)', 'progid:DXImageTransform.Microsoft.Fade(duration=0.3)' ]/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 6",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            }
        ]
    },
    {code:"Dissolve",
        sub:[
            {/*2{*/itemFilters:[ 'progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.15)', 'progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.15)' ]/*2}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 6",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            }
        ]
    },
    {code:"Pixelate",
        sub:[
            {/*3{*/itemFilters:'progid:DXImageTransform.Microsoft.Pixelate(duration=1.3)'/*3}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 6",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            }
        ]
    },
    {code:"H-Wipe",
        sub:[
            {/*4{*/itemFilters:[ 'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.5)', 'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.5)' ]/*4}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 6",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            }
        ]
    },
    {code:"V-Wipe",
        sub:[
            {/*5{*/itemFilters:[ 'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.5)', 'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.5)' ]/*5}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 4",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 5",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            },
            {code:"SubItem 6",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"},
                    {code:"SubSubItem 6"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
