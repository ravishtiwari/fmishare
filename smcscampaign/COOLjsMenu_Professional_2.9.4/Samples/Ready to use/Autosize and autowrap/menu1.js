/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    /*1{*/size:[ '+maxItem', '+self' ]/*1}*/,
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem-1px', 0 ]
};

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0, STYLE_1 ], blankImage:'images/b.gif'},
    {code:"Own size",
        sub:[
            {/*2{*/size:[ "+self", "+self" ]/*2}*/},
            {code:"Short"},
            {code:"Very, very long"},
            {code:"Two<br />lines of text"},
            {code:"Three<br />lines<br />of text"}
        ]
    },
    {code:"Own width<br />and tallest item's height",
        sub:[
            {/*3{*/size:[ "+maxItem", "+self" ]/*3}*/},
            {code:"Short"},
            {code:"Very, very long"},
            {code:"Two<br />lines of text"},
            {code:"Three<br />lines<br />of text"}
        ]
    },
    {code:"Own height<br />and widest item's width",
        sub:[
            {/*4{*/size:[ "+self", "+maxItem" ]/*4}*/},
            {code:"Short"},
            {code:"Very, very long"},
            {code:"Two<br />lines of text"},
            {code:"Three<br />lines<br />of text"}
        ]
    },
    {code:"Largest item's size",
        sub:[
            {/*5{*/size:[ "+maxItem", "+maxItem" ]/*5}*/},
            {code:"Short"},
            {code:"Very, very long"},
            {code:"Two<br />lines of text"},
            {code:"Three<br />lines<br />of text"}
        ]
    },
    {code:"Fixed width<br />with autowrap",
        sub:[
            {/*6{*/size:[ "+self", 220 ]/*6}*/},
            {code:"Short text"},
            {code:"Longer text"},
            {code:"Several words that will not fit item's width for sure"},
            {code:"Nemo enim ipsam voluptatem Quia voluptas sit aspernatur aut odit aut fugit sed, Quia consequuntur magni dolores. Nisi Ut, aliquid ex Ea commodi consequatur quis. Iste natus error Sit voluptatem accusantium doloremque laudantium totam.", textStyle:"text-align: justify; font: 8pt Tahoma, sans-serif;"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
