/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    /*1{*/itemoff:[ '+previousItem-1px', 0 ]/*1}*/,
    /*2{*/leveloff:[ 0, '+parentItem-1px' ]/*2}*/
};

var MENU_ITEMS = [
    {pos:'relative', /*3{*/style:STYLE_0/*3}*/, blankImage:'images/b.gif'},
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 2",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 3",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 4",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 5",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            }
        ]
    },
    {code:"Item 2",
        sub:[
            {},
            {code:"SubItem 1",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 2",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 3",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 4",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 5",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            }
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 2",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 3",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 4",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 5",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            }
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 1",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 2",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 3",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 4",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 5",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            }
        ]
    },
    {code:"Item 5",
        sub:[
            {},
            {code:"SubItem 1",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 2",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 3",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 4",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            },
            {code:"SubItem 5",
                sub:
                [
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"},
                    {code:"SubSubItem 4"},
                    {code:"SubSubItem 5"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
