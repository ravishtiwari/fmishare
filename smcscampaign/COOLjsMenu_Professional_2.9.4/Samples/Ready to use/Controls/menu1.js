/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    shadow:2,
    shadowColor:"#DBD8D1",
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var STYLE_1 =
{
    size:[ 22, 170 ]
};

var MENU_ITEMS =
[
    {pos:'relative', style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif"},
    {code:"Text box",
        sub:
        [
            {},
            {code:"Here we have a text box."},
            {code:'<form action="http://www.google.com/search">Google: <input class="text" type="text" name="q" /></form>', /*1{*/hasControls:true/*1}*/, size:[ 36, 170 ]}
        ]
    },
    {code:"Memo",
        sub:
        [
            {},
            {code:"Here we have a memo box."},
            {code:'Some memo:<br /><textarea></textarea>', /*2{*/hasControls:true/*2}*/, format:{size:[ 85, 170 ]}}
        ]
    },
    {code:"Combo",
        sub:
        [
            {},
            {code:"Here we have a combo box."},
            {code:'Some combo:<br /><select><option>Option 1</option><option>Option 2</option><option>Option 3</option></select>', /*3{*/hasControls:true/*3}*/, format:{size:[ 43, 170 ]}}
        ]
    },
    {code:"Checkbox",
        sub:
        [
            {},
            {code:"Here we have a combo box."},
            {code:'<input class="checkbox" type="checkbox" id="value1" /><label for="value1">Value 1</label><br /><input class="checkbox" type="checkbox" id="value2" /><label for="value2">Value 2</label><br /><input class="checkbox" type="checkbox" id="value3" /><label for="value3">Value 3</label>', /*4{*/hasControls:true/*4}*/, size:[ 70, 170 ]}
        ]
    },
    {code:"IFRAME",
        sub:
        [
            {itemoff:[ 0, '+previousItem-1px' ]},
            {code:'<iframe frameborder="0" style="width: 100%; height: 100%;" src="form.html"></iframe>', /*5{*/hasControls:true/*5}*/, size:[ 180, 200 ]},
            {code:'<iframe frameborder="0" style="width: 100%; height: 100%;" src="http://www.google.com/"></iframe>', /*6{*/hasControls:true/*6}*/, size:[ 180, 550 ]}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
