/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem-1px', 0 ]
};

var MENU_ITEMS = [
    {/*1{*/pos:'relative'/*1}*/, style:[ STYLE_0, STYLE_1 ], blankImage:'images/b.gif'},
    {code:"Item 1",
        sub:[
            {itemoff:[21,0]},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"},
            {code:"SubItem 6"}
        ]
    },
    {code:"Item 2",
        sub:[
            {itemoff:[21,0]},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"},
            {code:"SubItem 6"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
