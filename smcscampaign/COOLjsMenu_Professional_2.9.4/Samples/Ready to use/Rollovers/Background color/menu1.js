/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    /*1{*/backgroundColor:[ "white", "#B6BDD2" ]/*1}*/,
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var RED_STYLE =
{
	/*1{*/backgroundColor:[ "#FFE0E0", "#FF0000" ]/*1}*/
};

var GREEN_STYLE =
{
	/*1{*/backgroundColor:[ "#E0FFE0", "#008000" ]/*1}*/
};

var BLUE_STYLE =
{
	/*1{*/backgroundColor:[ "#E0E0FF", "#0000FF" ]/*1}*/
};

var MENU_ITEMS = [
	{pos:"relative", style:STYLE_0, blankImage:'images/b.gif'},
	{code:"Red items",
		sub:[
			{/*1{*/style:RED_STYLE/*1}*/},
			{code:"SubItem 1",
				sub:[
					{},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"SubItem 2",
				sub:[
					{},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"SubItem 3",
				sub:[
					{},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			}
		]
	},
	{code:"Green items",
		sub:[
			{/*1{*/style:GREEN_STYLE/*1}*/},
			{code:"Red items",
				sub:[
					{/*1{*/style:RED_STYLE/*1}*/},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"Green items",
				sub:[
					{/*1{*/style:GREEN_STYLE/*1}*/},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"Blue items",
				sub:[
					{/*1{*/style:BLUE_STYLE/*1}*/},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			}
		]
	},
	{code:"Mixed items",
		sub:[
			{},
			{code:"SubItem 1", /*1{*/style:RED_STYLE,/*1}*/
				sub:[
					{},
					{code:"SubSubItem 1", /*1{*/style:BLUE_STYLE/*1}*/},
					{code:"SubSubItem 2", /*1{*/style:GREEN_STYLE/*1}*/},
					{code:"SubSubItem 3", /*1{*/style:RED_STYLE/*1}*/}
				]
			},
			{code:"SubItem 2", /*1{*/style:GREEN_STYLE,/*1}*/
				sub:[
					{},
					{code:"SubSubItem 1", /*1{*/style:RED_STYLE/*1}*/},
					{code:"SubSubItem 2", /*1{*/style:BLUE_STYLE/*1}*/},
					{code:"SubSubItem 3", /*1{*/style:GREEN_STYLE/*1}*/}
				]
			},
			{code:"SubItem 3", /*1{*/style:BLUE_STYLE,/*1}*/
				sub:[
					{},
					{code:"SubSubItem 1", /*1{*/style:RED_STYLE/*1}*/},
					{code:"SubSubItem 2", /*1{*/style:GREEN_STYLE/*1}*/},
					{code:"SubSubItem 3", /*1{*/style:BLUE_STYLE/*1}*/}
				]
			}
		]
	}
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
