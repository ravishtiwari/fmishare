/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    /*1{*/backgroundClass:[ "clsCMOn", "clsCMOver" ],/*1}*/
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var BOLD_STYLE =
{
    /*1{*/backgroundClass:[ "clsCMOn", "clsCMOverBold" ]/*1}*/
};

var ITALIC_STYLE =
{
    /*1{*/backgroundClass:[ "clsCMOn", "clsCMOverItalic" ]/*1}*/
};

var UNDERLINE_STYLE =
{
    /*1{*/backgroundClass:[ "clsCMOn", "clsCMOverUnderline" ]/*1}*/
};

var COLORED_STYLE =
{
    /*1{*/backgroundClass:[ "clsCMOn", "clsCMOverColored" ]/*1}*/
};

var MENU_ITEMS = [
    {pos:"relative", style:STYLE_0, blankImage:'images/b.gif'},
    {code:"Bold",
        sub:[
            {/*1{*/style:BOLD_STYLE/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Italic",
        sub:[
            {/*1{*/style:ITALIC_STYLE/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Underline",
        sub:[
            {/*1{*/style:UNDERLINE_STYLE/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Colored",
        sub:[
            {/*1{*/style:COLORED_STYLE/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
