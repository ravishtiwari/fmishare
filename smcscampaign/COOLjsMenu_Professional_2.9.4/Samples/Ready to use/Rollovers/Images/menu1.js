/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    /*1{*/itemoff:[ "+previousItem", 0 ],
    leveloff:[ 0, "+parentItem" ]/*1}*/
};

var MENU_ITEMS = [
    {pos:"relative", style:STYLE_0, blankImage:'images/b.gif'},
    {/*2{*/code:'<img src="images/item1.gif" width="111" height="24" />',
        ocode:'<img src="images/item1o.gif" width="111" height="24" />',
        size:[ 24, 111 ]/*2}*/,
        sub:[
            {},
            {code:'<img src="images/subitem1.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem1o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            },
            {code:'<img src="images/subitem2.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem2o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            },
            {code:'<img src="images/subitem3.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem3o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            }
        ]
    },
    {code:'<img src="images/item2.gif" width="111" height="24" />',
        ocode:'<img src="images/item2o.gif" width="111" height="24" />',
        size:[ 24, 111 ],
        sub:[
            {},
            {code:'<img src="images/subitem1.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem1o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            },
            {code:'<img src="images/subitem2.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem2o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            },
            {code:'<img src="images/subitem3.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem3o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            }
        ]
    },
    {code:'<img src="images/item3.gif" width="111" height="24" />',
        ocode:'<img src="images/item3o.gif" width="111" height="24" />',
        size:[ 24, 111 ],
        sub:[
            {},
            {code:'<img src="images/subitem1.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem1o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            },
            {code:'<img src="images/subitem2.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem2o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            },
            {code:'<img src="images/subitem3.gif" width="111" height="24" />',
                ocode:'<img src="images/subitem3o.gif" width="111" height="24" />',
                size:[ 24, 111 ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
