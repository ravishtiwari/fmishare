/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    shadow:2,
    shadowColor:"#DBD8D1",
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 120 ],
    itemoff:[ "+previousItem-1px", 0 ],
    leveloff:[ 0, "+parentItem-1px" ]
};

var MENU_ITEMS = [
    {pos:"relative", style:STYLE_0, blankImage:"images/b.gif"},
    {code:"Custom tip",
        sub:[
            {},
            {code:"SubItem 1", /*1{*/tip:"First subitem."/*1}*/},
            {code:"SubItem 2", /*1{*/tip:"Second subitem."/*1}*/},
            {code:"SubItem 3", /*1{*/tip:"Third subitem."/*1}*/}
        ]   
    },
    {code:"code:\"...\" as tip",
        sub:[
            {/*2{*/tip:"code"/*2}*/},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"}
        ]   
    },
    {code:"url:\"...\" as tip",
        sub:[
            {/*3{*/tip:"url"/*1}*/},
            {code:"SubItem 1", url:"http://www.google.com"},
            {code:"SubItem 2", url:"http://www.lycos.com"},
            {code:"SubItem 3", url:"http://www.yahoo.com"}
        ]   
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
