/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var MENU_ITEMS = [
    {pos:"relative", style:STYLE_0, blankImage:'images/b.gif'},
    {code:"Relative URLS",
        sub:[
            {},
            {code:"In this file", /*1{*/url:"#sectionName"/*1}*/},
            {code:"Other file", /*1{*/url:"other.html"/*1}*/},
            {code:"Other folder", /*1{*/url:"../other_folder/file.html"/*1}*/}
        ]   
    },
    {code:"GET Queries",
        sub:[
            {},
            {code:"Same file", /*1{*/url:"?parameter1=value1"/*1}*/},
            {code:"Other file", /*1{*/url:"some_script.php?parameter2=value2"/*1}*/}
        ]   
    },
    {code:"Absolute URLS",
        sub:[
            {},
            {code:"Same server", /*1{*/url:"/some_folder/some_file.html"/*1}*/},
            {code:"Other server", /*1{*/url:"http://www.other-server.com/some_file.html"/*1}*/}
        ]   
    },
    {code:"Other protocols",
        sub:[
            {},
            {code:"HTTPS", /*1{*/url:"https://www.secure-server.com/"/*1}*/},
            {code:"FTP", /*1{*/url:"ftp://sunsite.unc.edu/"/*1}*/},
            {code:"E-mail", /*1{*/url:"mailto:somebody@somewhere.com"/*1}*/}
        ]   
    },
    {code:"JavaScript",
        sub:[
            {},
            {code:"Function call 1", /*1{*/url:"javascript:void(alert('Function 1 has been called.'))"/*1}*/},
            {code:"Function call 2", /*1{*/url:"javascript:void(alert('Function 2 has been called.'))"/*1}*/},
            {code:"Function call 3", /*1{*/url:"javascript:void(alert('Function 3 has been called.'))"/*1}*/}
        ]   
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
