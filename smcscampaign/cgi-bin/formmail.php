<?php

    function spamcheck($field)
    {
        //filter_var() sanitizes the e-mail
        //address using FILTER_SANITIZE_EMAIL
        $field=filter_var($field, FILTER_SANITIZE_EMAIL);
        
        //filter_var() validates the e-mail
        //address using FILTER_VALIDATE_EMAIL
        if(filter_var($field, FILTER_VALIDATE_EMAIL))
        {
            return TRUE;
        }else{
            return FALSE;
        }
    }

	// recipient's email address
	$to = 'md@smcsmail.com';    

	// subject of the message
	$re = 'SMCS iShare Capital Campaign Website Feedback'; 

	// message from the feedback form
	$emailAddress = $email;
	$telephone = $phone;
	$msg = 'From: '.$_REQUEST["name"]."\r\n\n".'Email: '.$_REQUEST["email"]."\r\n\n".'Phone Number: '.$_REQUEST["phone"]."\r\n\n".$_REQUEST["comments"]."\r\n\n";


    //check if the email address is invalid
    $mailcheck = spamcheck($_REQUEST['email']);
    
    if ($mailcheck==FALSE)
    {
        header( 'Location: http://smcscampaign.ishare.ca/error/invalid_input.htm' ) ;
    }else{
        // send the email now...
        mail($to,$re,$msg);
    } 

?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>iShare.ca</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

      <link rel="stylesheet" type="text/css" href="../css/default.css" />
<script type="text/javascript" src="../COOLjsMenu_Professional_2.9.4/Scripts/coolmenupro.js"></script>
<link rel="stylesheet" type="text/css" href="../js_menu/style.css" />
<link rel="stylesheet" type="text/css" href="../css/print_subpages.css" media="print" />
</head>

<body>
<div class="navigation">
<!--Header-->
  <table class="main" cellspacing="0" cellpadding="0">

    <tr>
      <td class="mainheader">
      
      <!--navigation bar, user login-->
      <form action="http://www.fundingmatters.com/ishare/cgi-bin/iShare/iShare.pl?action=login2" method="post">
          <table class="main-menu" cellpadding="0" cellspacing="0">
		    <tr class="main-menu">
				<td class="js_menu"><script type="text/javascript" src="../js_menu/menu1.js"></script></td>

		     </tr>
				 <tr>
					<td class="register" colspan="4" > <!--cellpadding="40">-->
					
				</td>
		</tr>
          </table>
	</form>
      </td>
    </tr>
  </table>
  
  <!--Main Section-->
  <table class="subpages-main" cellspacing="0" cellpadding="0" align="center">
    <tr>
    
    <!--Content Section-->
      <td class="subpages-main"> 
        <p><br />
          <br />
          <img src="../ishareImages/headings/contact_us.jpg" class="headings" alt="Bursaries" /></p>
         <div align="center">
           
	   <p class="pagedesc-withbottompic">Thank you.  Your comments and suggestions have been submitted.</p>
		
        </div>      
	</td>
	
     <!--Right Menu Section-->
      <td class="right-menu"><div class="right-menu">
	<p class="right-menu-quote"></p>
		<div align="center">
		</div>
	</div>
      </td>
    </tr>
        <tr>
	<td class="page-footer" colspan="2">2008 FUNDINGmatters Inc., All rights reserved</td>
    </tr>
  </table>

  <p>&nbsp;</p>
</div>

</body>
</html>

