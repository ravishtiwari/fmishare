###############################################################################
# Paths.pl                                                                    #
###############################################################################
# YaBB: Yet another Bulletin Board                                            #
# Open-Source Community Software for Webmasters                               #
# Version:        YaBB 2.1                                                    #
# Released:       November 8, 2005                                            #
# Distributed by: http://www.yabbforum.com                                    #
# =========================================================================== #
# Copyright (c) 2000-2005 YaBB (www.yabbforum.com) - All Rights Reserved.     #
# Software by: The YaBB Development Team                                      #
#              with assistance from the YaBB community.                       #
# Sponsored by: Xnull Internet Media, Inc. - http://www.ximinc.com            #
###############################################################################

$lastsaved = "iShare Administrator";
$lastdate = "1160752058";                                             ########## Directories ##########

$boardurl = "http://www.fundingmatters.com/smcscampaign/cgi-bin/iShare";
                                                                      # URL of your board's folder (without trailing '/')
$boarddir = ".";                                                      # The server path to the board's folder (usually can be left as '.')
$boardsdir = "./Boards";                                              # Directory with board data files
$datadir = "./Messages";                                              # Directory with messages
$memberdir = "./Members";                                             # Directory with member files
$sourcedir = "./Sources";                                             # Directory with iShare source files
$admindir = "./Admin";                                                # Directory with iShare admin source files
$vardir = "./Variables";                                              # Directory with variable files
$langdir = "./Languages";                                             # Directory with Language files and folders
$helpfile = "./Help";                                                 # Directory with Help files and folders
$templatesdir = "./Templates";                                        # Directory with template files and folders
$forumstylesdir = "/home/funding/public_html/smcscampaign/iSharefiles/Templates/Forum";
                                                                      # Directory with forum style files and folders
$adminstylesdir = "/home/funding/public_html/smcscampaign/iSharefiles/Templates/Admin";
                                                                      # Directory with admin style files and folders
$htmldir = "/home/funding/public_html/smcscampaign/iSharefiles";              # Base Path for all html/css files and folders
$facesdir = "/home/funding/public_html/smcscampaign/iSharefiles/avatars";
                                                                      # Base Path for all avatar files
$smiliesdir = "/home/funding/public_html/smcscampaign/iSharefiles/Smilies";
                                                                      # Base Path for all smilie files
$modimgdir = "/home/funding/public_html/smcscampaign/iSharefiles/ModImages";
                                                                      # Base Path for all mod images
$uploaddir = "/home/funding/public_html/smcscampaign/iSharefiles/Attachments";# Base Path for all attachment files

########## URL's ##########

$forumstylesurl = "http://www.fundingmatters.com/smcscampaign/iSharefiles/Templates/Forum";
                                                                      # Default Forum Style Directory
$adminstylesurl = "http://www.fundingmatters.com/smcscampaign/iSharefiles/Templates/Admin";
                                                                      # Default Admin Style Directory
$ubbcjspath = "http://www.fundingmatters.com/smcscampaign/iSharefiles/ubbc.js";
                                                                      # Default Location for the ubbc.js file
$faderpath = "http://www.fundingmatters.com/smcscampaign/iSharefiles/fader.js";
                                                                      # Default Location for the fader.js file
$yabbcjspath = "http://www.fundingmatters.com/smcscampaign/iSharefiles/yabbc.js";
                                                                      # Default Location for the yabbc.js file
$postjspath = "http://www.fundingmatters.com/smcscampaign/iSharefiles/post.js";
                                                                      # Default Location for the post.js file
$html_root = "http://www.fundingmatters.com/smcscampaign/iSharefiles";        # Base URL for all html/css files and folders
$facesurl = "http://www.fundingmatters.com/smcscampaign/iSharefiles/avatars";
                                                                      # Base URL for all avatar files
$smiliesurl = "http://www.fundingmatters.com/smcscampaign/iSharefiles/Smilies";
                                                                      # Base URL for all smilie files
$modimgurl = "http://www.fundingmatters.com/smcscampaign/iSharefiles/ModImages";
                                                                      # Base URL for all mod images
$uploadurl = "http://www.fundingmatters.com/smcscampaign/iSharefiles/Attachments";
                                                                      # Base URL for all attachment files

1;
