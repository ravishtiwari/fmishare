var xmlDoc;

function listDonors(){
    getDonors("supporterGifts", "Supporter" );
    getDonors("friendGifts", "Friend");
    getDonors("sponsorGifts", "Sponsor");
    getDonors("majorGifts", "Major");
    getDonors("patronGifts", "Patron");
    getDonors("leadershipGifts", "Leadership" );
    getDonors("pacesettingGifts", "Pacesetting" );
    getDonors("bequestPatronGifts", "Bequest Patron" );
};

function getDonors(section, giftLevel){
   try{
        // Internet Explorer
        xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.load("../xml/donors.xml");
    }catch(e){
        try{
            // Firefox, Mozilla, Opera, etc.
            var xmlhttp = new window.XMLHttpRequest();
			xmlhttp.open("GET","../xml/donors.xml",false);
			xmlhttp.send(null);
			xmlDoc = xmlhttp.responseXML.documentElement;
        }catch(e){
            alert(e.message);
            return;
        }
    }
    
    var x = xmlDoc.getElementsByTagName("Donor");
    
    output = "";
    
    if (giftLevel == "Pacesetting"){
        output = '<span class="blue-bold-title">PACESETTING GIFTS ($250,000 +)</span><br/>';
    }else if (giftLevel == "Leadership" || giftLevel == "Bequest Leadership"){
        output = '<span class="blue-bold-title">LEADERSHIP GIFTS ($125,000 to $249,999)</span><br/>';
    }else if (giftLevel == "Patron"){
        output = '<span class="blue-bold-title">PATRON GIFTS ($25,000 to $124,999)</span><br/>';
    }else if (giftLevel == "Major"){
        output = '<span class="blue-bold-title">MAJOR GIFTS ($5,000 to $24,999)</span><br/>';
    }else if (giftLevel == "Sponsor"){
        output = '<span class="blue-bold-title">SPONSOR GIFTS ($2,500 to $4,999)</span><br/>';
    }else if (giftLevel == "Friend"){
        output = '<span class="blue-bold-title">FRIEND GIFTS ($500 to $2,499)</span><br/>';
    }else if (giftLevel == "Supporter"){
        output = '<span class="blue-bold-title">SUPPORTER GIFTS (up to $499)</span><br/>';
    }else if (giftLevel == "Bequest Patron"){
        output = '<span class="blue-bold-title">BEQUEST PATRON GIFTS ($25,000 to $124,999)</span><br/>';
    }

    for (var i=0;i<x.length;i++){ 
        if (x[i].getElementsByTagName("GiftLevel")[0].childNodes[0].nodeValue == giftLevel){
            output = output + x[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue + "<br/>";
        }
    }
    
    output = output + "";
    document.getElementById(section).innerHTML = output;
};
