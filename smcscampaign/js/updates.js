var xmlDoc;

function frontPageUpdates(){
    // controls donation updates
    updateNews("updatesDonations", "Donations");
    
    // controls media updates
    updateNews("updatesMedia", "Media");
    
    // controls general news and events updates
    updateNews("updatesNews", "News");
};

function updateNews(section, category){
    try{
        // Internet Explorer
        xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.load("xml/news.xml");
    }catch(e){
        try{
            // Firefox, Mozilla, Opera, etc.
            var xmlhttp = new window.XMLHttpRequest();
			xmlhttp.open("GET","xml/news.xml",false);
			xmlhttp.send(null);
			xmlDoc = xmlhttp.responseXML.documentElement;
        }catch(e){
            alert(e.message);
            return;
        }
    }
    
    var x = xmlDoc.getElementsByTagName("NewsItem");
    output = "<ul>";

    for (var i=0;i<x.length;i++){ 
        if(x[i].getElementsByTagName("Category")[0].childNodes[0].nodeValue == category){
            
            output = output + '<li><span class="newsTitle">' + x[i].getElementsByTagName("Date")[0].childNodes[0].nodeValue + ' - ' + x[i].getElementsByTagName("Title")[0].childNodes[0].nodeValue + '</span><br />' + x[i].getElementsByTagName("Details")[0].childNodes[0].nodeValue + '<br /><br /></li>';
            
        }
    }
    
    output = output + "</ul>";
    document.getElementById(section).innerHTML = output;
};


