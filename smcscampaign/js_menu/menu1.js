/* COOLjsMenu style + structure file */
/*For html files in ishare_project folder*/

var STYLE_0 =
{

    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 23, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-5px', 0 ]
};



var SUB_STYLE =
{
	borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ]
    /*eveloff:[ '+parentItem+1px', 0 ]*/

}

var color = {border:"#666666", bgON:"white",bgOVER:"#B6BDD2"};
var css = {ON:"clsCMOn", OVER:"clsCMOver"};

var MENU_ITEMS = [
    {pos:'relative', style:[ STYLE_0], blankImage:'http://smcscampaign.ishare.ca/js_menu/images/b.gif'},
    {code:"", size:[ 22, 54 ], url:"http://smcscampaign.ishare.ca/index.htm",
        sub:[
		{style:SUB_STYLE, size:[24,215]},
		{code:"Mission&nbsp;Statement", url:"http://smcscampaign.ishare.ca/home/mission_statement.htm", sub:[]}, 
		{code:"About&nbsp;SMCS", url:"http://smcscampaign.ishare.ca/home/about_smcs.htm", sub:[]},
		{code:"Campaign&nbsp;Leadership", url:"http://smcscampaign.ishare.ca/home/campaign_leadership.htm", sub:[]},
		{code:"Strategic&nbsp;Plan&nbsp;2007-2012", url:"http://smcscampaign.ishare.ca/home/strategic_objectives.htm", sub:[]},
		{code:"St.&nbsp;Michael&#39;s&nbsp;College&nbsp;School&nbsp;Website", target: "_blank", url:"http://www.stmichaelscollegeschool.com/hm", sub:[]}
        ]
    },
    {code:"", size:[22,66], url:"http://smcscampaign.ishare.ca/donate/make_donation.htm",
        sub:[
		{style:SUB_STYLE, size:[24,130]},
		{code:"Investment&nbsp;Options", url:"http://smcscampaign.ishare.ca/donate/investment_options.htm", sub:[]},
		{code:"Giftabulator", url:"http://smcscampaign.ishare.ca/giftabulator/giftabulator.html", sub:[]},
		{code:"List of Donors", url:"http://smcscampaign.ishare.ca/donate/donors.htm", sub:[]},
		{code:"Make&nbsp;a&nbsp;Donation", url:"http://smcscampaign.ishare.ca/donate/make_donation.htm", sub:[]},
        ]
    },
    {code:"", size:[22,174], url:"http://smcscampaign.ishare.ca/theatre/theatre.htm", 
        sub:[
		{style:SUB_STYLE, size:[24,174]},
		{code:"centre&nbsp;for&nbsp;the&nbsp;arts", url:"http://smcscampaign.ishare.ca/theatre/theatre.htm", sub:[]},
		{code:"Building&nbsp;Team", url:"http://smcscampaign.ishare.ca/theatre/building_team.htm", sub:[]},
		{code:"Naming&nbsp;Opportunities", url:"http://smcscampaign.ishare.ca/theatre/theatre_naming.htm", sub:[]},
		{code:"Historical Timeline", url:"http://smcscampaign.ishare.ca/theatre/timeline.htm", sub:[]},
        ]
    },
        {code:"", size:[22,134], url:"http://smcscampaign.ishare.ca/learning_centre/learning_centre.htm",
        sub:[
		{style:SUB_STYLE, size:[24,178]},
		{code:"Learning&nbsp;Centre&nbsp;Introduction", url:"http://smcscampaign.ishare.ca/learning_centre/learning_centre.htm", sub:[]},
		{code:"Learning&nbsp;Centre&nbsp;Initiative", url:"http://smcscampaign.ishare.ca/learning_centre/initiative.htm", sub:[]},
        ]
    },
	{code:"", size:[22,100], url:"http://smcscampaign.ishare.ca/bursaries/bursaries.htm",
		sub:[
			{style:SUB_STYLE, size:[24,133]},
			{code:"Bursaries&nbsp;Information", url:"http://smcscampaign.ishare.ca/bursaries/bursaries.htm", sub:[]},
		]
	},
	{code:"", size:[22,80], url:"http://smcscampaign.ishare.ca/media/videos_app.html",
		sub:[
			{style:SUB_STYLE, size:[24,250]},
			{code:"Videos", url:"http://smcscampaign.ishare.ca/media/videos_app.html", sub:[]},
			{code:"Photo&nbsp;Gallery", url:"http://smcscampaign.ishare.ca/media/image_app.html", sub:[]},
			{code:"SMCS Campaign Special Magazine", target: "_blank", url:"http://smcscampaign.ishare.ca/media/magazines/special_campaign_edition_magazine.pdf", sub:[]},
            {code:"Capital Campaign Newsletter - March 2009", target: "_blank", url:"http://smcscampaign.ishare.ca/media/magazines/capital_campaign_newsletter_mar20092.pdf", sub:[]},
		]
	},
		{code:"", size:[22,100], url:"http://www.fundingmatters.com/smcscampaign/cgi-bin/iShare/iShare.pl",
		sub:[
			{style:SUB_STYLE, size:[24,100]},
			{code:"Register", url:"http://www.fundingmatters.com/smcscampaign/cgi-bin/iShare/iShare.pl?action=register", sub:[]},
			{code:"Forum", url:"http://www.fundingmatters.com/smcscampaign/cgi-bin/iShare/iShare.pl", sub:[]},
		]
	},
		{code:"", size:[22,103], url:"http://smcscampaign.ishare.ca/contact_us/contact_us.htm",
		sub:[
			{style:SUB_STYLE, size:[24,103]},
			{code:"Contact Us", url:"http://smcscampaign.ishare.ca/contact_us/contact_us.htm", sub:[]},
		]
	}	

];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
