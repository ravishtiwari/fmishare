/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    textClass:[ "l0_out", "l0_over" ],
    size:[ 24, 118 ],
    itemoff:[ 0, "+previousItem+1px" ],
    leveloff:[ "+parentItem+1px", 0 ],
    imgsize:[ 24, 26 ],
    image:[ "images/l0_icon_out.gif", "images/l0_icon_over.gif" ]
};

var STYLE_1 =
{
    backgroundClass:[ "l1_bg_middle", "l1_bg_middle" ],
    textClass:[ "l1_out", "l1_over" ],
    size:[ 16, 118 ],
    itemoff:[ "+previousItem", 0 ],
    image:null,
    ifFirst:{
        backgroundClass:[ "l1_bg_top", "l1_bg_top" ],
        size:[ 26, 118 ],
        valign:"bottom"
    },
    ifLast:{
        backgroundClass:[ "l1_bg_bottom", "l1_bg_bottom" ],
        size:[ 36, 118 ],
        valign:"top"
    },
    filters:"progid:DXImageTransform.Microsoft.Alpha(opacity=90)"
};

var MENU_ITEMS = [
    {style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif"},
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 2", url:"#",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
