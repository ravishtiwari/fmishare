/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    levelFilters:"progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#aaaaaa,strength=3)",
    borderWidth:1,
    borderColor:"#0B42B7",
    backgroundStyle:[ "background: #E1E8F6 left top repeat-y url(images/l0_bg_out.gif);", "background: #C7D4EE left top repeat-y url(images/l0_bg_over.gif);" ],
    textStyle:[ "text-align: right; color: #0B42B7; font: bold 9pt Verdana, sans-serif; padding-right: 13px;", "text-align: right; color: #171D28; font: bold 9pt Verdana, sans-serif; padding-right: 13px;" ],
    size:[ 30, 159 ],
    itemoff:[ "+previousItem-1px", 0 ],
    leveloff:[ 0, "+parentItem-1px" ],
    imgsize:[ 28, 41 ],
    image:null
};

var STYLE_1 =
{
    borderWidth:1,
    borderColor:"#0B42B7",
    backgroundStyle:[ "background: #E1E8F6 left top repeat-y url(images/l1_bg_out.gif);", "background: #C7D4EE left top repeat-y url(images/l1_bg_over.gif);" ],
    textStyle:[ "text-align: right; color: #0B42B7; font: 8pt Verdana, sans-serif; padding-right: 19px;", "text-align: right; color: #171D28; font: 8pt Verdana, sans-serif; padding-right: 19px;" ],
    size:[ 30, 141 ],
    imgsize:[ 28, 35 ],
    image:null
};

var MENU_ITEMS = [
    {exclusive:true, style:[ STYLE_0, STYLE_1 ], blankImage:"images/b.gif" },
    {code:"Item 1", image:"images/ico_0_0.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"images/ico_1_0.gif"},
            {code:"SubItem 2", image:"images/ico_1_1.gif"},
            {code:"SubItem 3", image:"images/ico_1_2.gif"},
            {code:"SubItem 4", image:"images/ico_1_3.gif"}
        ]
    },
    {code:"Item 2", image:"images/ico_0_1.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"images/ico_1_2.gif"},
            {code:"SubItem 2", image:"images/ico_1_3.gif"},
            {code:"SubItem 3", image:"images/ico_1_0.gif"},
            {code:"SubItem 4", image:"images/ico_1_1.gif"}
        ]
    },
    {code:"Item 3", image:"images/ico_0_2.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"images/ico_1_3.gif"},
            {code:"SubItem 2", image:"images/ico_1_0.gif"},
            {code:"SubItem 3", image:"images/ico_1_2.gif"},
            {code:"SubItem 4", image:"images/ico_1_1.gif"}
        ]
    },
    {code:"Item 4", image:"images/ico_0_3.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"images/ico_1_3.gif"},
            {code:"SubItem 2", image:"images/ico_1_2.gif"},
            {code:"SubItem 3", image:"images/ico_1_0.gif"},
            {code:"SubItem 4", image:"images/ico_1_1.gif"}
        ]
    },
    {code:"Item 5", image:"images/ico_0_4.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"images/ico_1_1.gif"},
            {code:"SubItem 2", image:"images/ico_1_3.gif"},
            {code:"SubItem 3", image:"images/ico_1_2.gif"},
            {code:"SubItem 4", image:"images/ico_1_0.gif"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
