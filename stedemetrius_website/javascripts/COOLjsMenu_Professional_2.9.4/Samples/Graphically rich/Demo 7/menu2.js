/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    levelFilters:"progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#aaaaaa,strength=3)",
    borderWidth:[ 1, 0, 1, 0 ],
    borderColor:"#0B42B7",
    backgroundStyle:[ "background: #E1E8F6 left top repeat-y url(images/l0_bg_out.gif);", "background: #C7D4EE left top repeat-y url(images/l0_bg_over.gif);" ],
    textStyle:[ "text-align: right; color: #0B42B7; font: bold 9pt Verdana, sans-serif; padding-right: 13px;", "text-align: right; color: #171D28; font: bold 9pt Verdana, sans-serif; padding-right: 13px;" ],
    size:[ 28, 159 ],
    itemoff:[ "+previousItem", 0 ],
    leveloff:[ 0, "+parentItem-1px" ],
    imgsize:[ 28, 41 ],
    image:null,
    ifFirst:
    {
        borderWidth:[ 1, 1, 1, 0 ],
        size:[ 29, 159 ]
    },
    ifLast:
    {
        borderWidth:[ 1, 0, 1, 1 ],
        size:[ 29, 159 ]
    }
};

var STYLE_1 =
{
    borderWidth:[ 1, 0, 1, 0 ],
    borderColor:"#0B42B7",
    backgroundStyle:[ "background: #E1E8F6 left top repeat-y url(images/l1_bg_out.gif);", "background: #C7D4EE left top repeat-y url(images/l1_bg_over.gif);" ],
    textStyle:[ "text-align: right; color: #0B42B7; font: 8pt Verdana, sans-serif; padding-right: 19px;", "text-align: right; color: #171D28; font: 8pt Verdana, sans-serif; padding-right: 19px;" ],
    size:[ 28, 141 ],
    imgsize:[ 28, 35 ],
    image:null,
    ifFirst:
    {
        borderWidth:[ 1, 1, 1, 0 ],
        size:[ 29, 141 ]
    },
    ifLast:
    {
        borderWidth:[ 1, 0, 1, 1 ],
        size:[ 29, 141 ]
    }
};

var MENU_ITEMS = [
    {pos:"relative", exclusive:true, style:[ STYLE_0, STYLE_1 ], dynamic:true, delay:[ 50, 800 ], imagePrefix:"images/", blankImage:"b.gif" },
    {code:"Item 1", image:"ico_0_0.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"ico_1_0.gif"},
            {code:"SubItem 2", image:"ico_1_1.gif"},
            {code:"SubItem 3", image:"ico_1_2.gif"},
            {code:"SubItem 4", image:"ico_1_3.gif"}
        ]
    },
    {code:"Item 2", image:"ico_0_1.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"ico_1_2.gif"},
            {code:"SubItem 2", image:"ico_1_3.gif"},
            {code:"SubItem 3", image:"ico_1_0.gif"},
            {code:"SubItem 4", image:"ico_1_1.gif"}
        ]
    },
    {code:"Item 3", image:"ico_0_2.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"ico_1_3.gif"},
            {code:"SubItem 2", image:"ico_1_0.gif"},
            {code:"SubItem 3", image:"ico_1_2.gif"},
            {code:"SubItem 4", image:"ico_1_1.gif"}
        ]
    },
    {code:"Item 4", image:"ico_0_3.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"ico_1_3.gif"},
            {code:"SubItem 2", image:"ico_1_2.gif"},
            {code:"SubItem 3", image:"ico_1_0.gif"},
            {code:"SubItem 4", image:"ico_1_1.gif"}
        ]
    },
    {code:"Item 5", image:"ico_0_4.gif",
        sub:[
            {},
            {code:"SubItem 1", image:"ico_1_1.gif"},
            {code:"SubItem 2", image:"ico_1_3.gif"},
            {code:"SubItem 3", image:"ico_1_2.gif"},
            {code:"SubItem 4", image:"ico_1_0.gif"}
        ]
    }
];

var menu2 = new COOLjsMenuPRO("menu2", MENU_ITEMS);
menu2.initTop();
