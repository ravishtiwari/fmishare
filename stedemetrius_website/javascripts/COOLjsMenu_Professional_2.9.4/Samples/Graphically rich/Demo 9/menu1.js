/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    itemFilters:"progid:DXImageTransform.Microsoft.Fade(duration=0.3)",
    size:[ 24, 146 ],
    backgroundStyle:[ "background: #F5EEDD url(images/l0_bg_out.gif);", "background: #4FC4FA url(images/l0_bg_over.gif);" ],
    borderWidth:1,
    borderColor:[ "#E4D9C0", "#118AC2" ],
    textStyle:[ "color: #8E8C77; font: bold 8pt Arial, sans-serif; text-align: right; padding-right: 21px;", "color: #DF0000; font: bold 8pt Arial, sans-serif; text-align: right; padding-right: 21px;" ],
    itemoff:[ "+previousItem-1px", 0 ],
    leveloff:[ 3, "+parentItem-9px", -14 ]
};

var STYLE_1 =
{
    levelFilters:"progid:DXImageTransform.Microsoft.Alpha(opacity=80)",
    //levelFilters:"progid:DXImageTransform.Microsoft.Fade(duration=0.3)",
    levelFilters:[ "progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=reverse,duration=0.2)", "progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.2)"],
    itemFilters:null,
    borderWidth:0,
    backgroundStyle:[ "background: #F8F2E4 url(images/l1_bg_other_out.gif);", "background: #7FD4FC url(images/l1_bg_over.gif);"  ],
    ifFirst:
    {
        backgroundStyle:[ "background: #F8F2E4 url(images/l1_bg_first_out.gif);", "background: #7FD4FC url(images/l1_bg_over.gif);"  ]
    },
    size:[ 24, 139 ],
    itemoff:[ "+previousItem-1px", 0 ]
};

var MENU_ITEMS = [
    {exclusive:true, style:[ STYLE_0, STYLE_1 ], delay:[ 200, 800 ], blankImage:"images/b.gif" },
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 2",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    },
    {code:"Item 5",
        sub:[
            {},
            {code:"SubItem 1"},
            {code:"SubItem 2"},
            {code:"SubItem 3"},
            {code:"SubItem 4"},
            {code:"SubItem 5"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
