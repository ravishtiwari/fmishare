/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    /*1{*/itemoff:[ 0, '+previousItem-1px' ]/*1}*/,
    /*2{*/leveloff:[ '+parentItem-1px', 'level' ]/*2}*/
};

var MENU_ITEMS = [
    {pos:'relative', /*3{*/style:STYLE_0/*3}*/, blankImage:'images/b.gif'},
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1.1"},
            {code:"SubItem 1.2"},
            {code:"SubItem 1.3"},
            {code:"SubItem 1.4"},
            {code:"SubItem 1.5"}
        ]
    },
    {code:"Item 2",
        sub:[
            {},
            {code:"SubItem 2.1"},
            {code:"SubItem 2.2"},
            {code:"SubItem 2.3"},
            {code:"SubItem 2.4"},
            {code:"SubItem 2.5"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 3.1"},
            {code:"SubItem 3.2"},
            {code:"SubItem 3.3"},
            {code:"SubItem 3.4"},
            {code:"SubItem 3.5"}
        ]
    },
    {code:"Item 4",
        sub:[
            {},
            {code:"SubItem 4.1"},
            {code:"SubItem 4.2"},
            {code:"SubItem 4.3"},
            {code:"SubItem 4.4"},
            {code:"SubItem 4.5"}
        ]
    },
    {code:"Item 5",
        sub:[
            {},
            {code:"SubItem 5.1"},
            {code:"SubItem 5.2"},
            {code:"SubItem 5.3"},
            {code:"SubItem 5.4"},
            {code:"SubItem 5.5"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
