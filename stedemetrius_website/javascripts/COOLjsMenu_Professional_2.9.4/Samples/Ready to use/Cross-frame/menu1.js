/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem-1px', 0 ]
};

var STYLE_2 =
{
    leveloff:[ 0, '+parentItem-1px' ]
};

var MENU_ITEMS = [
    {pos:"relative", style:[ STYLE_0, STYLE_1, STYLE_2 ], blankImage:'images/b.gif', /*1{*/frames:[ "menuFrame", "mainFrame" ]/*1}*/},
    {code:"Item 1",
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            }
        ]
    },
    {code:"Item 2",
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            }
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubItem 1"},
                    {code:"SubItem 2"},
                    {code:"SubItem 3"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
