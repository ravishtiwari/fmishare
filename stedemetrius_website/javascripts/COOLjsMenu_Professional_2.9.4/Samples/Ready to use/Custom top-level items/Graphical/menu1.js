/* COOLjsMenu style + structure file */

var STYLE_1 = {
    shadow:2,
    shadowColor:"#DBD8D1",
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ "+previousItem-1px", 0 ],
    leveloff:[ "+parentItem-4px", 0 ]
};

var STYLE_2 = {
    leveloff:[ 0, "+parentItem-1px" ]
};

var MENU_ITEMS = [
    {/*1{*/pos:[ 10, 15 ]/*1}*/, style:[ /*2{*/{}/*2}*/, STYLE_1, STYLE_2 ], blankImage:'images/b.gif'},
    {code:"", size:[ 28, 79 ],
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"", itemoff:[ "level+35px", "level+54px" ], size:[ 28, 79 ],
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"", itemoff:[ "level+7px", "level+131px" ], size:[ 28, 79 ],
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"", itemoff:[ "level+44px", "level+177px" ], size:[ 28, 79 ],
        sub:[
            {},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
