/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ 0, '+previousItem-1px' ],
    leveloff:[ '+parentItem-1px', 0 ]
};

var STYLE_1 =
{
    itemoff:[ '+previousItem-1px', 0 ]
};

var MENU_ITEMS = [
    {pos:"relative", style:[ STYLE_0, STYLE_1 ], blankImage:'images/b.gif'},
    {code:"This frame",
        sub:[
            {},
            {code:"Google", url:"http://www.google.com/"},
            {code:"Lycos", url:"http://www.lycos.com/"},
            {code:"Yahoo!", url:"http://www.yahoo.com/"}
        ]
    },
    {code:"Other frame",
        sub:[
            {},
            {code:"Google", url:"http://www.google.com/", /*1{*/target:"contents"/*1}*/},
            {code:"Lycos", url:"http://www.lycos.com/", /*1{*/target:"contents"/*1}*/},
            {code:"Yahoo!", url:"http://www.yahoo.com/", /*1{*/target:"contents"/*1}*/}
        ]
    },
    {code:"New window",
        sub:[
            {},
            {code:"Google", url:"http://www.google.com/", /*2{*/target:"_blank"/*2}*/},
            {code:"Lycos", url:"http://www.lycos.com/", /*2{*/target:"_blank"/*2}*/},
            {code:"Yahoo!", url:"http://www.yahoo.com/", /*2{*/target:"_blank"/*2}*/}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
