/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var STYLE_1 = {
    backgroundColor:"white",
    backgroundClass:"clsCentered"
};

var MENU_ITEMS = [
    {pos:"relative", style:[ STYLE_0, STYLE_1 ], blankImage:'images/b.gif'},
    {code:"Item 1",
        sub:[
            {},
            {/*1{*/code:"SubItem 1", ocode:"SubItem <u>1</u>"/*1}*/},
            {code:"SubItem 2", ocode:"SubItem <u>2</u>"},
            {code:"SubItem 3", ocode:"SubItem <u>3</u>"},
            {code:"SubItem 4", ocode:"SubItem <u>4</u>"}
        ]
    },
    {code:"Item 2",
        sub:[
            {},
            {code:"SubItem 1", ocode:"- SubItem 1 -"},
            {code:"SubItem 2", ocode:"- SubItem 2 -"},
            {code:"SubItem 3", ocode:"- SubItem 3 -"},
            {code:"SubItem 4", ocode:"- SubItem 4 -"}
        ]
    },
    {code:"Item 3",
        sub:[
            {},
            {code:"SubItem 1", ocode:"[ SubItem 1 ]"},
            {code:"SubItem 2", ocode:"[ SubItem 2 ]"},
            {code:"SubItem 3", ocode:"[ SubItem 3 ]"},
            {code:"SubItem 4", ocode:"[ SubItem 4 ]"}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
