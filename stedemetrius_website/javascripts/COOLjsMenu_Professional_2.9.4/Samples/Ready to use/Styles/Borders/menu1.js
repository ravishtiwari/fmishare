/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    /*1{*/borderWidth:1,/*1}*/
    /*1{*/borderColor:"#666666",/*1}*/
    backgroundColor:[ "white", "#B6BDD2" ],
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 30, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var STYLE_BORDERS_0 =
{
    backgroundColor:"#F8F8F8",
    itemoff:[ '+previousItem+5px', 0 ],
    leveloff:[ 0, '+parentItem+5px' ],
    /*1{*/borderWidth:0/*1}*/
};

var STYLE_BORDERS_1 =
{
    itemoff:[ '+previousItem+5px', 0 ],
    leveloff:[ 0, '+parentItem+5px' ],
    /*1{*/borderWidth:1,/*1}*/
    /*1{*/borderColor:"#00FF00"/*1}*/
};

var STYLE_BORDERS_2 =
{
    itemoff:[ '+previousItem+5px', 0 ],
    leveloff:[ 0, '+parentItem+5px' ],
    /*1{*/borderWidth:2,/*1}*/
    /*1{*/borderColor:"#0000FF"/*1}*/
};

var STYLE_BORDERS_3 =
{
    backgroundColor:"#F8F8F8",
    itemoff:[ '+previousItem+5px', 0 ],
    leveloff:[ 0, '+parentItem+5px' ],
    /*1{*/borderWidth:[ 5, 0, 0, 0 ],/*1}*/
    /*1{*/borderColor:"#E00020"/*1}*/
};

var STYLE_BORDERS_4 =
{
    itemoff:[ '+previousItem-2px', 0 ],
    leveloff:[ 0, '+parentItem-2px' ],
    /*1{*/borderWidth:[ 2, 0, 2, 0 ],/*1}*/
    borderColor:"#2000E0",
    ifFirst:
    {
        /*1{*/borderWidth:[ 2, 2, 2, 0 ]/*1}*/
    },
    ifLast:
    {
        /*1{*/borderWidth:[ 2, 0, 2, 2 ]/*1}*/
    },
    ifOnly:
    {
        /*1{*/borderWidth:[ 2, 2, 2, 2 ]/*1}*/
    }
};

var MENU_ITEMS = [
    {pos:"relative", style:STYLE_0, blankImage:'images/b.gif'},
    {code:"0px",
        sub:[
            {/*1{*/style:STYLE_BORDERS_0/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"1px green",
        sub:[
            {/*1{*/style:STYLE_BORDERS_1/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"2px blue",
        sub:[
            {/*1{*/style:STYLE_BORDERS_2/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"5px red left",
        sub:[
            {/*1{*/style:STYLE_BORDERS_3/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"No inner lines",
        sub:[
            {/*1{*/style:STYLE_BORDERS_4/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"One item"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"First item"},
                    {code:"Second item"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"First item"},
                    {code:"Second item"},
                    {code:"Third item"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
