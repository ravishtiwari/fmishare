/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    borderWidth:1,
    borderColor:"#666666",
    backgroundColor:[ "white", "#B6BDD2" ],
    /*1{*/textClass:[ "clsCMOn", "clsCMOver" ],/*1}*/
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
};

var RED_STYLE =
{
    /*1{*/textClass:[ "clsCMOnRed", "clsCMOverRed" ]/*1}*/
};

var GREEN_STYLE =
{
    /*1{*/textClass:[ "clsCMOnGreen", "clsCMOverGreen" ]/*1}*/
};

var BLUE_STYLE =
{
    /*1{*/textClass:[ "clsCMOnBlue", "clsCMOverBlue" ]/*1}*/
};

var CENTERED_STYLE =
{
    /*1{*/textClass:[ "clsCMOnCentered", "clsCMOverCentered" ]/*1}*/
};

var IMAGED_STYLE =
{
    /*1{*/backgroundClass:[ "clsCMOnImaged", "clsCMOverImaged" ]/*1}*/
};

var MENU_ITEMS = [
    {pos:"relative", style:STYLE_0, blankImage:'images/b.gif'},
    {code:"Red items",
        sub:[
            {/*1{*/style:RED_STYLE/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Green items",
        sub:[
            {/*1{*/style:GREEN_STYLE/*1}*/},
            {code:"Red items",
                sub:[
                    {/*1{*/style:RED_STYLE/*1}*/},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"Green items",
                sub:[
                    {/*1{*/style:GREEN_STYLE/*1}*/},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"Blue items",
                sub:[
                    {/*1{*/style:BLUE_STYLE/*1}*/},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    },
    {code:"Mixed items",
        sub:[
            {},
            {code:"SubItem 1", /*1{*/style:RED_STYLE/*1}*/,
                sub:[
                    {},
                    {code:"SubSubItem 1", /*1{*/style:BLUE_STYLE/*1}*/},
                    {code:"SubSubItem 2", /*1{*/style:GREEN_STYLE/*1}*/},
                    {code:"SubSubItem 3", /*1{*/style:RED_STYLE/*1}*/}
                ]
            },
            {code:"SubItem 2", /*1{*/style:GREEN_STYLE/*1}*/,
                sub:[
                    {},
                    {code:"SubSubItem 1", /*1{*/style:RED_STYLE/*1}*/},
                    {code:"SubSubItem 2", /*1{*/style:BLUE_STYLE/*1}*/},
                    {code:"SubSubItem 3", /*1{*/style:GREEN_STYLE/*1}*/}
                ]
            },
            {code:"SubItem 3", /*1{*/style:BLUE_STYLE/*1}*/,
                sub:[
                    {},
                    {code:"SubSubItem 1", /*1{*/style:RED_STYLE/*1}*/},
                    {code:"SubSubItem 2", /*1{*/style:GREEN_STYLE/*1}*/},
                    {code:"SubSubItem 3", /*1{*/style:BLUE_STYLE/*1}*/}
                ]
            }
        ]
    },
    {code:"Centered items",
        sub:[
            {/*1{*/style:CENTERED_STYLE/*1}*/},
            {code:"Tiny",
                sub:[
                    {},
                    {code:"1"},
                    {code:"2"},
                    {code:"3"}
                ]
            },
            {code:"Medium",
                sub:[
                    {},
                    {code:"4"},
                    {code:"5"},
                    {code:"6"}
                ]
            },
            {code:"Quite long",
                sub:[
                    {},
                    {code:"7"},
                    {code:"8"},
                    {code:"9"}
                ]
            }
        ]
    },
    {code:"Bg. image",
        sub:[
            {/*1{*/style:IMAGED_STYLE/*1}*/},
            {code:"SubItem 1",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 2",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            },
            {code:"SubItem 3",
                sub:[
                    {},
                    {code:"SubSubItem 1"},
                    {code:"SubSubItem 2"},
                    {code:"SubSubItem 3"}
                ]
            }
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
