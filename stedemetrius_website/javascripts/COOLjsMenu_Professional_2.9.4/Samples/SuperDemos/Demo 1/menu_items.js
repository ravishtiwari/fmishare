BLANK_IMAGE = 'images/b.gif';

var STYLE = {
	border:0,
	shadow:1,
	color:{
		border:"#666666",
		shadow:"#666666",
		bgON:"#E0E0E0",
		bgOVER:"#F0F0F0"
	},
	css:{
		ON:"clsCMOn",
		OVER:"clsCMOver"
	}
};

var STYLE_FADE = {
	border:1,
	shadow:0,
	color:{
		border:"#666666",
		shadow:"#DBD8D1",
		bgON:"#F0F0F0",
		bgOVER:"#B6BDD2"
	},
	css:{
		ON:"clsCMOn",
		OVER:"clsCMOver"
	},
	transition:{
		fadeIn:'progid:DXImageTransform.Microsoft.Fade(duration=0.3)',
		fadeOut:'progid:DXImageTransform.Microsoft.Fade(duration=0.3)'
	}
};

var STYLE_DISSOLVE = {
	border:1,
	shadow:0,
	color:{
		border:"#666666",
		shadow:"#DBD8D1",
		bgON:"#F0F0F0",
		bgOVER:"#B6BDD2"
	},
	css:{
		ON:"clsCMOn",
		OVER:"clsCMOver"
	},
	transition:{
		fadeIn:'progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.3)',
		fadeOut:'progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.3)'
	}
};

var STYLE_WIPE1 = {
	border:1,
	shadow:0,
	color:{
		border:"#666666",
		shadow:"#DBD8D1",
		bgON:"#F0F0F0",
		bgOVER:"#B6BDD2"
	},
	css:{
		ON:"clsCMOn",
		OVER:"clsCMOver"
	},
	transition:{
		fadeIn:'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.5)',
		fadeOut:'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.5)'
	}
};

var STYLE_WIPE2 = {
	border:1,
	shadow:0,
	color:{
		border:"#666666",
		shadow:"#DBD8D1",
		bgON:"#F0F0F0",
		bgOVER:"#B6BDD2"
	},
	css:{
		ON:"clsCMOn",
		OVER:"clsCMOver"
	},
	transition:{
		fadeIn:'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.5)',
		fadeOut:'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=1,motion=forward,duration=0.5)'
	}
};

var MENU_ITEMS = [
	{pos:[10,10], itemoff:['+previousItem',0], leveloff:[0,'+parentItem+2px'], autosize:true, style:STYLE, size:['+maxMain','+maxMain'],
		arrsize:[18,18], arrow:'images/arr.gif', oarrow:'images/arr_h.gif'
		/*, filters:'progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#000000,strength=1)'*/
	},
	{code:'Message Icons',
		sub:[
			{imgsize:[18,18], size:['+main','+maxMain+18px']},
			{code:'Question', format:{image:'images/ico_question.gif', oimage:'images/ico_question_h.gif'}, sub:[{}, {code:'Test'}]},
			{code:'Warning', format:{image:'images/ico_warning.gif', oimage:'images/ico_warning_h.gif'}},
			{code:'Error', format:{image:'images/ico_error.gif', oimage:'images/ico_error_h.gif'}}
		]
	},
	{code:"Filters",
		sub:[
			{},
			{code:"Transparency",
				sub:[
					{filters:'progid:DXImageTransform.Microsoft.Alpha(opacity=40)'},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"Shadow",
				sub:[
					{filters:'progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#aaaaaa,strength=3)'},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"Waves",
				sub:[
					{filters:'progid:DXImageTransform.Microsoft.Wave(freq=1,LightStrength=10,Phase=-17,Strength=5)'},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			},
			{code:"Combined",
				sub:[
					{filters:'progid:DXImageTransform.Microsoft.Alpha(opacity=60) progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#CCCCCC,strength=5) progid:DXImageTransform.Microsoft.Wave(freq=1,LightStrength=10,Phase=-17,Strength=5)'},
					{code:"SubSubItem 1"},
					{code:"SubSubItem 2"},
					{code:"SubSubItem 3"}
				]
			}
		]
	},
	{code:"Transitions",
		sub:[
			{},
			{code:"Fade",
				sub:[
					{itemoff:[21,0], fadeIn:'progid:DXImageTransform.Microsoft.Fade(duration=0.3)', fadeOut:'progid:DXImageTransform.Microsoft.Fade(duration=0.3)'},
					{code:"SubItem 1",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 2",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 3",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					}
				]
			},
			{code:"Pixelate",
				sub:[
					{itemoff:[21,0], fadeIn:'progid:DXImageTransform.Microsoft.Pixelate(duration=0.3)', fadeOut:'progid:DXImageTransform.Microsoft.Pixelate(duration=0.3)'},
					{code:"SubItem 1",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 2",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 3",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					}
				]
			},
			{code:"Dissolve",
				sub:[
					{itemoff:[21,0], fadeIn:'progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.3)', fadeOut:'progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.3)'},
					{code:"SubItem 1",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 2",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 3",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					}
				]
			},
			{code:"Wipe",
				sub:[
					{itemoff:[21,0], fadeIn:'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.5)', fadeOut:'progid:DXImageTransform.Microsoft.GradientWipe(GradientSize=0.25,wipestyle=0,motion=forward,duration=0.5)'},
					{code:"SubItem 1",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 2",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					},
					{code:"SubItem 3",
						sub:[
							{leveloff:[0,99]},
							{code:"SubSubItem 1"},
							{code:"SubSubItem 2"},
							{code:"SubSubItem 3"}
						]
					}
				]
			}
		]
	},
	{code:'Rollover transitions',
		sub:[
			{},
			{code:"Fade",
				sub:[
					{style:STYLE_FADE},
					{code:"SubItem 1"},
					{code:"SubItem 2"},
					{code:"SubItem 3"},
					{code:"SubItem 4"},
					{code:"SubItem 5"},
					{code:"SubItem 6"},
					{code:"SubItem 7"},
					{code:"SubItem 8"},
					{code:"SubItem 9"}
				]
			},
			{code:"Dissolve",
				sub:[
					{style:STYLE_DISSOLVE},
					{code:"SubItem 1"},
					{code:"SubItem 2"},
					{code:"SubItem 3"},
					{code:"SubItem 4"},
					{code:"SubItem 5"},
					{code:"SubItem 6"},
					{code:"SubItem 7"},
					{code:"SubItem 8"},
					{code:"SubItem 9"}
				]
			},
			{code:"Wipe",
				sub:[
					{style:STYLE_WIPE1},
					{code:"SubItem 1"},
					{code:"SubItem 2"},
					{code:"SubItem 3"},
					{code:"SubItem 4"},
					{code:"SubItem 5"},
					{code:"SubItem 6"},
					{code:"SubItem 7"},
					{code:"SubItem 8"},
					{code:"SubItem 9"}
				]
			},
			{code:"Another wipe",
				sub:[
					{style:STYLE_WIPE2},
					{code:"SubItem 1"},
					{code:"SubItem 2"},
					{code:"SubItem 3"},
					{code:"SubItem 4"},
					{code:"SubItem 5"},
					{code:"SubItem 6"},
					{code:"SubItem 7"},
					{code:"SubItem 8"},
					{code:"SubItem 9"}
				]
			}
		]
	},
	{code:'Form elements',
		sub:[
			{},
			{code:"Text box",
				sub:[
					{size:[22,170]},
					{code:"Here we have a text box."},
					{code:'<form action="http://www.google.com/search">Google: <input class="text" type="text" name="q" /></form>', hasControls:true, format:{size:[28,170]}}
				]
			},
			{code:"Memo box",
				sub:[
					{size:[22,170]},
					{code:"Here we have a memo box."},
					{code:'Some memo:<br /><textarea></textarea>', hasControls:true, format:{size:[79,170]}}
				]
			},
			{code:"Combo box",
				sub:[
					{size:[22,170]},
					{code:"Here we have a combo box."},
					{code:'Some combo:<br /><select><option>Option 1</option><option>Option 2</option><option>Option 3</option></select>', hasControls:true, format:{size:[43,170]}}
				]
			}
		]
	},
	{code:'Tips &amp; Status bar',
		sub:[
			{},
			{code:'Tips can be...', alt:'...easily defined!'},
			{code:'Status bar line can be...', status:'...altered without any problems!'},
			{code:'And also...', alt:'Tips &amp; Status bar can  be combined!', status:'Status bar & Tips can  be combined!'}
		]
	}
];
