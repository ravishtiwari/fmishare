/* COOLjsMenu style + structure file */

var STYLE_0 =
{
    backgroundClass:[ "clsCMOn", "clsCMOver" ],
    size:[ 15, 200 ],
    itemoff:[ '+previousItem-1px', 0 ],
    leveloff:[ 0, '+parentItem-1px' ]
}

var SUB_STYLE =
{
backgroundColor:[ "#3D5486", "#AC8C4D"],
	borderWidth:1,
    borderColor:"#666666",
    backgroundClass:[ "clsCMOn_submenu", "clsCMOver_submenu" ],
    size:[ 22, 100 ],
    itemoff:[ '+previousItem-1px', 0 ]
    /*eveloff:[ '+parentItem+1px', 0 ]*/

}

var color = {border:"#666666", bgON:"white",bgOVER:"#B6BDD2"};
var css = {ON:"clsCMOn", OVER:"clsCMOver"};


var MENU_ITEMS = [
    {pos:'relative', style:STYLE_0, blankImage:'http://smcscampaign.ishare.ca/js_menu/images/b.gif'},
    {code:"Ways to Give", url:"../fundraising/fundraising_heart-membership.htm", size:[15,200],
        sub:[
	    {style:SUB_STYLE, size:[22,200]},	    
            {code:"Home with a Heart Club", size:[22,200], url:"../fundraising/fundraising_heart-membership.htm", sub: [{}]},
            {code:"A Legacy for the Future", size:[22,200], url:"../fundraising/fundraising_legacyforthefuture.htm", sub: [{}]},
            {code:"Tribute Gifts", size:[22,200], url:"../fundraising/fundraising_waystodonate.htm",  sub: [{}]}
        ]
    },
    {code:"Home with a Heart Campaign", url:"../fundraising/fund_capitalcampaign.htm", size:[15,200],
        sub:[
	    {style:SUB_STYLE, size:[22,200]},	    
            {code:"Capital Goal", size:[22,200], url:"../fundraising/fund_capitalcampaign.htm", sub: [{}]},
            {code:"Campaign Cabinet", size:[22,200], url:"../fundraising/fund_campaigncabinet.htm", sub: [{}]},
            {code:"Building Advisory Committee", size:[22,200], url:"../fundraising/fund_advisorycommittee.htm",  sub: [{}]},
            {code:"Expanding Our Home", size:[22,200], url:"../fundraising/fund_expandingourhome.htm", sub: [{}]},
            {code:"Groundbreaking Pictures", size:[22,200], url:"../fundraising/fund_groundbreaking.htm", sub: [{}]},
            {code:"Construction Pictures", size:[22,200], url:"../fundraising/fund_construction.htm",  sub: [{}]}
        ]
    }
];

var menu1 = new COOLjsMenuPRO("menu1", MENU_ITEMS);
menu1.initTop();
