<?php

    function spamcheck($field)
    {
        //filter_var() sanitizes the e-mail
        //address using FILTER_SANITIZE_EMAIL
        $field=filter_var($field, FILTER_SANITIZE_EMAIL);
        
        //filter_var() validates the e-mail
        //address using FILTER_VALIDATE_EMAIL
        if(filter_var($field, FILTER_VALIDATE_EMAIL))
        {
            return TRUE;
        }else{
            return FALSE;
        }
    }

	// recipient's email address
	//$to = 'kpetrova@stdemetrius.ca';    
	//$to = 'wtran@fundingmatters.com';
	$to = 'volunteer@stdemetrius.ca';
	//$to = 'lashley@fundingmatters.com';    
	
	// subject of the message
	$re = 'Volunteer Form - St. Demetrius Website'; 


	//personal information
	$salutation = $_POST['saluation'];
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	
	//address
	$apt = $_POST['apt'];
	$street = $_POST['street'];
	$city = $_POST['city'];
	$province = $_POST['province'];

	$postal_code = $_POST['postal_code'];

	//contact
	$home_phone_number = $_POST['home_phone_number'];
	$cell_number = $_POST['cell_number'];
	$business_number = $_POST['business_number'];
	$fax = $_POST['fax'];
	$email_address = $_POST['email_address'];

	//program interest
	$program_interest = $_POST['programinterest'];
	$otheractivity = $_POST['otheractivity'];

	//availabilities
	$availabilities = $_POST['availabilities'];

	//language
	$languages = $_POST['languages'];
	$otherlanguage = $_POST['otherlanguage'];

	//education
	$grade_completed = $_POST['gradecompleted'];
	$courses = $_POST['courses'];
	$hobbies = $_POST['hobbies'];

	//employment history
	$current_job_title = $_POST['jobtitle'];
	$current_employer = $_POST['employer'];
	$former_job_title = $_POST['formerjobtitle'];
	$former_employer = $_POST['formeremployer'];
	$previous_volunteer_work = $_POST['previousvolunteerwork'];
	$reference_1 = $_POST['reference1'];
	$reference_2 = $_POST['reference2'];

	$consent = $_POST['consent'];

	if(empty($program_interest)){
		$program_interest_items = "No program interest specified";
	}else{
		$program_interest_items = implode(", ", $program_interest);
	}

	if(empty($availabilities)){
		$availabilities_items = "No availabilities specified";
	}else{
		$availabilities_items = implode(", ", $availabilities);
	}

	if(empty($languages)){
		$languages_types = "No knowledge of languages specified";
	}else{
		$languages_types = implode(", ", $languages);
	}

	if(empty($consent)){
		$consent_values = "No Consent specified";
	}else{
		$consent_values = implode(", ", $consent);
	}

	//if(empty($salutation)){
	//	$salutation_value = "No Salutation Chosen";
	//}else{
	//	$salutation_value = implode(", ", $salutation);
	//}

    if ($salutation == "mr") {
        $salutation_value = "Mr";   
    }else if ($salutation == "mrs"){
        $salutation_value = "Mrs";
	}else if ($salutation == "ms"){
		$salutation_value = "Ms";
	}else if ($salutation == "miss"){
		$salutation_value = "Miss";      
    }    

$markup = "Salutation:  ".$salutation_value."\r\n".
"Firstname:  ".$firstname."\r\n".
"Lastname:  ".$lastname ."\r\n\n".
"Apt:  ".$apt ."\r\n\n".
"Street:  ".$street ."\r\n\n".
"City:  ".$city ."\r\n\n".
"Province:  ".$province ."\r\n\n".
"Postal Code:  ".$postal_code ."\r\n\n".
"Home Phone Number:  ".$home_phone_number ."\r\n\n".
"Cell Number:  ".$cell_number ."\r\n\n".
"Business Number:  ".$business_number ."\r\n\n".
"Fax:  ".$fax ."\r\n\n".
"Email Address:  ".$email_address ."\r\n\n".
"Program Interests:  ".$program_interest_items ."\r\n\n".
"Other Intersts:  ".$otheractivity ."\r\n\n".
"Availabilities:  ".$availabilities_items ."\r\n\n".
"Language Knowledge:  ".$languages_types ."\r\n\n".
"Other Language:  ".$otherlanguage ."\r\n\n".
"Last Grade Completed:  ".$grade_completed ."\r\n\n".
"Courses:  ".$courses ."\r\n\n".
"Hobbies:  ".$hobbies ."\r\n\n".
"Current Job Title:  ".$current_job_title ."\r\n\n".
"Current Employer:  ".$current_employer ."\r\n\n".
"Former Job Title:  ".$former_job_title ."\r\n\n".
"Former Employer:  ".$former_employer ."\r\n\n".
"Previous Volunteer Work:  ".$previous_volunteer_work ."\r\n\n".
"Reference 1:  ".$reference_1 ."\r\n\n".
"Reference 2:  ".$reference_2 ."\r\n\n".
"Consent:  ".$consent_values ."\r\n\n";


	$msg = $markup;

	mail($to,$re,$msg);



 //echo "this is" .$msg;


?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Ukrainian Canadian Care Centre</title>
		<link rel="stylesheet" type="text/css" href="../css/default.css" />
		<script type="text/javascript" src="../javascripts/COOLjsMenu_Professional_2.9.4/Scripts/coolmenupro.js"></script>
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/style.css" />
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/menu1.js" />

	</head>

<body>

<div class="mainContainer">
			<table class="simple" cellspacing="0" cellpadding="0">
				<tr>
					<!-- main header -->
					<td colspan="8"><img src="images/mainheader.jpg"></td>
				</tr>
				<tr class="topnavigation_volunteer_navbar">
					<!-- top navigation bar -->
					<td>
					<script type="text/javascript" src="../javascripts/js_menu/menu1.js"></script>
					</td>
				</tr>
			</table>
			<table class="simple" cellspacing="0" cellpadding="0">
				<tr>
					<!-- top left navigation bar -->
					<td class="topleftNavBar" background="images/leftNavigationBg.jpg">
						<a class="navigation" href="volunteer.htm">Opportunities</a><br/>
						<a class="navigation" href="volunteer_co-op.htm">Students</a><br/>
						<a class="navigation" href="volunteer_corporate.htm">Corporate</a><br/>
						<a class="navigation" href="volunteer_legacyproject.htm">Legacy Project</a><br/>
						<a class="navigation" href="volunteer_howtoapply.htm"><span class="active-link">Apply</span></a><br/>
					</td>

					<!-- image beside left navigation bar -->
					<td class="banner">
						<img class="banner" src="images/banner.jpg">
					</td>
				</tr>
				<tr>
					<td class="topBannerDivider" colspan="2"><img src="images/topBannerDivider.jpg"></td>
				</tr>
			</table>

			<!-- Main Content Table -->
			<table class="bordered">
				<tr>
					<td>
						<img src="images/headings/howToApplyu.gif"/><br/><br/>
						
						<center><p>Thank you.  Your volunteer form has been submitted</p></center>
						

					</td>
				</tr>
			</table>
			<!-- End Main Content Table -->

			<table class="footer" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="6"><img class="footerDivider" src="images/bottomBannerDivider.jpg"></td>
				</tr>
				<tr>
					<td class="footer-column-1">
						<a href="http://www.oanhss.org" target="_blank"><img class="footer" src="images/footer/oanhss.jpg"></a>
					</td>
					<td class="footer-column-2">
						<a href="http://www.ocsa.on.ca" target="_blank"><img class="footer" src="images/footer/ocsa.jpg"></a>
					</td>
					<td class="footer-column-3">
						<a href="http://www.oltca.com" target="_blank"><img class="footer" src="images/footer/oltca.jpg"></a>
					</td>
					<td class="footer-column-4">
						<a href="http://www.onpha.on.ca" target="_blank"><img class="footer" src="images/footer/onpha.jpg"></a>
					</td>
					<td class="footer-column-5">
						<img class="footer" src="images/footer/lhins.jpg">
					</td>
					<td class="footer-column-5">
						<img class="footer" src="../images/footer/otf.jpg">
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" align="right">
				<tr>
					<td class="page-footer">
						Designed by FUNDING matters Inc.					
					</td>
				</tr>
			</table>
	</div>
<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_project=4253090; 
var sc_invisible=1; 
var sc_partition=34; 
var sc_click_stat=1; 
var sc_security="6dd5a993"; 
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript><div class="statcounter"><a title="web counter" href="http://www.statcounter.com/" target="_blank"><img class="statcounter" src="http://c.statcounter.com/4253090/0/6dd5a993/1/" alt="web counter" ></a></div></noscript> 
<!-- End of StatCounter Code -->
</body>
</html>