<?php

    function spamcheck($field)
    {
        //filter_var() sanitizes the e-mail
        //address using FILTER_SANITIZE_EMAIL
        $field=filter_var($field, FILTER_SANITIZE_EMAIL);
        
        //filter_var() validates the e-mail
        //address using FILTER_VALIDATE_EMAIL
        if(filter_var($field, FILTER_VALIDATE_EMAIL))
        {
            return TRUE;
        }else{
            return FALSE;
        }
    }

	// recipient's email address
	//$to = 'kpetrova@stdemetrius.ca';    
	$to = 'wtran@fundingmatters.com';
	//$to = 'lashley@fundingmatters.com';    
	
	// subject of the message
	$re = 'Stdemetrius Website Feedback'; 

	// message from the feedback form
	$emailAddress = $email;
	
	$msg = 'From: '.$_REQUEST["firstname"].' '.$_REQUEST["lastname"]."\r\n".'Email: '.$_REQUEST["email"]."\r\n\n".'Comment(s):'.$_REQUEST["comment"]."\r\n\n";


    //check if the email address is invalid
    $mailcheck = spamcheck($_REQUEST['e-mail']);
    
    /*if ($mailcheck==FALSE)
    {
        header( 'Location: http://www.stdemetrius.ca/contactus/contact-us.htm' ) ;
    }else{*/
        // send the email now...
        mail($to,$re,$msg);
    /*}*/ 

?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Ukrainian Canadian Care Centre</title>
		<link rel="stylesheet" type="text/css" href="../css/default.css" />
		<script type="text/javascript" src="../javascripts/COOLjsMenu_Professional_2.9.4/Scripts/coolmenupro.js"></script>
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/style.css" />
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/menu1.js" />

	</head>

<body>

<div class="mainContainer">
			<table class="simple" cellspacing="0" cellpadding="0">
				<tr>
					<!-- main header -->
					<td colspan="8"><img src="images/mainheader.jpg"></td>
				</tr>
				<tr class="topnavigation_publications_navbar">
					<!-- top navigation bar -->
					<td>
					<script type="text/javascript" src="../javascripts/js_menu/menu1.js"></script>
					</td>
				</tr>
			</table>
			<table class="simple" cellspacing="0" cellpadding="0">
				<tr>
					<!-- top left navigation bar -->
					<td class="topleftNavBar" background="images/leftNavigationBg.jpg">
						<a class="navigation" href="publications.htm">Newsletters</a><br/>
						<a class="navigation" href="publications_campaign-update.htm">Campaign Update</a><br/>
						<a class="navigation" href="publications_annual.htm"><span class="active-link">Annual Reports</span></a><br/>
					</td>

					<!-- image beside left navigation bar -->
					<td class="banner">
						<img class="banner" src="images/banner.jpg">
					</td>
				</tr>
				<tr>
					<td class="topBannerDivider" colspan="2"><img src="images/topBannerDivider.jpg"></td>
				</tr>
			</table>

			<table class="bordered" cellspacing="0" cellpadding="0">
			    <tr>
					<td >

						<img src="images/headings/newsletters.gif"/><br/><br/>

						<table class="publications-annualreports" cellpadding="0" cellspacing="0">

						  <tr>
							<td>

							<p>Your form has been submitted</p>

							</td>								
						  </tr>

						</table>

					</td>
				</tr>
			</table>

			<table class="footer" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><img class="footerDivider" src="images/bottomBannerDivider.jpg"></td>
				</tr>
				<tr>
					<td class="footer-column-1">
						<a href="http://www.oanhss.org" target="_blank"><img class="footer" src="images/footer/oanhss.jpg"></a>
					</td>
					<td class="footer-column-2">
						<a href="http://www.ocsa.on.ca" target="_blank"><img class="footer" src="images/footer/ocsa.jpg"></a>
					</td>
					<td class="footer-column-3">
						<a href="http://www.oltca.com" target="_blank"><img class="footer" src="images/footer/oltca.jpg"></a>
					</td>
					<td class="footer-column-4">
						<a href="http://www.onpha.on.ca" target="_blank"><img class="footer" src="images/footer/onpha.jpg"></a>
					</td>
					<td class="footer-column-5">
						<img class="footer" src="images/footer/lhins.jpg">
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" align="right">
				<tr>
					<td class="page-footer">
						Designed by FUNDING matters Inc.					
					</td>
				</tr>
			</table>
	</div>
<!-- Start of StatCounter Code -->
 <script type="text/javascript">
var sc_project=4253090; 
var sc_invisible=1; 
var sc_partition=34; 
var sc_click_stat=1; 
var sc_security="6dd5a993"; 
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript><div class="statcounter"><a title="web counter" href="http://www.statcounter.com/" target="_blank"><img class="statcounter" src="http://c.statcounter.com/4253090/0/6dd5a993/1/" alt="web counter" ></a></div></noscript>
<!-- End of StatCounter Code -->
</body>
</html>