<?php

    function spamcheck($field)
    {
        //filter_var() sanitizes the e-mail
        //address using FILTER_SANITIZE_EMAIL
        $field=filter_var($field, FILTER_SANITIZE_EMAIL);
        
        //filter_var() validates the e-mail
        //address using FILTER_VALIDATE_EMAIL
        if(filter_var($field, FILTER_VALIDATE_EMAIL))
        {
            return TRUE;
        }else{
            return FALSE;
        }
    }

	// recipient's email address
	//$to = 'volunteer@stdemetrius.ca';    
	$to = 'wtran@fundingmatters.com';
	//$to = 'lashley@fundingmatters.com';    
	
	// subject of the message
	$re = 'Family Council Contact - St. Demetrius Website'; 

	// message from the feedback form
	$emailAddress = $email;
	
	$msg = 'From: '.$_REQUEST["firstname"].' '.$_REQUEST["lastname"]."\r\n".'Email: '.$_REQUEST["email"]."\r\n\n".'Comment(s):'.$_REQUEST["comment"]."\r\n\n";


    //check if the email address is invalid
    $mailcheck = spamcheck($_REQUEST['e-mail']);
    
    /*if ($mailcheck==FALSE)
    {
        header( 'Location: http://www.stdemetrius.ca/contactus/contact-us.htm' ) ;
    }else{*/
        // send the email now...
        mail($to,$re,$msg);
    /*}*/ 

?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Ukrainian Canadian Care Centre</title>
		<link rel="stylesheet" type="text/css" href="../css/default.css" />
        <script type="text/javascript" src="../javascripts/js_menu_carecentre/coolmenupro.js"></script>
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/style.css" />
		<link rel="stylesheet" type="text/css" href="../javascripts/js_menu/menu1.js" />

		<script language="JavaScript1.2">

				var slideShowSpeed = 5000
				var crossFadeDuration = 3

				var Pic = new Array() // don't touch this

				Pic[0] = 'images/banner_home.jpg'
				Pic[1] = 'images/banner_carecentre.jpg'
				Pic[2] = 'images/banner_volunteers.jpg'
				Pic[3] = 'images/banner_fundraising.jpg'
				Pic[4] = 'images/banner_publications.jpg'
				Pic[5] = 'images/banner_residence.jpg'


				// =======================================
				// do not edit anything below this line
				// =======================================

				var t
				var j = 0
				var p = Pic.length

				var preLoad = new Array()
				for (i = 0; i < p; i++){
				   preLoad[i] = new Image()
				   preLoad[i].src = Pic[i]
				}

				function runSlideShow(){
				   if (document.all){
					  document.images.SlideShow.style.filter="blendTrans(duration=2)"
					  document.images.SlideShow.style.filter="blendTrans(duration=crossFadeDuration)"
					  document.images.SlideShow.filters.blendTrans.Apply()      
				   }
				   document.images.SlideShow.src = preLoad[j].src
				   if (document.all){
					  document.images.SlideShow.filters.blendTrans.Play()
				   }
				   j = j + 1
				   if (j > (p-1)) j=0
				   t = setTimeout('runSlideShow()', slideShowSpeed)
				}
		</script>
	</head>

<body onLoad="runSlideShow();">

	<div class="mainContainer">
			<table class="simple" cellspacing="0" cellpadding="0">
				<tr>
					<!-- main header -->
					<td colspan="8"><img src="images/mainheader.gif"></td>
				</tr>
				<tr class="topnavigation_carecentre_navbar">
					<!-- top navigation bar -->
					<td>
					<script type="text/javascript" src="../javascripts/js_menu/menu1.js"></script>
					</td>
				</tr>
			</table>
			<table class="simple" cellspacing="0" cellpadding="0">
				<tr>
					<!-- top left navigation bar -->
					<td class="topleftNavBar" background="images/leftNavigationBg.jpg">
						<a class="navigation" href="uccc_home.htm">Care Centre</a><br/>
						<a class="navigation" href="uccc_admission.htm">Admission</a><br/>
						<a class="navigation" href="uccc_staff.htm">Staff</a><br/>
						<a class="navigation" href="uccc_activities.htm">Activities</a><br/>
						<a class="navigation" href="uccc_family-council.htm"><span class="active-link">Family Council</span></a><br/>
						<a class="navigation" href="uccc_location.htm">Location</a><br/>
						<a class="navigation" href="uccc_virtual-tour.htm">Virtual Tour</a><br/>
					</td>

					<!-- image beside left navigation bar -->
					<td class="banner">
						<img class="banner" src="images/banner.jpg" name="slideShow">
					</td>
				</tr>
				<tr>
					<td class="topBannerDivider" colspan="2"><img src="images/topBannerDivider.jpg"></td>
				</tr>
			</table>

			<table class="bordered" cellspacing="0" cellpadding="0">
			    <tr>
					<td>

						<img src="images/headings/family_council.jpg"/><br/><br/>

							<center><p>Your Comments have been submitted!  Thank you for your feedback.</p>
							
							<p>Return to the <a href="uccc_family-council.htm">Ukrainian Canadian Care Centre Family Council</a> page<p></center>
							
							
						</td>
					</tr>
				</table>
		<table class="footer" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><img class="footerDivider" src="images/bottomBannerDivider.jpg"></td>
				</tr>
				<tr>
					<td class="footer-column-1">
						<a href="http://www.oanhss.org" target="_blank"><img class="footer" src="images/footer/oanhss.jpg"></a>
					</td>
					<td class="footer-column-2">
						<a href="http://www.ocsa.on.ca" target="_blank"><img class="footer" src="images/footer/ocsa.jpg"></a>
					</td>
					<td class="footer-column-3">
						<a href="http://www.oltca.com" target="_blank"><img class="footer" src="images/footer/oltca.jpg"></a>
					</td>
					<td class="footer-column-4">
						<a href="http://www.onpha.on.ca" target="_blank"><img class="footer" src="images/footer/onpha.jpg"></a>
					</td>
					<td class="footer-column-5">
						<img class="footer" src="images/footer/lhins.jpg">
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" align="right">
				<tr>
					<td class="page-footer">
						Designed by FUNDING matters Inc.					
					</td>
				</tr>
			</table>
	</div>
<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_project=4253090; 
var sc_invisible=1; 
var sc_partition=34; 
var sc_click_stat=1; 
var sc_security="6dd5a993"; 
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript><div class="statcounter"><a title="web counter" href="http://www.statcounter.com/" target="_blank"><img class="statcounter" src="http://c.statcounter.com/4253090/0/6dd5a993/1/" alt="web counter" ></a></div></noscript>
<!-- End of StatCounter Code -->
</body>
</html>
