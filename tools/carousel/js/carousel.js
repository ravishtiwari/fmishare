function carousel(n, pw, ph, dh, db, tc, fc, fs){

	var name = n;
	var picWidth = pw;
	var picHeight = ph;
	var descHeight = dh;
	var descBottom = db;
	var transColor = tc;
	var fontColor = fc;
	var fontSize = fs;
	
	var self = this;
	
	
	self.imageList = [
			{"path":"images/carousel/0001.jpg", "text":"Gabriel is watched over by a couple of helpers"},
			{"path":"images/carousel/0105.jpg", "text":"Monica has her hair done."},
			{"path":"images/carousel/0170.jpg", "text":"A night of dancing."},
			{"path":"images/carousel/0252.jpg", "text":"With friends."},
			{"path":"images/carousel/0387.jpg", "text":"Monica has a happy moment playing the piano."},
			{"path":"images/carousel/0455.jpg", "text":"The tea ceremony."},
			{"path":"images/carousel/0677.jpg", "text":"A lucky one catches the bouquet."}
		];
	
	
	self.timer = 0;
	
	// current index of the image
	self.curImage = 0;
	
	// time between image changes
	self.time_length = 5000;
	

	// create the carousel elements
	self.createElements = function(){
		// display area
		var displayArea = document.createElement("div");
		displayArea.id = "display";
		displayArea.onmouseover = new Function(name + '.stopTimer();');
		displayArea.onmouseout = new Function(name + '.delayedTimedRotation();');
		document.getElementById("carousel").appendChild(displayArea);
		
		var picture = document.createElement("img");
		picture.id = "picture";
		picture.src = self.imageList[0].path;
		picture.alt = "No Image Loaded";
		document.getElementById("display").appendChild(picture);
		
		// description text contaner
		var description = document.createElement("div");
		description.id = "description";
		document.getElementById("display").appendChild(description);
		
		// description text transparency
		var descTransparency = document.createElement("div");
		descTransparency.id = "descTransparency";
		document.getElementById("description").appendChild(descTransparency);
		
		// description text 
		var descText = document.createElement("div");
		descText.id = "descText";
		descText.innerHTML = "This is where the image description will be.  Information will be provided shortly.  This is where the image description will be.  Information will be provided shortly.";
		document.getElementById("description").appendChild(descText);
		
		// control area container
		var control = document.createElement("div");
		control.id = "control";
		document.getElementById("carousel").appendChild(control);
		
		// prev button
		var prev = document.createElement("img");
		prev.id = "prev";
		prev.onclick = self.prevImage;
		prev.onmouseover = new Function(name + '.prevButtonSwitch("over");');
		prev.onmouseout = new Function(name + '.prevButtonSwitch("out");');
		prev.src = "images/carousel/prev.png";
		document.getElementById("control").appendChild(prev);
		
		// next button
		var next = document.createElement("img");
		next.id = "next";
		next.onclick = self.nextImage;
		next.onmouseover = new Function(name + '.nextButtonSwitch("over");');
		next.onmouseout = new Function(name + '.nextButtonSwitch("out");');
		next.src = "images/carousel/next.png";
		document.getElementById("control").appendChild(next);
		
		// position dot markers
		var position = document.createElement("div");
		position.id = "position";
		document.getElementById("control").appendChild(position);
	};
	
	
	
	self.setParameters = function(){
		document.getElementById("carousel").style.width = picWidth + "px";
		document.getElementById("display").style.height = picHeight + "px";
		document.getElementById("display").style.width = picWidth + "px";
		document.getElementById("next").style.left = (picWidth - 57) + "px";
		document.getElementById("description").style.bottom = descBottom + "px";
		document.getElementById("description").style.height = descHeight + "px";
		document.getElementById("descTransparency").style.backgroundColor = tc;
		document.getElementById("descText").style.color = fc;
		document.getElementById("descText").style.fontSize = fs + "px";
	}
	
	// pop in/out next button
	self.nextButtonSwitch = function(s){
		if (s === "over"){
			document.getElementById("next").src = "images/carousel/next2.png";
		}else if(s === "out"){
			document.getElementById("next").src = "images/carousel/next.png";
		}
	};
	
	// pop in/out prev button
	self.prevButtonSwitch = function(s){
		if (s === "over"){
			document.getElementById("prev").src = "images/carousel/prev2.png";
		}else if(s === "out"){
			document.getElementById("prev").src = "images/carousel/prev.png";
		}
	};
	
	// set an image at location n
	self.setImage = function(n){
		document.getElementById("picture").src = self.imageList[n].path;
		document.getElementById("descText").innerHTML = self.imageList[n].text;
		self.curImage = n;
	};
	
	// move to the next image
	self.nextImage = function(){
		if(self.curImage < self.imageList.length - 1){
			self.dotClick(self.curImage+1);
			self.stopTimer();
			self.delayedTimedRotation();
		}
	};
	
	// move back to the previous image
	self.prevImage = function(){
	
		if(self.curImage > 0){
			self.dotClick(self.curImage-1);
			self.stopTimer();
			self.delayedTimedRotation();
		}
	};
	
	// switch dot n from popped in or popped out
	// s is "over" on mouseover
	// s is "out" on mouseout
	self.dotSwitch = function(n, s){
		
		if (self.curImage !== n){
			if (s === "over"){
				document.getElementById("dot" + n).src = "images/carousel/dot2.png";
			}else if (s === "out"){
				document.getElementById("dot" + n).src = "images/carousel/dot.png";
			}
		}
		
	};
	
	// set reset the previous clicked dot
	// set the new image in carousel display
	// set clicked dot with 'clicked dot' image
	self.dotClick = function(n){
		document.getElementById("dot" + self.curImage).src = "images/carousel/dot.png";
		self.setImage(n);
		document.getElementById("dot" + n).src = "images/carousel/dot3.png";
	};
	
	// create the navigation dots
	self.createDots = function(){
		var totalDots = self.imageList.length;
		var dot;
		
		var i=0;
		for (i=0; i<totalDots; i++)
		{
			dot = document.createElement("img");
			dot.id = "dot" + i;
			dot.setAttribute("class", "dot");
			dot.onclick = new Function(name + '.dotClick(' + i + ');');
			dot.onmouseover = new Function(name + '.dotSwitch(' + i + ',"over");');
			dot.onmouseout = new Function(name + '.dotSwitch(' + i + ',"out");');
			dot.src = "images/carousel/dot.png";
			document.getElementById("position").appendChild(dot);
		}
		
		// realign the set of dots
		document.getElementById("position").style.left = ((picWidth/2) - 6*i) + "px";
		
	};
	
	// delay resuming the carousel rotation
	self.delayedTimedRotation = function(){
		self.timer = setTimeout(function() {self.setTimedRotation();},self.time_length);
	};
	
	// timer is recursive
	// image switch immediately, then wait before calling again
	self.setTimedRotation = function(){
		if(self.curImage < self.imageList.length-1){
			self.dotClick(self.curImage+1);
		}else{
			self.dotClick(0);
		}
		self.timer = setTimeout(function() {self.setTimedRotation();},self.time_length);
	};
	
	// stop the timer
	self.stopTimer = function(){
		clearTimeout(self.timer);
	};
	
	
	self.createElements();	
	self.createDots();
	self.setParameters();
	self.dotClick(0);
	self.timer = setTimeout(function() {self.setTimedRotation();}, self.time_length);
	
}